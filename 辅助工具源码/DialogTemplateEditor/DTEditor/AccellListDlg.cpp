// AccellListDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DTEditor.h"
#include "AccellListDlg.h"

#include "EditorView.h"
#include "KeyConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern char* g_KeyMapPath;
/////////////////////////////////////////////////////////////////////////////
// CAccellListDlg dialog


CAccellListDlg::CAccellListDlg(CEditorView* pParent)
	: CDialog(CAccellListDlg::IDD, pParent)
{
	m_pView = pParent;
	//{{AFX_DATA_INIT(CAccellListDlg)
	m_nID = 0;
	m_strKeyValue = _T("");
	//}}AFX_DATA_INIT
}


void CAccellListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAccellListDlg)
	DDX_Control(pDX, IDC_LIST_KEY_NAME, m_lstKeyName);
	DDX_Control(pDX, IDC_LIST, m_lstAccell);
	DDX_Control(pDX, IDC_EDIT_KEYVALUE, m_edtKeyValue);
	DDX_Control(pDX, IDC_EDIT_ID, m_edtID);
	DDX_Text(pDX, IDC_EDIT_ID, m_nID);
	DDV_MinMaxInt(pDX, m_nID, 0, 2147483647);
	DDX_Text(pDX, IDC_EDIT_KEYVALUE, m_strKeyValue);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAccellListDlg, CDialog)
	//{{AFX_MSG_MAP(CAccellListDlg)
	ON_BN_CLICKED(ID_BUTTON_ADD, OnAdd)
	ON_BN_CLICKED(ID_BUTTON_MODIFY, OnModify)
	ON_BN_CLICKED(ID_BUTTON_DELETE, OnDelete)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_KEY_NAME, OnDblclkListKeyName)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAccellListDlg message handlers
BOOL CAccellListDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// 设置一行全部选中属性
	m_lstAccell.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_lstAccell.InsertColumn(1, "ID号", LVCFMT_LEFT, 75);
	m_lstAccell.InsertColumn(2, "快捷键值", LVCFMT_LEFT, 120);

	// 从结构体传送给列表控件
	_ACCELL* theNext = m_pAccell;
	for( int i=0; i<m_iCount; i++ )
	{
		if( theNext == NULL )
			break;

		int nID = theNext->ID;
		char* psKeyValue = theNext->psKeyValue;
		theNext = theNext->next;

		int iPos = m_lstAccell.GetItemCount ();
		LV_ITEM lv;
		memset ((void *)&lv, 0, sizeof(LV_ITEM));
		lv.mask = LVIF_TEXT;
		lv.iItem = iPos;
		lv.iSubItem = 0;
		lv.pszText = (LPTSTR)(LPCTSTR)"EXISTUSER";
		m_lstAccell.InsertItem(&lv);

		CString strID;
		strID.Format("%d",nID);
		CString strKeyValue;
		strKeyValue.Format("%s",psKeyValue);

		m_lstAccell.SetItemText (iPos, 0, strID);
		m_lstAccell.SetItemText (iPos, 1, strKeyValue);
	}

	if( m_nID != -1 )
	{
		// 首先在列表中察看有没有ID号相同的列表项
		int nCount = m_lstAccell.GetItemCount();
		BOOL bE = FALSE;
		for( int i=0; i<nCount; i++ )
		{
			// 如果有，设置该条目为选中，并将其数值复制到两个输入框中
			int nItemID = atoi( m_lstAccell.GetItemText( i, 0 ) );
			if( m_nID == nItemID )
			{
				m_nID = nItemID;
				m_strKeyValue = m_lstAccell.GetItemText( i, 1 );
				UpdateData( FALSE );
				m_lstAccell.SetSelectionMark( i );
				bE = TRUE;
			}
		}

		// 如果没有，则将ID号设置在ID输入框，将焦点设置在KeyValue输入框
		if( !bE )
		{
			m_edtKeyValue.SetFocus();
		}
	}

	// 初始化KeyName列表
	m_lstKeyName.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_lstKeyName.InsertColumn(1, "Key名称", LVCFMT_LEFT, 160);

	// 打开HSKEY.txt文件，创建按键名称索引表
	m_pKeyList = new CKeyConfig();

	if( !m_pKeyList->Load( m_pView->m_KeyMapPath ) )
	{
		char temp[240];
		sprintf( temp, "未能打开键盘宏定义文件%s", KEYMAP_FILENAME );
		MessageBox( temp, "提示信息", MB_OK );
		return FALSE;
	}

	int nCount = m_pKeyList->GetCount();

	char psTemp[64];
	for( int j=0; j<nCount; j++ )
	{
		m_pKeyList->Get( j, psTemp );
		m_lstKeyName.SetItemText( j, 0, psTemp );

		// 新的条目插入到最后
		int iPos = m_lstKeyName.GetItemCount ();

		LV_ITEM lv;
		memset ((void *)&lv, 0, sizeof(LV_ITEM));
		lv.mask = LVIF_TEXT;
		lv.iItem = iPos;
		lv.iSubItem = 0;
		lv.pszText = (LPTSTR)(LPCTSTR)"EXISTUSER";
		m_lstKeyName.InsertItem(&lv);

		m_lstKeyName.SetItemText (iPos, 0, psTemp);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAccellListDlg::OnClose() 
{
	// 删除CHSKEY类
	delete m_pKeyList;
	m_pKeyList = NULL;
	
	CDialog::OnClose();
}

int CAccellListDlg::DoModal( int nID ) 
{
	m_nID = nID;

	return CDialog::DoModal();
}

BOOL CAccellListDlg::RemoveAll ()
{
	_ACCELL* theNext = NULL;
	int i;
	for (i=0; i<m_iCount; i++)
	{
		if (m_pAccell == NULL)
		{
			m_iCount = 0;
			return FALSE;
		}

		theNext = m_pAccell->next;
		if (m_pAccell != NULL )
		{
			delete m_pAccell;
		}
		m_pAccell = theNext;
	}
	m_iCount = 0;
	return TRUE;
}

// 从字符串恢复控件属性
BOOL CAccellListDlg::Open( CString strInfo )
{
	RemoveAll ();
	CString strTemp;

	if( GetSlice( &strTemp, strInfo, 0 ) )
	{
		if( strTemp != "ACCELL" )
			return FALSE;
	}

	_ACCELL* theNext = m_pAccell;
	int k = 1;
	BOOL bFirst = TRUE;
	while (1)
	{
		strTemp = "";
		BOOL b = GetSlice( &strTemp, strInfo, k );
		k ++;
		if (!b)
		{
			return TRUE;
		}

		char psFirstPara[256];
		memset( psFirstPara, 0x0, 256 );
		strncpy( psFirstPara, strTemp, 255 );

		strTemp = "";
		b = GetSlice( &strTemp, strInfo, k );
		k ++;
		if (!b)
		{
			return FALSE;	// 不成组
		}

		int iSecondPara = atoi( strTemp );

		m_iCount ++;
		_ACCELL* theNewOne = new _ACCELL;
		memset( theNewOne->psKeyValue, 0x0, 256 );
		strncpy( theNewOne->psKeyValue, psFirstPara, 255 );
		theNewOne->ID		 = iSecondPara;

		if (bFirst)		// 如果是第一次，新建表项的指针赋给m_pAccell
		{
			m_pAccell = theNewOne;
			theNext = m_pAccell;
			bFirst = FALSE;
		}
		else
		{
			theNext->next = theNewOne;
			theNext = theNewOne;
		}
	}
}

// 将控件属性存入字符串
CString CAccellListDlg::Save()
{
	CString strResult;

	// 添加注释行
	if( m_iCount > 0 )
	{
		strResult = "#快捷键列表\r\n";
	}
	else
	{
		strResult = "#快捷键列表为空\r\n";
	}

	// 写入快捷键列表信息
	strResult += "ACCELL;";

	_ACCELL* theNext = m_pAccell;
	for( int i=0; i<m_iCount; i++ )
	{
		if( theNext == NULL )
			break;

		CString strKeyValue;
		strKeyValue.Format("%s",theNext->psKeyValue);
		CString strID;
		strID.Format("%d",theNext->ID);

		strResult += strKeyValue;
		strResult += ";";
		strResult += strID;
		strResult += ";";

		theNext = theNext->next;
	}

	return strResult;
}

void CAccellListDlg::OnAdd() 
{
	UpdateData( TRUE );

	// 首先进行数值合法性检查
	if (! IsValueValid(-1)) {
		return;
	}

	// 插入列表
	int iPos = m_lstAccell.GetItemCount();
	LV_ITEM lv;
	memset ((void *)&lv, 0, sizeof(LV_ITEM));
	lv.mask = LVIF_TEXT;
	lv.iItem = iPos;
	lv.iSubItem = 0;
	lv.pszText = (LPTSTR)(LPCTSTR)"EXISTUSER";
	m_lstAccell.InsertItem(&lv);

	CString strID;
	strID.Format("%d",m_nID);
	CString strKeyValue;
	strKeyValue = m_strKeyValue;

	m_lstAccell.SetItemText (iPos, 0, strID);
	m_lstAccell.SetItemText (iPos, 1, strKeyValue);
}

void CAccellListDlg::OnModify() 
{
	UpdateData( TRUE );

	int nPos = m_lstAccell.GetSelectionMark();
	if (nPos < 0)
	{
		MessageBox( "您必须选中一个列表项，才能执行此操作。", "提示信息", MB_OK | MB_ICONINFORMATION );
		return;
	}

	// 进行数值合法性检查
	if (! IsValueValid(nPos)) {
		return;
	}

	// 修改列表项内容
	CString strID;
	strID.Format("%d",m_nID);
	CString strKeyValue;
	strKeyValue = m_strKeyValue;

	m_lstAccell.SetItemText( nPos, 0, strID );
	m_lstAccell.SetItemText( nPos, 1, strKeyValue );
}

void CAccellListDlg::OnDelete() 
{
	int nPos = m_lstAccell.GetSelectionMark();
	if( nPos < 0 )
	{
		MessageBox( "您必须选中一个列表项，才能执行此操作。", "提示信息", MB_OK | MB_ICONINFORMATION );
		return;
	}

	m_lstAccell.DeleteItem( nPos );
}

// 鼠标双击列表项
void CAccellListDlg::OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// 将列表项的内容复制到两个编辑框
	// 得到选中的行数
	int nIndex = m_lstAccell.GetSelectionMark();

	m_nID = atoi( m_lstAccell.GetItemText( nIndex, 0 ) );
	m_strKeyValue = m_lstAccell.GetItemText( nIndex, 1 );

	UpdateData( FALSE );
	*pResult = 0;
}

void CAccellListDlg::OnOK() 
{
	RemoveAll ();
	CString strTemp;

	_ACCELL* theNext = m_pAccell;
	int k = 1;
	BOOL bFirst = TRUE;

	// 从列表控件传送给结构体
	int nCount = m_lstAccell.GetItemCount();
	for( int i=0; i<nCount; i++ )
	{
		int iFirstPara = atoi( m_lstAccell.GetItemText( i, 0 ) );
		CString strSecondPara = m_lstAccell.GetItemText( i, 1 );

		m_iCount ++;
		_ACCELL* theNewOne = new _ACCELL;
		theNewOne->ID		 = iFirstPara;
		memset( theNewOne->psKeyValue, 0x0, 256 );
		strncpy( theNewOne->psKeyValue, strSecondPara, 255 );

		if (bFirst)		// 如果是第一次，新建表项的指针赋给m_pAccell
		{
			m_pAccell = theNewOne;
			theNext = m_pAccell;
			bFirst = FALSE;
		}
		else
		{
			theNext->next = theNewOne;
			theNext = theNewOne;
		}
	}

	CDialog::OnOK();
}

// 数值合法性检查
BOOL CAccellListDlg::IsValueValid(int nIndex)
{
	// 首先进行数值合法性检查
	// 1、禁止输入雷同项
	// 2、快捷键的值不能重复
	// 注：多个快捷键可以对应同一个ID，而一个快捷键只能对应一个ID
	char sTemp[256];
	memset(sTemp, 0x0, sizeof(sTemp));
	memcpy(sTemp, m_strKeyValue.GetBuffer(0),
		min(m_strKeyValue.GetLength(), sizeof(sTemp)-1));

	// 在按键宏定义中不能找到对应的宏名称
	if (m_pKeyList->Find(sTemp) == -1 )
	{
		MessageBox( "该快捷键的名字在列表中不存在！", "提示信息", MB_OK | MB_ICONINFORMATION );
		return FALSE;
	}

	int nCount = m_lstAccell.GetItemCount();
	int i;
	for (i = 0; i < nCount; i++)
	{
		if (i == nIndex) {
			continue; //不检查自己
		}

		int nID = atoi (m_lstAccell.GetItemText(i, 0));
		CString strKeyValue = m_lstAccell.GetItemText (i, 1);

		if ((m_nID == nID) && (m_strKeyValue == strKeyValue))
		{
			MessageBox( "列表中已经存在一个相同的快捷键映射！", "提示信息", MB_OK | MB_ICONINFORMATION );
			return FALSE;
		}
		
		if( m_strKeyValue == strKeyValue )
		{
			MessageBox( "列表中存在相同的快捷键！", "提示信息", MB_OK | MB_ICONINFORMATION );
			return FALSE;
		}
	}

	return TRUE;
}

void CAccellListDlg::OnCancel() 
{
	CDialog::OnCancel();
}

void CAccellListDlg::OnDblclkListKeyName(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CPoint point;//用于确定弹出菜单的位置
	GetCursorPos( &point );
	CPoint pointTemp = point;//用于点击判断
	m_lstKeyName.ScreenToClient( &pointTemp );
	UINT uFlags = 0;
	int nIndex = m_lstKeyName.HitTest( pointTemp , &uFlags );//得到被选中的行
	m_strKeyValue = m_lstKeyName.GetItemText( nIndex, 0 );

	UpdateData( FALSE );
	*pResult = 0;
}

/* END */