#if !defined(AFX_STYLEDLG_H__4AD9B996_DB46_4F82_8916_1B00E65B80E0__INCLUDED_)
#define AFX_STYLEDLG_H__4AD9B996_DB46_4F82_8916_1B00E65B80E0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StyleDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStyleDlg dialog

class CStyleDlg : public CDialog
{
// Construction
public:
	CStyleDlg( int nType, int nStyle, CWnd* pParent = NULL );   // standard constructor
	int m_nType;
	int m_nStyle;

// Dialog Data
	//{{AFX_DATA(CStyleDlg)
	enum { IDD = IDD_EDITSTYLE };
	CButton	m_checkCloseButton;
	CButton	m_checkIgnoreEnter;
	CButton	m_checkAutoOpenIme;
	CButton	m_checkAutoDropdown;
	CButton	m_checkNoScroll;
	CButton	m_checkDiasbleIme;
	CButton	m_checkNoTitle;
	CButton	m_checkOriDefault;
	CButton	m_checkSolid;
	CButton	m_checkGroup;
	CButton	m_checkPassword;
	CButton	m_checkRoundEdge;
	CButton	m_checkNoBounder;
	BOOL	m_bNoBorder;
	BOOL	m_bRoundEdge;
	BOOL	m_bPassword;
	BOOL	m_bGroup;
	CString	m_strType;
	BOOL	m_bSolid;
	BOOL	m_bOriDefault;
	BOOL	m_bNoTitle;
	BOOL	m_bDisableIme;
	BOOL	m_bNoScroll;
	BOOL	m_bAutoDropdown;
	BOOL	m_bAutoOpenIme;
	BOOL	m_bIgnoreEnter;
	BOOL	m_bCloseButton;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStyleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CStyleDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STYLEDLG_H__4AD9B996_DB46_4F82_8916_1B00E65B80E0__INCLUDED_)
