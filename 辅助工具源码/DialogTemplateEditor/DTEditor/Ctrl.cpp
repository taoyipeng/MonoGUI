// Ctrl.cpp: implementation of the CCtrl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DTEditor.h"
#include "EditorView.h"
#include "CtrlMgt.h"
#include "Ctrl.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCtrl::CCtrl( int nType )
{
	// 设置成员变量的默认值
	m_ControlInfo.nType		= nType;
	m_ControlInfo.nStyle	= WND_STYLE_NORMAL;
	m_ControlInfo.nX1		= 0;
	m_ControlInfo.nY1		= 0;
	m_ControlInfo.nX2		= 0;
	m_ControlInfo.nY2		= 0;
	m_ControlInfo.nID		= 0;
	memset( m_ControlInfo.psCaption, 0x0, 256 );
	m_ControlInfo.nAddData	= 0;
	m_bSelect				= FALSE;		// 是否处于选中状态
	m_nTab					= 0;
}

CCtrl::~CCtrl()
{
}

int CCtrl::GetX1()
{
	return m_ControlInfo.nX1;
}

int CCtrl::GetX2()
{
	return m_ControlInfo.nX2;
}

int CCtrl::GetY1()
{
	return m_ControlInfo.nY1;
}

int CCtrl::GetY2()
{
	return m_ControlInfo.nY2;
}

int CCtrl::SetX1(int x1)
{
	int old_x1 = m_ControlInfo.nX1;
	m_ControlInfo.nX1 = x1;
	return old_x1;
}

int CCtrl::SetX2(int x2)
{
	int old_x2 = m_ControlInfo.nX2;
	m_ControlInfo.nX2 = x2;
	return old_x2;
}

int CCtrl::SetY1(int y1)
{
	int old_y1 = m_ControlInfo.nY1;
	m_ControlInfo.nY1 = y1;
	return old_y1;
}

int CCtrl::SetY2(int y2)
{
	int old_y2 = m_ControlInfo.nY2;
	m_ControlInfo.nY2 = y2;
	return old_y2;
}

// 重新调整调节矩形的位置
void CCtrl::ResizeLevelRects()
{
    int x1 = GetX1();
    int y1 = GetY1();
    int x2 = GetX2();
    int y2 = GetY2();
    int w = x2 - x1;
    int h = y2 - y1;

	m_LeverTop.SetRect(
		(x1 + w / 2 - LEVER_RECT_W / 2),
		(y1 - LEVER_RECT_H / 2),
		(x1 + w / 2 - LEVER_RECT_W / 2 + LEVER_RECT_W),
		(y1 - LEVER_RECT_H / 2 + LEVER_RECT_H));
	m_LeverLeft.SetRect(
		(x1 - LEVER_RECT_W / 2),
		(y1 + h / 2 - LEVER_RECT_H / 2),
		(x1 - LEVER_RECT_W / 2 + LEVER_RECT_W),
		(y1 + h / 2 - LEVER_RECT_H / 2 + LEVER_RECT_H));
	m_LeverBottom.SetRect(
		(x1 + w / 2 - LEVER_RECT_W / 2),
		(y1 + h - LEVER_RECT_H / 2 - 1),
		(x1 + w / 2 - LEVER_RECT_W / 2 + LEVER_RECT_W),
		(y1 + h - LEVER_RECT_H / 2 - 1 + LEVER_RECT_H));
	m_LeverRight.SetRect(
		(x1 + w - LEVER_RECT_W / 2 - 1),
		(y1 + h / 2 - LEVER_RECT_H / 2),
		(x1 + w - LEVER_RECT_W / 2 - 1 + LEVER_RECT_W),
		(y1 + h / 2 - LEVER_RECT_H / 2 + LEVER_RECT_H));
}
int CCtrl::GetXPos()
{
	return GetX1();
}
int CCtrl::GetYPos()
{
	return GetY1();
}
int CCtrl::GetWidth()
{
    int x1 = GetX1();
    int x2 = GetX2();
	return (x2 - x1);
}
int CCtrl::GetHeight()
{
    int y1 = GetY1();
    int y2 = GetY2();
	return (y2 - y1);
}

void CCtrl::SetPos(int x, int y)
{
    int w = GetWidth();
    int h = GetHeight();
    SetX1(x);
    SetY1(y);
    SetX2(x + w);
    SetY2(y + h);
	ResizeLevelRects();
}

// 显示控件
void CCtrl::Show( CDC* pdc, BOOL bShowTabNum )
{
	HANDLE hOldPen;
	HDC hdc = pdc->GetSafeHdc();
	pdc->SetBkMode( TRANSPARENT );
	pdc->SetTextColor( RGB(0,0,255) );	// 蓝色

	// 根据选中状态决定绘制所使用的画笔
	if( m_bSelect )
	{
		// 使用黄色画笔
		hOldPen = SelectObject (hdc, CreatePen (PS_SOLID, 1, RGB (255, 255, 0)));
	}
	else
	{
		// 使用蓝色画笔
		hOldPen = SelectObject (hdc, CreatePen (PS_SOLID, 1, RGB (0, 0, 255)));
	}

	// 根据类型选择不同的显示方式
	if (WND_TYPE_DIALOG == m_ControlInfo.nType) {
		// 绘制对话框，填充淡蓝色
		pdc->Rectangle( m_ControlInfo.nX1+SCREEN_LEFT, m_ControlInfo.nY1+SCREEN_TOP, m_ControlInfo.nX2+SCREEN_LEFT, m_ControlInfo.nY2+SCREEN_TOP);
		pdc->FillSolidRect( m_ControlInfo.nX1+1+SCREEN_LEFT, m_ControlInfo.nY1+1+SCREEN_TOP, m_ControlInfo.width()-2, m_ControlInfo.height()-2, RGB(128,128,255) );
		if( (m_ControlInfo.nStyle & WND_STYLE_NO_TITLE) == 0 )
		{
			pdc->TextOut( m_ControlInfo.nX1+SCREEN_LEFT, m_ControlInfo.nY1+SCREEN_TOP, m_ControlInfo.psCaption );
			pdc->MoveTo( m_ControlInfo.nX1+SCREEN_LEFT, m_ControlInfo.nY1+SCREEN_TOP+TITLE_HEIGHT );
			pdc->LineTo( m_ControlInfo.nX2+SCREEN_LEFT, m_ControlInfo.nY1+SCREEN_TOP+TITLE_HEIGHT );
		}
	}
	else {
		CString  strText = "UnknownItem";
		COLORREF ctrlColor = RGB(0,0,0);
		switch (m_ControlInfo.nType) {
		case WND_TYPE_STATIC:
			// 绘制静态文本，填充淡灰色
			ctrlColor = RGB(220,220,220);
			strText = "Static";
			break;
		case WND_TYPE_BUTTON:
			// 绘制按钮，填充深黄色
			ctrlColor = RGB(192,192,000);
			strText = "Button";
			break;
		case WND_TYPE_EDIT:
			// 绘制编辑框，填充淡绿色
			ctrlColor = RGB(200,255,255);
			strText = "Edit";
			break;
		case WND_TYPE_LIST:
			// 绘制列表框，填充深绿色
			ctrlColor = RGB(100,200,100);
			strText = "List";
			break;
		case WND_TYPE_COMBO:
			// 绘制组合框，填充淡红色
			ctrlColor = RGB(255,200,200);
			strText = "ComboBox";
			break;
		case WND_TYPE_PROGRESS:
			// 绘制进度条，填充淡红色
			ctrlColor = RGB(255,255,200);
			strText = "ProgressBar";
			break;
		case WND_TYPE_IMAGE_BUTTON:
			// 绘制图形按钮，填充天蓝色
			ctrlColor = RGB(128,225,255);
			strText = "ImageButton";
			break;
		case WND_TYPE_CHECK_BOX:
			// 绘制复选框，填充深红色
			ctrlColor = RGB(225,96,128);
			strText = "CheckBox";
			break;
		}
		pdc->Rectangle(m_ControlInfo.nX1+SCREEN_LEFT, m_ControlInfo.nY1+SCREEN_TOP, m_ControlInfo.nX2+SCREEN_LEFT, m_ControlInfo.nY2+SCREEN_TOP);
		pdc->FillSolidRect(m_ControlInfo.nX1+1+SCREEN_LEFT, m_ControlInfo.nY1+1+SCREEN_TOP, m_ControlInfo.width()-2, m_ControlInfo.height()-2, ctrlColor);
		pdc->TextOut(m_ControlInfo.nX1+SCREEN_LEFT, m_ControlInfo.nY1+SCREEN_TOP, strText);
	}

	// 显示Tab序号
	if (bShowTabNum) {
		if (! IsDialog()) { //对话框不显示tab序号（对话框的tab号永远是0，不参与排号）
			CString strTabNum;
			strTabNum.Format("%d", m_nTab);
			pdc->SetBkMode(OPAQUE);
			pdc->SetTextColor(RGB(255,0,0));	// 红色Tab编号
			pdc->TextOut(m_ControlInfo.nX1+1+SCREEN_LEFT, m_ControlInfo.nY1+1+SCREEN_TOP, strTabNum);
		}
	}

	// 绘制调节位置的矩形框
	if( !bShowTabNum && m_bSelect )
	{
        // 调节框固定使用黑色画笔
        // 先复原画笔
        DeleteObject( SelectObject( hdc, hOldPen ) );
        hOldPen = SelectObject (hdc, CreatePen (PS_SOLID, 1, RGB (0, 0, 0)));
        COLORREF colorLever = RGB(192,255,192);
		pdc->Rectangle(m_LeverTop.left+SCREEN_LEFT, m_LeverTop.top+SCREEN_TOP, m_LeverTop.right+SCREEN_LEFT, m_LeverTop.bottom+SCREEN_TOP);
		pdc->FillSolidRect(m_LeverTop.left+SCREEN_LEFT+1, m_LeverTop.top+SCREEN_TOP+1, LEVER_RECT_W-2, LEVER_RECT_H-2, colorLever);
		pdc->Rectangle(m_LeverLeft.left+SCREEN_LEFT, m_LeverLeft.top+SCREEN_TOP, m_LeverLeft.right+SCREEN_LEFT, m_LeverLeft.bottom+SCREEN_TOP);
		pdc->FillSolidRect(m_LeverLeft.left+SCREEN_LEFT+1, m_LeverLeft.top+SCREEN_TOP+1, LEVER_RECT_W-2, LEVER_RECT_H-2, colorLever);
		pdc->Rectangle(m_LeverBottom.left+SCREEN_LEFT, m_LeverBottom.top+SCREEN_TOP, m_LeverBottom.right+SCREEN_LEFT, m_LeverBottom.bottom+SCREEN_TOP);
		pdc->FillSolidRect(m_LeverBottom.left+SCREEN_LEFT+1, m_LeverBottom.top+SCREEN_TOP+1, LEVER_RECT_W-2, LEVER_RECT_H-2, colorLever);
		pdc->Rectangle(m_LeverRight.left+SCREEN_LEFT, m_LeverRight.top+SCREEN_TOP, m_LeverRight.right+SCREEN_LEFT, m_LeverRight.bottom+SCREEN_TOP);
		pdc->FillSolidRect(m_LeverRight.left+SCREEN_LEFT+1, m_LeverRight.top+SCREEN_TOP+1, LEVER_RECT_W-2, LEVER_RECT_H-2, colorLever);		
	}
	DeleteObject (SelectObject(hdc, hOldPen));
}
// 当前控件是不是对话框
BOOL CCtrl::IsDialog()
{
	return (WND_TYPE_DIALOG == m_ControlInfo.nType);
}
// 检测鼠标落点是否在控件的矩形区域中
BOOL CCtrl::PtInArea(int x, int y)
{
	// 根据XY坐标确定当前控件是否被选中。如果选中则修改选中标志并返回TRUE
    int x1 = GetX1() + SCREEN_LEFT;
    int y1 = GetY1() + SCREEN_TOP;
    int x2 = GetX2() + SCREEN_LEFT;
    int y2 = GetY2() + SCREEN_TOP;
	return (x >= x1 && x <= x2 && y >= y1 && y <=y2);
}
// 判断是否与矩形相交
BOOL CCtrl::RectInArea (CRect rc)
{
	// 根据XY坐标确定当前控件是否被选中。如果选中则修改选中标志并返回TRUE
    int x1 = GetX1() + SCREEN_LEFT;
    int y1 = GetY1() + SCREEN_TOP;
    int x2 = GetX2() + SCREEN_LEFT;
    int y2 = GetY2() + SCREEN_TOP;

    return( abs(x1 + x2 - rc.left - rc.right) < abs(x2 - x1) + abs(rc.right - rc.left)
         && abs(y1 + y2 - rc.top - rc.bottom) < abs(y2 - y1) + abs(rc.bottom - rc.top) );
}

// 设置控件的选中状态
// TRUE:选中；FALSE:未选中
BOOL CCtrl::SetSelState (BOOL bSelect)
{
	// 设置修改标志
	BOOL bOldState;
	bOldState = m_bSelect;
	m_bSelect = bSelect;
	return bOldState;
}

// 得到选择状态
BOOL CCtrl::GetSelState()
{
	return m_bSelect;
}

// 设置Tab序号
int CCtrl::SetTabNum (int nTabNum)
{
	int nOldTab = m_nTab;
	m_nTab = nTabNum;
	return nOldTab;
}

// 取得Tab序号
int CCtrl::GetTabNum()
{
	return m_nTab;
}

// 从字符串恢复控件属性
BOOL CCtrl::Open (CString strInfo)
{
// 对话框
// DIALOG;对话框风格(数字);
// X(数字);Y(数字);W(数字);H(数字);ID(数字);
// 对话框题头文字(字符串，不超过255);

// 控件
// CONTROL;控件类型(数字);控件风格(数字);
// X(数字);Y(数字);W(数字);H(数字);ID(数字);
// 对话框题头文字(字符串，不超过255);附加数据(数字);

	int k = 0;
	CString strTemp;

	if (! GetSlice(&strTemp, strInfo, k))
		return FALSE;
	if (strTemp == "DIALOG")
	{
		m_ControlInfo.nType = WND_TYPE_DIALOG;
	}
	else if (strTemp == "CONTROL")
	{
		k ++;
		if (! GetSlice(&strTemp, strInfo, k))
			return FALSE;
		m_ControlInfo.nType = atoi (strTemp);
	}
	else
	{
		return FALSE;
	}

	k ++;
	if (! GetSlice(&strTemp, strInfo, k))
		return FALSE;
	m_ControlInfo.nStyle = atoi (strTemp);

	k ++;
	if (! GetSlice(&strTemp, strInfo, k))
		return FALSE;
	m_ControlInfo.nX1 = atoi (strTemp);

	k ++;
	if (! GetSlice(&strTemp, strInfo, k))
		return FALSE;
	m_ControlInfo.nY1 = atoi (strTemp);

	k ++;
	if (! GetSlice(&strTemp, strInfo, k))
		return FALSE;
	m_ControlInfo.nX2 = m_ControlInfo.nX1 + atoi (strTemp);

	k ++;
	if (! GetSlice(&strTemp, strInfo, k))
		return FALSE;
	m_ControlInfo.nY2 = m_ControlInfo.nY1 + atoi (strTemp);

	k ++;
	if (! GetSlice(&strTemp, strInfo, k))
		return FALSE;
	m_ControlInfo.nID = atoi (strTemp);

	k ++;
	if (! GetSlice(&strTemp, strInfo, k))
		return FALSE;
	strncpy ((char *)m_ControlInfo.psCaption, strTemp ,255);

	if (m_ControlInfo.nType != WND_TYPE_DIALOG)
	{
		k ++;
		if (! GetSlice(&strTemp, strInfo, k))
			return FALSE;
		m_ControlInfo.nAddData = atoi (strTemp);
	}
	else
	{
		m_ControlInfo.nAddData = 0;
	}

	return TRUE;
}

// 将控件属性存入字符串
CString CCtrl::Save()
{
// 对话框
// DIALOG;对话框风格(数字);
// X(数字);Y(数字);W(数字);H(数字);ID(数字);
// 对话框题头文字(字符串，不超过255);

// 控件
// CONTROL;控件类型(数字);控件风格(数字);
// X(数字);Y(数字);W(数字);H(数字);ID(数字);
// 对话框题头文字(字符串，不超过255);附加数据(数字);

	CString strResult;
	CString strValue;

	// 添加注释行
	switch (m_ControlInfo.nType)
	{
	case WND_TYPE_DIALOG:
		strResult = "#对话框\r\n";
		break;
	case WND_TYPE_STATIC:
		strResult = "#静态文本\r\n";
		break;
	case WND_TYPE_BUTTON:
		strResult = "#按钮\r\n";
		break;
	case WND_TYPE_EDIT:
		strResult = "#编辑框\r\n";
		break;
	case WND_TYPE_LIST:
		strResult = "#列表框\r\n";
		break;
	case WND_TYPE_COMBO:
		strResult = "#组合框\r\n";
		break;
	case WND_TYPE_PROGRESS:
		strResult = "#进度条\r\n";
		break;
	case WND_TYPE_IMAGE_BUTTON:
		strResult = "#图像按钮\r\n";
		break;
	case WND_TYPE_CHECK_BOX:
		strResult = "#复选框\r\n";
		break;
	default:{}
	}

	// 写入模板信息
	if( m_ControlInfo.nType == WND_TYPE_DIALOG )
		strResult += "DIALOG;";
	else
		strResult += "CONTROL;";

	if( m_ControlInfo.nType != WND_TYPE_DIALOG )
	{
		strValue.Format( "%d", m_ControlInfo.nType );
		strResult += strValue;
		strResult += ";";
	}

	strValue.Format( "%d", m_ControlInfo.nStyle );
	strResult += strValue;
	strResult += ";";

	strValue.Format( "%d", m_ControlInfo.nX1 );
	strResult += strValue;
	strResult += ";";

	strValue.Format( "%d", m_ControlInfo.nY1 );
	strResult += strValue;
	strResult += ";";

	strValue.Format( "%d", m_ControlInfo.width() );
	strResult += strValue;
	strResult += ";";

	strValue.Format( "%d", m_ControlInfo.height() );
	strResult += strValue;
	strResult += ";";

	strValue.Format( "%d", m_ControlInfo.nID );
	strResult += strValue;
	strResult += ";";

	strValue.Format( "%s", m_ControlInfo.psCaption );
	strResult += strValue;
	strResult += ";";

	if( m_ControlInfo.nType != WND_TYPE_DIALOG )
	{
		strValue.Format( "%d", m_ControlInfo.nAddData );
		strResult += strValue;
		strResult += ";";
	}

	return strResult;
}

// 设置改变尺寸操作的鼠标光标
BOOL CCtrl::SetModifySizeCursor (int* pnCursor, int *pnMode, int x, int y)
{
	CPoint pt(x - SCREEN_LEFT, y - SCREEN_TOP);
	if (m_LeverRight.PtInRect(pt))
	{
		// 拖动右边缘
		*pnCursor = CEditorView::CURSOR_L_R;
		*pnMode = CCtrlMgt::MOVE_RIGHT;
	}
	else if (m_LeverBottom.PtInRect(pt))
	{
		// 拖动下边缘
		*pnCursor = CEditorView::CURSOR_U_D;
		*pnMode = CCtrlMgt::MOVE_BOTTOM;
	}
	else if (m_LeverLeft.PtInRect(pt))
	{
		// 拖动左边缘
		*pnCursor = CEditorView::CURSOR_L_R;
		*pnMode = CCtrlMgt::MOVE_LEFT;
	}
	else if (m_LeverTop.PtInRect(pt))
	{
		// 拖动上边缘
		*pnCursor = CEditorView::CURSOR_U_D;
		*pnMode = CCtrlMgt::MOVE_TOP;
	}
	else if (PtInArea(x,y))
	{
		// 移动
		*pnCursor = CEditorView::CURSOR_MOVE;
		*pnMode = CCtrlMgt::MOVE_CENTER;
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

// 处理鼠标拖拽改变尺寸
void CCtrl::MouseDrag (int nMode, int nDX, int nDY)
{
	switch (nMode)
	{
	case CCtrlMgt::NO_MOVE:		// 设定无效
		{
			return;
		}
	case CCtrlMgt::MOVE_CENTER:		// 移动
		{
            int x1 = GetX1();
            int y1 = GetY1();
            SetPos (x1 + nDX, y1 + nDY);
			break;
		}
	case CCtrlMgt::MOVE_TOP:		// 拖拽上边缘
		{
            int y1 = GetY1();
            int y2 = GetY2();
			y1 += nDY;
			if (y1 < -SCREEN_TOP)// 不允许拖出窗口
			{
				y1 = -SCREEN_TOP;
			}
			else if (y1 > y2)
			{
				y1 = y2;
			}
            SetY1(y1);
			break;
		}
	case CCtrlMgt::MOVE_BOTTOM:		// 拖拽下边缘
		{
            int y1 = GetY1();
            int y2 = GetY2();
			y2 += nDY;
            if (y2 < y1)
			{
				y2 = y1;
			}
            SetY2(y2);
			break;
		}
	case CCtrlMgt::MOVE_LEFT:		// 拖拽左边缘
		{
            int x1 = GetX1();
            int x2 = GetX2();
            x1 += nDX;
			if (x1 < -SCREEN_LEFT)// 不允许拖出窗口
			{
				x1 = -SCREEN_LEFT;
			}
			else if (x1 > x2)
			{
				x1 = x2;
			}
            SetX1(x1);
			break;
		}
	case CCtrlMgt::MOVE_RIGHT:		// 拖拽右边缘
		{
            int x1 = GetX1();
            int x2 = GetX2();
			x2 += nDX;
			if (x2 < x1)
			{
				x2 = x1;
			}
            SetX2(x2);
			break;
		}
	}

    // 调整控制矩形的位置
	ResizeLevelRects();
}

/* END */