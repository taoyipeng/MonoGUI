// CtrlMgt.cpp: implementation of the CCtrlMgt class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DTEditor.h"
#include "Ctrl.h"
#include "CtrlMgt.h"
#include "EditorView.h"
#include "AttributeDlg.h"
#include "AccellListDlg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCtrlMgt::CCtrlMgt(CEditorView* pParent)
{
	m_pView = pParent;
	m_pAttributeDlg = pParent->m_pAttributeDlg;

	m_nCount = 0;
	m_bIsSetTabMode = FALSE;

	for( int i=0; i<CTRL_MAX; i++ )
	{
		m_pCtrls[i] = NULL;
	}

	m_CX	= 100;	// 默认控件长度
	m_CY	= 20;	// 默认项目控件的高度
}

CCtrlMgt::~CCtrlMgt()
{
	for( int i=0; i<m_nCount; i++ )
	{
		delete m_pCtrls[i];
	}
}

// 设置属性列表
void CCtrlMgt::SetAttributeDlg( CAttributeDlg* pAttributeDlg )
{
	m_pAttributeDlg = pAttributeDlg;
}

// 显示全部控件
void CCtrlMgt::Show( CDC* pdc )
{
	int i;

	// 对话框显示在最底层
	int nDialogTabOrder = 0;
	for (i = 0; i < m_nCount; i++)
	{
		if (m_pCtrls[i]->m_ControlInfo.nType == WND_TYPE_DIALOG)
		{
			m_pCtrls[i]->Show( pdc, m_bIsSetTabMode );
			nDialogTabOrder = m_pCtrls[i]->GetTabNum();
			break;
		}
	}

	// 按照Tab编号从小到大依次显示
	for( i=0; i<m_nCount; i++ )
	{
		for( int j=0; j<m_nCount; j++ )
		{
			if( m_pCtrls[j]->GetTabNum() == i )
			{
				if( m_pCtrls[j]->GetTabNum() != nDialogTabOrder )
				{
					m_pCtrls[j]->Show( pdc, m_bIsSetTabMode );
				}
			}
		}
	}
}

// 添加一个控件
void CCtrlMgt::Add(int nType)
{
	if (m_nCount == (CTRL_MAX - 1))
	{
		MessageBox (m_pView->m_hWnd,
			"超越数组上限，您不能再添加新的控件了。",
			"提示信息",
			MB_OK | MB_ICONINFORMATION);
		return;
	}

	// 将所有控件都设置为“未选中”状态，并依次后推
	int i;
	for (i = m_nCount; i > 0; i--)
	{
		m_pCtrls[i] = m_pCtrls[i-1];
		m_pCtrls[i]->SetSelState(FALSE);
	}

	// 将新控件插入在Index = 0的位置
	m_pCtrls[0] = new CCtrl(nType);
	m_nCount ++;

	// 新控件设定为“选中”
	m_pCtrls[0]->SetSelState(TRUE);

	// 给予新控件最大的Tab编号
	m_pCtrls[0]->SetTabNum(m_nCount - 1);

	// 根据默认尺寸设置新控控件
	m_pCtrls[0]->m_ControlInfo.nX2 = m_CX;
	m_pCtrls[0]->m_ControlInfo.nY2 = m_CY;

	// 刷新控件属性列表
	RenewAttribute();
}

// 删除选中的控件
BOOL CCtrlMgt::DeleteSelected()
{
	if( m_bIsSetTabMode )
	{
		MessageBox( m_pView->m_hWnd, "控件排号状态不能删除控件！", "提示信息", MB_OK | MB_ICONINFORMATION );
		return FALSE;
	}

	// 提示用户必须选择一个控件
	CCtrl* pCtrl = NULL;
	int nIndex = 0;
	if( !GetCurSel( &pCtrl, &nIndex ) )
	{
		MessageBox( m_pView->m_hWnd, "您必须先选定一个控件，然后才能删除。", "提示信息", MB_OK | MB_ICONINFORMATION );
		return FALSE;
	}

	// 对话框不能被删除
	int nType = m_pCtrls[nIndex]->m_ControlInfo.nType;
	if( nType == WND_TYPE_DIALOG )
	{
		MessageBox( m_pView->m_hWnd, "对话框不能被删除！", "提示信息", MB_OK | MB_ICONINFORMATION );
		return FALSE;
	}

	// 得到将要被删除控件的Tab序号
	int nTab = m_pCtrls[nIndex]->GetTabNum();

	// 删除选中的控件
	delete m_pCtrls[nIndex];
	m_nCount --;

	// 删除位置后面的控件依次向前推移
	int i;
	for(i=nIndex; i<m_nCount; i++ )
	{
		m_pCtrls[i] = m_pCtrls[i+1];
	}

	// 给控件重新编排Tab号；Tab号比删除者大的控件，其Tab号要减一；
	for( i=0; i<m_nCount; i++ )
	{
		int nTabNum = m_pCtrls[i]->GetTabNum();
		if( nTabNum > nTab )
		{
			nTabNum --;
			m_pCtrls[i]->SetTabNum( nTabNum );
		}
	}

	// 将所有控件的状态都设为“未选中”
	// 只有Tab号等于被删除的那个设置为“选中”
	// 如果找不到相等的，则Tab号最大的被设置为“选中”
	int nIndexOfMaxTab = 0;		// Tab号最大的控件之Index
	int nMaxTab =0;				// 最大的Tab号
	BOOL bHaveSelected = FALSE;	// 是否有被选中的
	for( i=0; i<m_nCount; i++ )
	{
		m_pCtrls[i]->SetSelState(FALSE);
		int nTabNum = m_pCtrls[i]->GetTabNum();

		if (nTabNum > nMaxTab)
		{
			nMaxTab = nTabNum;
			nIndexOfMaxTab = i;
		}

		if( nTabNum == nTab )
		{
			m_pCtrls[i]->SetSelState (TRUE);
			bHaveSelected = TRUE;
		}
	}

	if (!bHaveSelected)
	{
		m_pCtrls[nIndexOfMaxTab]->SetSelState (TRUE);
	}

	// 刷新控件属性列表
	RenewAttribute ();
	return TRUE;
}

// 设置左上点
void CCtrlMgt::SetTopLeftPoint( int x, int y )
{
	if( !m_bIsSetTabMode )
	{
		// 提示用户必须选择一个控件
		CCtrl* pCtrl = NULL;
		int nIndex;
		if (! GetCurSel(&pCtrl, &nIndex))
		{
			MessageBox (m_pView->m_hWnd,
				"您必须先选定一个控件，然后才能设置左上点。",
				"提示信息",
				MB_OK | MB_ICONINFORMATION );
			return;
		}
		pCtrl->m_ControlInfo.nX2 = pCtrl->m_ControlInfo.width() + x;
		pCtrl->m_ControlInfo.nY2 = pCtrl->m_ControlInfo.height() + y;
		pCtrl->m_ControlInfo.nX1 = x;
		pCtrl->m_ControlInfo.nY1 = y;
	}

	// 刷新控件属性列表
	RenewAttribute ();
}

// 设置右下点
void CCtrlMgt::SetBottomRightPoint(int x, int y)
{
	if (! m_bIsSetTabMode)
	{
		// 提示用户必须选择一个控件
		CCtrl* pCtrl = NULL;
		int nIndex;
		if (! GetCurSel(&pCtrl, &nIndex))
		{
			MessageBox (m_pView->m_hWnd,
				"您必须先选定一个控件，然后才能设置右下点。",
				"提示信息",
				MB_OK | MB_ICONINFORMATION);
			return;
		}

		int nTempCX = x - (pCtrl->m_ControlInfo.nX1);
		int nTempCY = y - (pCtrl->m_ControlInfo.nY1);

		// 位置必须在左上点的右下方
		if (nTempCX >= 0 && nTempCY >= 0)
		{
			pCtrl->m_ControlInfo.nX2 = pCtrl->m_ControlInfo.nX1 + nTempCX;
			pCtrl->m_ControlInfo.nY2 = pCtrl->m_ControlInfo.nY1 + nTempCY;
		}

		SetDefaultSize(pCtrl);
	}

	// 刷新控件属性列表
	RenewAttribute ();
}

// 选取
BOOL CCtrlMgt::Select(int x, int y)
{
	if (m_bIsSetTabMode)
	{
		// 处理Tab序号
		// 如果没有选中则不处理
		// 全部设置为“未选中”
		int i;
		for (i=0; i<m_nCount; i++ )
		{
			m_pCtrls[i]->SetSelState(FALSE);
		}

		// 查找选中的控件
		int nIndex = -1;
		for (i = 0; i < m_nCount; i++)
		{
			if (! m_pCtrls[i]->IsDialog() && //对话框不参与排号
				m_pCtrls[i]->PtInArea(x, y))
			{
				nIndex = i;
				break;
			}
		}

		// 没有被选中的控件
		if( nIndex == -1 )
		{
			// 说明鼠标点在了所有控件之外，关闭排号模式
			InvertTabMode();

			// 刷新控件属性列表
			RenewAttribute ();
			return FALSE;
		}

		// 将选中的控件搬到控件列表的Index=0端
		CCtrl* pCtrl = m_pCtrls[nIndex];
		for( i=nIndex; i>0; i-- )
		{
			m_pCtrls[i] = m_pCtrls[i-1];
		}
		m_pCtrls[0] = pCtrl;

		// 重新设置所有控件的Tab数值，Index=0端的Tab数值最大，依次类推
		for( i=0; i<m_nCount; i++ )
		{
			m_pCtrls[i]->SetTabNum( m_nCount-i-1 );
		}

		// 将处于Index=0位置的控件设置为选中状态
		m_pCtrls[0]->SetSelState(TRUE);

		// 刷新控件属性列表
		RenewAttribute ();
		return TRUE;
	}
	else
	{
		// 处理普通的选取问题
		// 全部设置为“未选中”
		int i;
		for (i = 0; i < m_nCount; i++)
		{
			m_pCtrls[i]->SetSelState(FALSE);
		}

		//
		for (i = 0; i < m_nCount; i++)
		{
			if (m_pCtrls[i]->PtInArea(x, y))	// 只能选中一个
			{
				m_pCtrls[i]->SetSelState(TRUE);

				// 刷新控件属性列表
				RenewAttribute ();
				return TRUE;
			}
		}

		// 刷新控件属性列表
		RenewAttribute ();
		return FALSE;
	}
}

// 得到处于选取状态的控件的指针
BOOL CCtrlMgt::GetCurSel( CCtrl** pCtrl, int* pnIndex )
{
	for( int i=0; i<m_nCount; i++ )
	{
		if( m_pCtrls[i]->GetSelState() )
		{
			* pnIndex = i;
			* pCtrl = m_pCtrls[i];
			return TRUE;
		}
	}
	return FALSE;
}

// 翻转Tab模式的状态
BOOL CCtrlMgt::InvertTabMode()
{
	int i;
	if( m_bIsSetTabMode )
	{
		m_bIsSetTabMode = FALSE;
		// 关闭Tab设置模式时，应调整Tab序号
		// 寻找对话框
		int nDialogTabOrder = 0;
		for (i=0; i<m_nCount; i++ )
		{
			if( m_pCtrls[i]->m_ControlInfo.nType == WND_TYPE_DIALOG )
			{
				nDialogTabOrder = m_pCtrls[i]->GetTabNum();
				break;
			}
		}

		// 如果对话框的Tab序号不是0，则提示用户ID号变更
		if( nDialogTabOrder == 0 )
		{
			return m_bIsSetTabMode;
		}
		else
		{
			MessageBox( m_pView->m_hWnd, "对话框的ID号不是0\n程序已将对话框ID号码调整为0。", "错误信息", MB_OK | MB_ICONEXCLAMATION );

			// 重编Tab编号
			for( i=0; i<m_nCount; i++ )
			{
				int nTabNum = m_pCtrls[i]->GetTabNum();
				if( nTabNum == nDialogTabOrder )
				{
					m_pCtrls[i]->SetTabNum( 0 );
				}
				else if( nTabNum < nDialogTabOrder )
				{
					m_pCtrls[i]->SetTabNum( nTabNum + 1 );
				}
			}
		}
	}
	else
	{
		m_bIsSetTabMode = TRUE;
	}
	return m_bIsSetTabMode;
}

// 刷新属性列表的显示
void CCtrlMgt::RenewAttribute()
{
	// 得到处于选中状态的控件，将其指针传递给属性列表
	CCtrl* pCtrl = NULL;
	int nIndex;
	if( GetCurSel( &pCtrl, &nIndex ) )
	{
		m_pAttributeDlg->Renew( TRUE );
	}
	else
	{
		m_pAttributeDlg->Renew( FALSE );
	}
}

// 修改属性（属性列表用）
BOOL CCtrlMgt::ModifyDataBase( int nIndex, CString strValue )
{
	CCtrl* pCtrl;
	int nSel = 0;
	if (! GetCurSel(&pCtrl, &nSel))
	{
		return FALSE;
	}

	switch( nIndex )
	{
	case 1:		// nStyle
		{
			pCtrl->m_ControlInfo.nStyle = atoi( strValue );
			break;
		}
	case 2:		// nX
		{
			pCtrl->m_ControlInfo.nX1 = atoi( strValue );
			break;
		}
	case 3:		// nY
		{
			pCtrl->m_ControlInfo.nY1 = atoi( strValue );
			break;
		}
	case 4:		// nW
		{
			int w = atoi( strValue );
			pCtrl->m_ControlInfo.nX2 = pCtrl->m_ControlInfo.nX1 + w;
			break;
		}
	case 5:		// nH
		{
			int h = atoi( strValue );
			pCtrl->m_ControlInfo.nY2 = pCtrl->m_ControlInfo.nY1 + h;
			break;
		}
	case 6:		// nID
		{		// ID号互相不能重复
			int nTemp = atoi( strValue );
			if( nTemp != 0 )
			{
				for( int i=0; i<m_nCount; i++ )
				{

					if( (pCtrl != m_pCtrls[i]) && (m_pCtrls[i]->m_ControlInfo.nID == nTemp) )
					{
						MessageBox( m_pView->m_hWnd, "ID号重复，请换用其他数值！", "错误", MB_OK | MB_ICONINFORMATION );
						return FALSE;
					}
				}
			}
			pCtrl->m_ControlInfo.nID = nTemp;
			break;
		}
	case 7:		// psCaption
		{
			int nCount = strValue.GetLength();
			for( int i=0; i<nCount; i++ )
			{
				if( strValue.GetAt(i) == ';' )
				{
					MessageBox( m_pView->m_hWnd, "字符串中不能包含“;”符号！", "错误", MB_OK | MB_ICONINFORMATION );
					return FALSE;
				}
			}
			memset( (char *)(pCtrl->m_ControlInfo.psCaption), 0x0, 256 );
			strncpy( (char *)(pCtrl->m_ControlInfo.psCaption), strValue, strValue.GetLength() );
			break;
		}
	case 8:		// nAddData
		{
			pCtrl->m_ControlInfo.nAddData = atoi (strValue);
			break;
		}
	}

	pCtrl->ResizeLevelRects();
	return TRUE;
}

// 根据控件尺寸设置默认值
void CCtrlMgt::SetDefaultSize( CCtrl* pCtrl )
{
	if( pCtrl->m_ControlInfo.nType != WND_TYPE_DIALOG )
	{
		m_CX = pCtrl->m_ControlInfo.width();
		m_CY = pCtrl->m_ControlInfo.height();
	}
}

// 恢复到新建状态
BOOL CCtrlMgt::New()
{
	// 删除所有控件
	for( int i=0; i<m_nCount; i++ )
	{
		delete m_pCtrls[i];
		m_pCtrls[i] = NULL;
	}
	m_nCount = 0;

	// 添加一个对话框
	Add (WND_TYPE_DIALOG);

	// 修改对话框的属性
	m_pCtrls[0]->m_ControlInfo.nX1 = 20;
	m_pCtrls[0]->m_ControlInfo.nY1 = 20;
	m_pCtrls[0]->m_ControlInfo.nX2 = SCREEN_W - 20;
	m_pCtrls[0]->m_ControlInfo.nY2 = SCREEN_H - 20;
	m_pCtrls[0]->SetTabNum(0);
	m_pCtrls[0]->ResizeLevelRects();

	// 更新属性列表的显示
	RenewAttribute();

	// 清除快捷键列表
	m_pView->m_pAccellListDlg->RemoveAll();

	return TRUE;
}

// 给所有控件的Y值加TITLE_HEIGHT或者减TITLE_HEIGHT
void CCtrlMgt::AdjustTitle( BOOL bTitle )
{
	int i;
	for (i = 0; i < m_nCount; i++)
	{
		if (m_pCtrls[i]->m_ControlInfo.nType != WND_TYPE_DIALOG)
		{
			if (bTitle) {
				m_pCtrls[i]->m_ControlInfo.nY1 += TITLE_HEIGHT;
				m_pCtrls[i]->m_ControlInfo.nY2 += TITLE_HEIGHT;
			}
			else {
				m_pCtrls[i]->m_ControlInfo.nY1 -= TITLE_HEIGHT;
				m_pCtrls[i]->m_ControlInfo.nY2 -= TITLE_HEIGHT;
			}
		}
	}
}

// 从文件恢复模板
BOOL CCtrlMgt::Open( CString strFilePath )
{
	HANDLE hFile;
	hFile = CreateFile(strFilePath,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,NULL);
	if(hFile==INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	// 将文件打开到一个char数组中
	DWORD dwFileLength = GetFileSize (hFile, 0);
	char* pFile = new char[dwFileLength+1];
	memset (pFile, 0x0, dwFileLength+1);

	DWORD dwActualSize = 0;
	BOOL bReadSt;
	bReadSt = ReadFile (hFile, pFile, dwFileLength, &dwActualSize, NULL);
	CloseHandle (hFile);
	
	if (!bReadSt)
		return FALSE;
	if (dwActualSize != dwFileLength)
		return FALSE;

	CString strFile = pFile;
	delete pFile;

	// 恢复到初始状态（删除所有控件并生成一个默认的对话框）
	New();

	// 对话框的X和Y，控件的X和Y需要根据此数值转换成绝对坐标
	int nDialogX = 0;
	int nDialogY = 0;

	// 逐行解读模板文件
	int k = 0;
	CString strOneLine;
	while( GetLine( &strOneLine, strFile, k ) )
	{
		k ++;
		CString strTemp = strOneLine.Left(1);
		if( (strTemp != "#") && (strTemp != "") )	// 去掉注释行和空行
		{
			if( GetSlice( &strTemp, strOneLine, 0 ) )
			{
				if( strTemp == "DIALOG" )
				{
					// 解读对话框设置
					if( m_pCtrls[0]->Open( strOneLine ) )
					{
						// 读出对话框的X和Y，其他控件的X和Y需要据此进行修正
						nDialogX = m_pCtrls[0]->m_ControlInfo.nX1;
						nDialogY = m_pCtrls[0]->m_ControlInfo.nY1;
						if( (m_pCtrls[0]->m_ControlInfo.nStyle & WND_STYLE_NO_TITLE) == 0 )
						{
							nDialogY += TITLE_HEIGHT;
						}
					}
					else
					{
						MessageBox( m_pView->m_hWnd, "对话框模板文件格式错误\n——对话框配置字符串格式错误。", "错误信息", MB_OK | MB_ICONSTOP );
						return FALSE;
					}

					m_pCtrls[0]->ResizeLevelRects();
				}
				else if( strTemp == "CONTROL" )
				{
					// 添加一个控件
					Add(0);

					// 解读控件设置
					if( m_pCtrls[0]->Open( strOneLine ) )
					{
						// 修正X和Y
						m_pCtrls[0]->m_ControlInfo.nX1 += nDialogX;
						m_pCtrls[0]->m_ControlInfo.nX2 += nDialogX;
						m_pCtrls[0]->m_ControlInfo.nY1 += nDialogY;
						m_pCtrls[0]->m_ControlInfo.nY2 += nDialogY;

						m_pCtrls[0]->ResizeLevelRects();
					}
					else
					{
						MessageBox( m_pView->m_hWnd, "对话框模板文件格式错误\n——控件配置字符串格式错误。", "错误信息", MB_OK | MB_ICONSTOP );
						return FALSE;
					}
				}
				else if( strTemp == "ACCELL" )
				{
					// 解读快捷键列表
					if( ! m_pView->m_pAccellListDlg->Open( strOneLine ) )
					{
						MessageBox( m_pView->m_hWnd, "对话框模板文件格式错误\n——快捷键列表格式错误。", "错误信息", MB_OK | MB_ICONSTOP );
						return FALSE;
					}
				}
				else
				{
					MessageBox( m_pView->m_hWnd, "对话框模板文件格式错误\n——遇到不可解读的内容。", "错误信息", MB_OK | MB_ICONSTOP );
					return FALSE;
				}
			}
			else
			{
				MessageBox( m_pView->m_hWnd, "对话框模板文件格式错误\n——文件格式错误。", "错误信息", MB_OK | MB_ICONSTOP );
				return FALSE;
			}
		}
	}

	return TRUE;
}

// 将模板存入文件
BOOL CCtrlMgt::Save( CString strFilePath )
{
	CString strFile = "";
	CString strTemp = "";

	// 首先写入文件头
	strFile = "#  MonoGUI Dialog Templet File\r\n";
	strFile += "#  Create by DTEditor V1.0\r\n";
	strFile += "#\r\n";

	int nDialogX = 0;
	int nDialogY = 0;

	// 写入对话框属性
	int nDialogTabOrder = 0;
	int i;
	for (i=0; i<m_nCount; i++ )
	{
		if( m_pCtrls[i]->m_ControlInfo.nType == WND_TYPE_DIALOG )
		{
			nDialogX = m_pCtrls[i]->m_ControlInfo.nX1;
			nDialogY = m_pCtrls[i]->m_ControlInfo.nY1;
			if( (m_pCtrls[i]->m_ControlInfo.nStyle & WND_STYLE_NO_TITLE) == 0 )
			{
				nDialogY += TITLE_HEIGHT;
			}

			strTemp = m_pCtrls[i]->Save();
			strFile += strTemp;
			strFile += "\r\n";	// 回车换行符
			nDialogTabOrder = m_pCtrls[i]->GetTabNum();
			break;
		}
	}

	// 如果对话框的Tab序号不是0，则提示用户ID号变更
	if( nDialogTabOrder != 0 )
	{
		MessageBox( m_pView->m_hWnd, "对话框的ID号不是0\n程序已将对话框属性调整到第一行。", "错误信息", MB_OK | MB_ICONSTOP );
	}

	// 写入控件属性
	// 按照Tab编号从小到大依次存储
	for( i=0; i<m_nCount; i++ )
	{
		for( int j=0; j<m_nCount; j++ )
		{
			if( m_pCtrls[j]->GetTabNum() == i )
			{
				// 对话框不加入控件列表
				if( m_pCtrls[j]->GetTabNum() != nDialogTabOrder )
				{
					// 修正X和Y
					m_pCtrls[j]->m_ControlInfo.nX1 -= nDialogX;
					m_pCtrls[j]->m_ControlInfo.nX2 -= nDialogX;
					m_pCtrls[j]->m_ControlInfo.nY1 -= nDialogY;
					m_pCtrls[j]->m_ControlInfo.nY2 -= nDialogY;

					strTemp = m_pCtrls[j]->Save();
					strFile += strTemp;
					strFile += "\r\n";	// 回车换行符

					// 恢复X和Y
					m_pCtrls[j]->m_ControlInfo.nX1 += nDialogX;
					m_pCtrls[j]->m_ControlInfo.nX2 += nDialogX;
					m_pCtrls[j]->m_ControlInfo.nY1 += nDialogY;
					m_pCtrls[j]->m_ControlInfo.nY2 += nDialogY;
				}
			}
		}
	}

	// 写入快捷键列表
	strTemp = m_pView->m_pAccellListDlg->Save();
	strFile += strTemp;
	strFile += "\r\n";	// 回车换行符

	// 写入磁盘文件
	HANDLE hFile;
	hFile = CreateFile(strFilePath,GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ,0,CREATE_ALWAYS,0,NULL);
	if(hFile==INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	DWORD dwWritten = 0;
	BOOL bWriteSt;
	bWriteSt = WriteFile (hFile, strFile, strFile.GetLength(), &dwWritten, NULL);
	CloseHandle (hFile);
	
	if (!bWriteSt)
		return FALSE;
	if (dwWritten != (DWORD)strFile.GetLength())
		return FALSE;

	return TRUE;
}

// 设置拖拽修改的模式（移动操作需要原点）
void CCtrlMgt::SetMouseDragMode( int nMode, int x, int y )
{
	// 如果处于Tab编号编辑状态，退出
	if( m_bIsSetTabMode )
	{
		return;
	}

	m_nDragMode = nMode;
	m_nOrgX 	= x;
	m_nOrgY		= y;
}

// 处理鼠标拖拽操作
void CCtrlMgt::MouseDrag( int x, int y )
{
	// 如果处于Tab编号编辑状态，退出
	if( m_bIsSetTabMode )
	{
		return;
	}

	CCtrl* pCtrl = NULL;
	int nIndex;
	if( !GetCurSel( &pCtrl, &nIndex ) )
	{
		return;
	}

	// 得到鼠标的移动距离
	int nDX = x - m_nOrgX;
	int nDY = y - m_nOrgY;

	pCtrl->MouseDrag( m_nDragMode, nDX, nDY );

	SetDefaultSize( pCtrl );

	// 修改默认长度和高度
	SetDefaultSize( pCtrl );

	// 将当前点设置为原点
	m_nOrgX = x;
	m_nOrgY	= y;
}

/* END */