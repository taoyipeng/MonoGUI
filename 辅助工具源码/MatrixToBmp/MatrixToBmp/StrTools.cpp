// StrTools.cpp

#include "stdafx.h"
#include "StrTools.h"


CString GetFileName(CString& strFullPath)
{
	CString s;
	int nPos = 0;
	int nLeft = 0;
	while(1){
		nLeft = strFullPath.Find('\\', nPos);
		if( nLeft < 0 )
			break;
		nPos = nLeft+1;
	}
	s = strFullPath.Mid(nPos);
	return s;
}
CString GetFilePath(CString& strFullPath)
{
	CString s;
	int nPos = 0;
	int nLeft = 0;
	while(1){
		nLeft = strFullPath.Find('\\', nPos);
		if( nLeft < 0 )
			break;
		nPos = nLeft+1;
	}
	s = strFullPath.Left(nPos);
	return s;
}
CString TrimAll(const CString& rs)
{
	CString s(rs);
	s.TrimLeft();
	s.TrimRight();
	return s;
}
int Split(CStringArray& rAr, LPCTSTR pszSrc, LPCTSTR pszDelimiter)
{
	int n = (int)_tcslen(pszDelimiter);
	rAr.RemoveAll();
	if( n<=0 ){
		rAr.Add(TrimAll(CString(pszSrc)));
		return (int)rAr.GetSize();
	}
	CString s(pszSrc);
	while( s.GetLength() ){
		int iPos = s.Find(pszDelimiter);
		if( iPos<0 ){
			rAr.Add(TrimAll(s));
			break;
		}
		rAr.Add(TrimAll(s.Left(iPos)));
		s = s.Mid(iPos + n);
	}
	return (int)rAr.GetSize();
}
CString AtoW(BYTE* asc_str, int len)
{
	WCHAR* wc = new WCHAR[len + 1];
	MultiByteToWideChar(CP_OEMCP, NULL, (char*)asc_str, len, wc, len + 1);
	CString ret_str(wc);
	delete[] wc;
	return ret_str;
}
