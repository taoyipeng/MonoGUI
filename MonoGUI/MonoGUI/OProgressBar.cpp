////////////////////////////////////////////////////////////////////////////////
// @file OProgressBar.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OProgressBar::OProgressBar ()
	: OWindow(self_type)
{
	m_dwAddData1 = 1;
}

OProgressBar::~OProgressBar ()
{
}

// 创建进度条
BOOL OProgressBar::Create (OWindow* pParent,WORD wStyle,WORD wStatus,int x,int y,int w,int h,int ID)
{
	if (!OWindow::Create(pParent, wStyle, wStatus, x, y, w, h, ID)) {
		return FALSE;
	}

	m_wStatus |= WND_STATUS_INVALID;	// 进度条不能获得焦点
	return TRUE;
}

void OProgressBar::Paint (LCD* pLCD)
{
	CHECK_TYPE;

	// 如果不可见，则什么也不绘制
	if (! IsWindowVisible()) {
		return;
	}

	int crBk = 1;
	int crFr = 0;

	// 绘制边框，填充背景
	pLCD->FillRect (m_x, m_y, m_w, m_h, 0);
	if ((m_wStyle & WND_STYLE_NO_BORDER) == 0)
	{
		pLCD->HLine (m_x, m_y, m_w, crFr);
		pLCD->HLine (m_x, m_y+m_h-1, m_w, crFr);
		pLCD->VLine (m_x, m_y, m_h, crFr);
		pLCD->VLine (m_x+m_w-1, m_y, m_h, crFr);
	}

	// 绘制背景条
	pLCD->FillRect (m_x+2, m_y+2, m_w-4, m_h-4, 2);

	// 绘制前景条
	int nBarWidth = (int)((m_w-4) * m_dwAddData2 / m_dwAddData1);
	pLCD->FillRect (m_x+2, m_y+2, nBarWidth, m_h-4, crBk);
}

int OProgressBar::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;
	return 0;
}

// 设置整体范围
BOOL OProgressBar::SetRange (int nRange)
{
	CHECK_TYPE_RETURN;

	if (nRange > 1)
	{
		m_dwAddData1 = nRange;
		if (m_dwAddData2 > m_dwAddData1)
			m_dwAddData2 = m_dwAddData1;

		// 进度条的修改应当立即被更新显示
		Paint (m_pApp->m_pLCD);
		m_pApp->Show();

		return TRUE;
	}
	return FALSE;
}

// 设置当前进度
BOOL OProgressBar::SetPos (int nPos)
{
	CHECK_TYPE_RETURN;

	if (nPos >= 0)
	{
		m_dwAddData2 = nPos;
		if (m_dwAddData2 > m_dwAddData1)
			m_dwAddData2 = m_dwAddData1;

		// 进度条的修改应当立即被更新显示
		Paint(m_pApp->m_pLCD);
		m_pApp->Show();

		return TRUE;
	}
	return FALSE;
}

/* END */