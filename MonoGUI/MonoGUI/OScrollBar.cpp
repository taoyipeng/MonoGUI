////////////////////////////////////////////////////////////////////////////////
// @file OScrollBar.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OScrollBar::OScrollBar ()
{
	m_nStatus = 0;		// 0：不显示；1：显示
	m_nMode	  = 0;		// 1：垂直滚动条；2：水平滚动条
	m_x		  = 0;
	m_y		  = 0;
	m_w		  = 0;
	m_h		  = 0;		// 位置
	m_nRange  = 2;		// 移动范围
	m_nSize   = 1;		// 中间按钮的大小
	m_nPos	  = 0;		// 当前位置

#if defined (MOUSE_SUPPORT)
	m_nOldPos = 0;
	m_nOldPt  = 0;
#endif // defined(MOUSE_SUPPORT)
}

OScrollBar::~OScrollBar ()
{
}

// 创建滚动条（应指定水平还是垂直）
BOOL OScrollBar::Create (int nMode, int x, int y, int w, int h, int nRange, int nSize, int nPos)
{
	m_x  = x;
	m_y  = y;

	if (nMode == 1)	// 垂直滚动条
	{
		m_w = 13;
		m_h = h;
		if (m_h < 20)
			m_h = 20;
	}
	else if (nMode == 2)
	{
		m_w = w;
		m_h = 13;
		if (m_w < 20)
			m_w = 20;
	}
	else
	{
		DebugPrintf("ERROR: OScrollBar::Create parameter nMode error!\n");
		return FALSE;
	}

	m_nStatus = 1;
	m_nMode	  = nMode;

	if (! SetRange (nRange)) {
		DebugPrintf("ERROR: OScrollBar::Create SetRange() error!\n");
		return FALSE;
	}

	if (! SetSize (nSize)) {
		DebugPrintf("ERROR: OScrollBar::Create SetSize() error!\n");
		return FALSE;
	}

	if (! SetPos (nPos)) {
		DebugPrintf("ERROR: OScrollBar::Create SetPos() error!\n");
		return FALSE;
	}

	return TRUE;
}

// 绘制滚动条
void OScrollBar::Paint (LCD* pLCD)
{
	if (m_nStatus == 0)		// 不显示
		return;

	int crBk = 1;
	int crFr = 0;

	// 绘制边框
	pLCD->HLine (m_x, m_y, m_w, crFr);
	pLCD->HLine (m_x, m_y+m_h-1, m_w, crFr);
	pLCD->VLine (m_x, m_y, m_h, crFr);
	pLCD->VLine (m_x+m_w-1, m_y, m_h, crFr);

	// 填充中间的部分
	pLCD->FillRect (m_x+1, m_y+1, m_w-2, m_h-2, crBk);

	// 绘制上下箭头
	if (m_nMode == 1)		// 垂直滚动条
	{
		pLCD->DrawImage (m_x+3, m_y+3, 7, 7,
				g_4color_Arror_Up, 0, 0, LCD_MODE_NORMAL);
		pLCD->DrawImage (m_x+3, m_y+m_h-10, 7, 7,
				g_4color_Arror_Down, 0, 0, LCD_MODE_NORMAL);
	}
	else if (m_nMode == 2)	// 水平滚动条
	{
		pLCD->DrawImage (m_x+3, m_y+3, 7, 7,
				g_4color_Arror_Left, 0, 0, LCD_MODE_NORMAL);
		pLCD->DrawImage (m_x+m_w-10, m_y+3, 7, 7,
				g_4color_Arror_Right, 0, 0, LCD_MODE_NORMAL);
	}

	// 绘制中间的按钮
	if (m_nMode == 1)	// 垂直
	{
		int nTopPos = m_y + 11;
		int nBottomPos = m_y + m_h - 11;
		int nHeight = nBottomPos - nTopPos;

		// 绘制黑白相间的底纹
		pLCD->FillRect (m_x+1, nTopPos, m_w-2, nHeight, 2);

		if (nHeight < 14)
		{
			// 没有空间绘制按钮
			return;
		}

		// 计算按钮的绘制位置和长宽
		int nBtnHeight = nHeight * m_nSize / m_nRange;	// 高度
		if (nBtnHeight < 11)		// 中间按钮至少11个像素的宽度
		{
			nBtnHeight = 11;
		}
		int nBtnTop    = nTopPos + (nHeight - nBtnHeight) * m_nPos / (m_nRange - m_nSize);
		int nBtnButtom = nBtnTop + nBtnHeight;

		// 绘制按钮
		pLCD->DrawImage (m_x+1, nBtnTop, 11, 4,
				g_4color_Scroll_Button, 0, 0, LCD_MODE_NORMAL);
		pLCD->DrawImage (m_x+1, nBtnButtom-4, 11, 4,
				g_4color_Scroll_Button, 0, 7, LCD_MODE_NORMAL);
		pLCD->FillRect (m_x+1, nBtnTop+4, m_w-2, nBtnButtom-nBtnTop-8, crBk);
		pLCD->VLine (m_x+2, nBtnTop+4, nBtnButtom-nBtnTop-8, crFr);
		pLCD->VLine (m_x+m_w-3, nBtnTop+4, nBtnButtom-nBtnTop-8, crFr);

		// 绘制按钮上的两条线
		pLCD->HLine (m_x+4, (nBtnTop+nBtnButtom)/2-1, 5, crFr);
		pLCD->HLine (m_x+4, (nBtnTop+nBtnButtom)/2+1, 5, crFr);
	}
	else if (m_nMode == 2)	// 水平
	{
		int nLeftPos = m_x + 11;
		int nRightPos = m_x + m_w - 11;
		int nWidth = nRightPos - nLeftPos;

		// 绘制黑白相间的底纹
		pLCD->FillRect (nLeftPos, m_y+1, nWidth, m_h-2, 2);

		if (nWidth < 14)
		{
			// 没有空间绘制按钮
			return;
		}

		// 计算按钮的绘制位置和长宽
		int nBtnWidth = nWidth * m_nSize / m_nRange;	// 高度
		if (nBtnWidth < 11)		// 中间按钮至少11个像素的宽度
		{
			nBtnWidth = 11;
		}
		int nBtnLeft  = nLeftPos + (nWidth-nBtnWidth) * m_nPos / (m_nRange-m_nSize);
		int nBtnRight = nBtnLeft + nBtnWidth;

		// 绘制按钮
		pLCD->DrawImage (nBtnLeft, m_y+1, 4, 11,
				g_4color_Scroll_Button, 0, 0, LCD_MODE_NORMAL);
		pLCD->DrawImage (nBtnRight-4, m_y+1, 4, 11,
				g_4color_Scroll_Button, 7, 0, LCD_MODE_NORMAL);
		pLCD->FillRect (nBtnLeft+4, m_y+1, nBtnRight-nBtnLeft-8, m_h-2, crBk);
		pLCD->HLine (nBtnLeft+4, m_y+2, nBtnRight-nBtnLeft-8, crFr);
		pLCD->HLine (nBtnLeft+4, m_y+m_h-3, nBtnRight-nBtnLeft-8, crFr);

		// 绘制按钮上的两条线
		pLCD->VLine ((nBtnLeft+nBtnRight)/2-1, m_y+4, 5, crFr);
		pLCD->VLine ((nBtnLeft+nBtnRight)/2+1, m_y+4, 5, crFr);
	}
}

// 设置滚动范围
BOOL OScrollBar::SetRange (int nRange)
{
	if (nRange > 2)	// 如果小于2会导致除数为0错误
	{
		m_nRange = nRange;
		SetSize (m_nSize);
		SetPos (m_nPos);
		return TRUE;
	}
	return FALSE;
}

// 设置中间按钮的大小
BOOL OScrollBar::SetSize (int nSize)
{
	if (nSize > 1)
	{
		m_nSize = nSize;
		if (m_nSize > m_nRange)
		{
			m_nSize = m_nRange;
		}
		return TRUE;
	}
	return FALSE;
}

// 设置当前位置
BOOL OScrollBar::SetPos (int nPos)
{
	if (nPos >= 0)
	{
		m_nPos = nPos;
		if (m_nPos > (m_nRange-1))
		{
			m_nPos = m_nRange - 1;
		}
		return TRUE;
	}
	return FALSE;
}

#if defined (MOUSE_SUPPORT)
// 记录当前位置和当前坐标
BOOL OScrollBar::RecordPos (int nPt)
{
	if (m_nStatus == 0)     // 不显示
		return FALSE;

	m_nOldPos = m_nPos;
	m_nOldPt = nPt;
	return TRUE;
}

// 判断鼠标落点
int OScrollBar::TestPt (int x, int y)
{
	if (m_nStatus == 0)     // 不显示
		return 0;

	int L = m_x;
	int T = m_y;
	int R = m_x+m_w;
	int B = m_y+m_h;

	if ( !((x >= L) && (y >= T) && (x <= R) && (y <= B)))
		return 0;

	if (m_nMode == 1)       // 垂直
	{
		int nTopPos = m_y + 11;
		int nBottomPos = m_y + m_h - 11;
		int nBtnTop = 0;
		int nBtnButtom = 0;

		int nHeight = nBottomPos - nTopPos;
		if (nHeight >= 14)
		{
			// 计算按钮的绘制位置和长宽
			int nBtnHeight = nHeight * m_nSize / m_nRange;	// 高度
			if (nBtnHeight < 11)		// 中间按钮至少11个像素的宽度
			{
				nBtnHeight = 11;
			}
			nBtnTop    = nTopPos + (nHeight - nBtnHeight) * m_nPos / (m_nRange-m_nSize);
			nBtnButtom = nBtnTop + nBtnHeight;
		}

		B = nTopPos;
		if (PtInRect (x,y,L,T,R,B))
			return SCROLL_PT_UP;
		T = nBottomPos;
		B = m_y+m_h;
		if (PtInRect (x,y,L,T,R,B))
			return SCROLL_PT_DOWN;
		if ((nBtnTop != 0) && (nBtnButtom != 0))
		{
			T = nTopPos;
			B = nBtnTop;
			if (PtInRect (x,y,L,T,R,B))
				return SCROLL_PT_PAGEUP;
			T = nBtnButtom;
			B = nBottomPos;
			if (PtInRect (x,y,L,T,R,B))
				return SCROLL_PT_PAGEDOWN;
			T = nBtnTop;
			B = nBtnButtom;
			if (PtInRect (x,y,L,T,R,B))
				return SCROLL_PT_BUTTON;
		}
	}
	else if (m_nMode == 2)  // 水平
	{
		int nLeftPos = m_x + 11;
		int nRightPos = m_x + m_w - 11;
		int nBtnLeft = 0;
		int nBtnRight = 0;

		int nWidth = nRightPos - nLeftPos;
		if (nWidth >= 14)
		{
			// 计算按钮的绘制位置和长宽
			int nBtnWidth = nWidth * m_nSize / m_nRange;	// 高度
			if (nBtnWidth < 11)		// 中间按钮至少11个像素的宽度
			{
				nBtnWidth = 11;
			}
			nBtnLeft  = nLeftPos + (nWidth-nBtnWidth) * m_nPos / (m_nRange-m_nSize);
			nBtnRight = nBtnLeft + nBtnWidth;
		}

		R = nLeftPos;
		if (PtInRect (x,y,L,T,R,B))
			return SCROLL_PT_UP;
		L = nRightPos;
		R = m_x+m_w;
		if (PtInRect (x,y,L,T,R,B))
			return SCROLL_PT_DOWN;
		if ((nBtnLeft != 0) && (nBtnRight != 0))
		{
			L = nLeftPos;
			R = nBtnLeft;
			if (PtInRect (x,y,L,T,R,B))
				return SCROLL_PT_PAGEUP;
			L = nBtnRight;
			R = nRightPos;
			if (PtInRect (x,y,L,T,R,B))
				return SCROLL_PT_PAGEDOWN;
			L = nBtnLeft;
			R = nBtnRight;
			if (PtInRect (x,y,L,T,R,B))
				return SCROLL_PT_BUTTON;
		}
	}

	return 0;
}

// 根据鼠标坐标计算对应的新位置
int OScrollBar::TestNewPos (int x, int y)
{
	if (m_nStatus == 0)		// 不显示
		return -1;

	if (m_nMode == 1)		// 垂直滚动条
	{
		// 计算鼠标有效范围
		int L = m_x-30;
		int T = m_y-10;
		int R = m_x+m_w+30;
		int B = m_y+m_h+10;

		if (! PtInRect (x,y,L,T,R,B))
			return m_nOldPos;

		int nTopPos = m_y+11;
		int nBottomPos = m_y+m_h-11;
		int nHeight = nBottomPos - nTopPos;
		if (nHeight >= 14)
		{
			// 计算按钮的绘制位置和长宽
			int nBtnHeight = nHeight * m_nSize / m_nRange;	// 高度
			if (nBtnHeight < 11)		// 中间按钮至少11个像素的宽度
			{
				nBtnHeight = 11;
			}

			int nNewPos = m_nOldPos-(m_nOldPt-y)*(m_nRange-m_nSize)/(nHeight-nBtnHeight);

			if (nNewPos < 0)
				nNewPos = 0;
			else if (nNewPos > (m_nRange-m_nSize) )
				nNewPos = m_nRange - m_nSize;

			return nNewPos;
		}
	}
	else if (m_nMode == 2)	// 水平滚动条
	{
		// 计算鼠标有效范围
		int L = m_x-10;
		int T = m_y-30;
		int R = m_x+m_w+10;
		int B = m_y+m_h+30;

		if (! PtInRect (x,y,L,T,R,B))
			return m_nOldPos;

		int nLeftPos = m_x+11;
		int nRightPos = m_x+m_w-11;
		int nWidth = nRightPos - nLeftPos;
		if (nWidth >= 14)
		{
			// 计算按钮的绘制位置和长宽
			int nBtnWidth = nWidth * m_nSize / m_nRange;	// 高度
			if (nBtnWidth < 11)		// 中间按钮至少11个像素的宽度
			{
				nBtnWidth = 11;
			}

			int nNewPos = m_nOldPos-(m_nOldPt-x)*(m_nRange-m_nSize)/(nWidth-nBtnWidth);

			if (nNewPos < 0)
				nNewPos = 0;
			else if (nNewPos > (m_nRange - m_nSize))
				nNewPos = m_nRange - m_nSize;

			return nNewPos;
		}
	}

	return -1;
}
#endif // defined(MOUSE_SUPPORT)

/* END */