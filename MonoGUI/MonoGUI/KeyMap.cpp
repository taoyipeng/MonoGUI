////////////////////////////////////////////////////////////////////////////////
// @file KeyMap.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

// 定义成员函数
KeyMap::KeyMap()
{
}

KeyMap::~KeyMap()
{
}

BOOL KeyMap::Load (char* sFileName)
{
#if defined (RUN_ENVIRONMENT_LINUX)
	// 打开文件
	FILE* fp = fopen (sFileName, "r");
	if (fp == NULL)
	{
		DebugPrintf ("ERROR: KeyMap::Load fp == NULL sFileName = %s\n", sFileName);
		return FALSE;
	}

	// 得到文件长度
    if (fseek(fp, 0, SEEK_END))
	{
		DebugPrintf ("ERROR: KeyMap::Load fseek SEEK_END error!\n");
		fclose (fp);
		return FALSE;
	}

	DWORD dwFileLen = ftell (fp);

	// 创建一个与文件等长的数组
	char* pcaFile = new char[dwFileLen];

	// 将文件内容读入数组
    if (fseek(fp, 0, SEEK_SET))
	{
		DebugPrintf ("ERROR: KeyMap::Load fseek SEEK_SET error!\n");
		fclose (fp);
		return FALSE;
	}

	fread (pcaFile, sizeof(char), dwFileLen, fp);

	// 关闭文件
	fclose (fp);
#endif // defined(RUN_ENVIRONMENT_LINUX)

#if defined (RUN_ENVIRONMENT_WIN32)
	// 打开文件
	HANDLE hFile;

	hFile = CreateFile (sFileName, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
		return FALSE;

	DWORD dwFileSize = GetFileSize (hFile, 0);

	// 创建一个数组存放文件内容
	char* pcaFile = new char[dwFileSize];

	// 将文件内容读入数组
	DWORD dwActuallSize;
	BOOL bReadSt;
	bReadSt = ReadFile (hFile, pcaFile, dwFileSize, &dwActuallSize, NULL);
	CloseHandle (hFile);

	if (! bReadSt || (dwActuallSize != dwFileSize))
	{
		delete [] pcaFile;
		return FALSE;
	}
#endif // defined(RUN_ENVIRONMENT_WIN32)

	// 用于存放一行内容的缓冲区
	char psTemp[256];
	char psKeyName[32];
	char psKeyValue[8];

	m_nCount = 0;
	// 逐行解析文件
	int k = 0;
	while (GetLine(pcaFile, psTemp, sizeof(psTemp), k))
	{
		// 如果开头不是“#define ”则跳过
		if (strncmp(psTemp, "#define", 7) != 0)
		{
			k ++;
			continue;
		}

		memset (psKeyName, 0x0, 32);
		memset (psKeyValue, 0x0, 8);

		int t = 7;
		int cur = 0;

		int nMaxLen = strlen (psTemp);

		// 找出宏名称
		while ((psTemp[t] == ' ')  ||
			   (psTemp[t] == '\t') ||
			   (psTemp[t] == '/')  ||
			   (psTemp[t] == '\0'))
		{
			t ++;
			if (t > nMaxLen)
				break;
		}

		if (t > nMaxLen)
		{
			k ++;
			continue;
		}

		while ((psTemp[t] != ' ')  &&
			   (psTemp[t] != '\t') &&
			   (psTemp[t] != '/')  &&
			   (psTemp[t] != '\0'))
		{
			psKeyName[cur] = psTemp[t];
			t ++;
			cur ++;

			if (t > nMaxLen)
				break;
		}

		if (t > nMaxLen)
		{
			k ++;
			continue;
		}

		psKeyName[cur] = '\0';

		// 找出宏值
		cur = 0;

		while ((psTemp[t] == ' ')  ||
			   (psTemp[t] == '\t') ||
			   (psTemp[t] == '/')  ||
			   (psTemp[t] == '\0'))
		{
			t ++;
			if (t > nMaxLen)
				break;
		}

		if (t > nMaxLen)
		{
			k ++;
			continue;
		}

		while ((psTemp[t] != ' ')  &&
			   (psTemp[t] != '\t') &&
			   (psTemp[t] != '/')  &&
			   (psTemp[t] != '\0'))
		{
			psKeyValue[cur] = psTemp[t];
			t ++;
			cur ++;

			if (t > nMaxLen)
				break;
		}

		if (t > nMaxLen)
		{
			k ++;
			continue;
		}

		psKeyValue[cur] = '\0';

		// 如果宏名或者宏指长度为零则进入下一个循环
		if (strlen(psKeyName) == 0)
		{
			k ++;
			continue;
		}

		if (strlen(psKeyValue) == 0)
		{
			k ++;
			continue;
		}

		int nKeyValue = 0;
		// 将宏值转换为数值
		// 区分十六进制数和十进制数
		if ((strncmp (psKeyValue, "0X", 2) == 0) ||
			(strncmp (psKeyValue, "0x", 2) == 0))
		{
			// 十六进制数
			char cc = psKeyValue[2];

			if (cc>='0' && cc<='9')
			{
				nKeyValue = (cc - '0');
			}
			else if (cc>='a' && cc<='f')
			{
				nKeyValue = 10 + (cc - 'a');
			}
			else if(cc>='A' && cc<='F')
			{
				nKeyValue = 10 + (cc - 'A');
			}
			else
			{
				// 存在非法字符
				k ++;
				continue;
			}

			cc =  psKeyValue[3];

			if (cc != '\0')
			{
				if (cc>='0' && cc<='9')
				{
					nKeyValue = nKeyValue * 16 + (cc - '0');
				}
				else if (cc>='a' && cc<='f')
				{
					nKeyValue = nKeyValue * 16 + 10 + (cc - 'a');
				}
				else if (cc>='A' && cc<='F')
				{
					nKeyValue = nKeyValue * 16 + 10 + (cc - 'A');
				}
				else
				{
					// 存在非法字符
					k ++;
					continue;
				}
			}
		}
		else
		{
			// 十进制数
			nKeyValue = atoi (psKeyValue);
			if(nKeyValue == 0)
			{
				k ++;
				continue;
			}
		}

		k ++;

		// 将数值添加到KeyMap数组中
		memset  (m_pstKeyMap[m_nCount].sKeyName, 0x0, 32);
		strncpy (m_pstKeyMap[m_nCount].sKeyName, psKeyName, 32);
		m_pstKeyMap[m_nCount].nKeyValue = nKeyValue;
		m_nCount ++;

		if (m_nCount == KEY_MAX)
			break;
	}

	delete [] pcaFile;
	return TRUE;
}

int KeyMap::Find (char* sKeyName)
{
	int i;
	for (i = 0; i < m_nCount; i++)
	{
		if (strncmp (sKeyName, m_pstKeyMap[i].sKeyName, 32) == 0)
		{
			return m_pstKeyMap[i].nKeyValue;
		}
	}

	return -1;
}

int  KeyMap::GetCount()
{
	return m_nCount;
}

BOOL KeyMap::GetName (int nIndex, char* sKeyName)
{
	if ((nIndex < 0) || (nIndex >= m_nCount))
		return FALSE;

	int nLen = strlen (m_pstKeyMap[nIndex].sKeyName);
	strncpy (sKeyName, m_pstKeyMap[ nIndex ].sKeyName, 32);
	sKeyName[nLen] = '\0';

	return TRUE;
}

/* END */
