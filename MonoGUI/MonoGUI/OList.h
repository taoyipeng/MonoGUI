////////////////////////////////////////////////////////////////////////////////
// @file OList.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined(__OLIST_H__)
#define __OLIST_H__

typedef struct _LISTCONTENT
{
	char text[LIST_TEXT_MAX_LENGTH];	// 保存列表框中一项文本内容
	struct _LISTCONTENT *next;			// 链表中下一个列表框中内容
} LISTCONTENT;							// 用于保存一条列表框中内容的数据结构


class OList : public OWindow  
{
private:
	enum { self_type = WND_TYPE_LIST };

	LISTCONTENT* m_pContent;	// 指向数据链表

public:
	OList ();
	virtual ~OList ();

public:
	// 创建窗口
	virtual BOOL Create
	(
		OWindow* pParent,			// 父窗口指针
		WORD wStyle,				// 窗口的样式
		WORD wStatus,				// 窗口的状态
		int x,
		int y,
		int w,
		int h,						// 绝对位置
		int ID						// 窗口的ID号
	);

	// 绘制输入法窗口
	virtual void Paint (LCD* pLCD);

	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);

#if defined (MOUSE_SUPPORT)
	// 坐标设备消息处理
	virtual int PtProc(OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);

	// 测试坐标落于的条目，-1表示未落于任何条目
	int PtInItems (int x, int y);
#endif // defined(MOUSE_SUPPORT)

	// 获得条目的数量
	int GetCount();

	// 获得当前显示区域最上面一个条目的Index
	int GetTopIndex();

	// 设置当前显示区域最上面一个条目的Index
	int SetTopIndex (int nIndex);

	// 得到当前选中项目的Index，如果没有选中的则返回-1
	int GetCurSel();

	// 设置当前的选中项目
	int SetCurSel (int nIndex);

	// 获得某一列表项的内容
	BOOL GetString (int nIndex, char* pText);

	// 设置某一列表项的内容
	BOOL SetString (int nIndex, char* pText);

	// 获得某一列表项内容的长度
	int GetStringLength (int nIndex);

	// 向列表框中添加一个串(加在末尾)
	BOOL AddString (char* pText);

	// 删除一个列表项
	BOOL DeleteString (int nIndex);

	// 在指定位置插入一个串
	BOOL InsertString (int nIndex, char* pText);

	// 删除所有列表项
	BOOL RemoveAll();

	// 在列表项中查找一个串
	int FindString (char* pText);

	// 在列表项中查找一个串，如果找到，则将它设置为选中，并显示在第一行
	// (如果在最后一页，则不必放在第一行，详见软件文档)
	int SelectString (char* pText);

	// 调整列表框的高度为整行
	BOOL SetLinage (int nLinage);

	// 更新滚动条
	void RenewScroll();

#if defined (MOUSE_SUPPORT)
	// 与滚动条有关的函数
	virtual void OnScrollUp ();
	virtual void OnScrollDown ();
	virtual void OnScrollPageUp ();
	virtual void OnScrollPageDown ();
	virtual void OnVScrollNewPos (int nNewPos);
#endif // defined(MOUSE_SUPPORT)
};

#endif // !defined(__OLIST_H__)
