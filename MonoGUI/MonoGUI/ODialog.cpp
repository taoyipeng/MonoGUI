////////////////////////////////////////////////////////////////////////////////
// @file ODialog.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if defined(RUN_ENVIRONMENT_LINUX)
#include <fcntl.h>
#endif // defined(RUN_ENVIRONMENT_LINUX)

#include "MonoGUI.h"

ODialog::ODialog ()
	: OWindow(self_type)
{
	m_nDoModalReturn = 0;
	m_pAccell = new OAccell ();
}

ODialog::~ODialog ()
{
	if (m_pAccell != NULL) {
		delete m_pAccell;
		m_pAccell = NULL;
	}
}

// 虚函数，绘制对话框
void ODialog::Paint (LCD* pLCD)
{
	CHECK_TYPE;

	// 如果不可见，则什么也不绘制
	if (! IsWindowVisible()) {
		return;
	}

	// 先进行其他绘制
	// 根据窗口不同的状态绘制
	// 如果处于焦点的窗口不是对话框，则绘制反白的题头，并绘制对话框阴影
	// 否则绘制普通的题头，在题头下面绘制一条线
	if (m_pActive != NULL)
	{
		if (m_pActive->GetWndType() != WND_TYPE_DIALOG)
		{
			// 绘制焦点的题头
			DrawDialog (pLCD, TRUE);
		}
		else
		{
			// 绘制普通的题头
			DrawDialog (pLCD, FALSE);
		}
	}
	else
	{
		// 绘制焦点的题头
		DrawDialog (pLCD, TRUE);
	}

	// 最后进行默认绘制
	OWindow::Paint (pLCD);
}

// 虚函数，消息处理
// 消息处理过了，返回1，未处理返回0
int ODialog::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	// 先进行默认处理
	int nReturn = OWindow::Proc (pWnd, nMsg, wParam, lParam);

	// 如果各控件没有处理，则进行下列处理
	// 是否关闭对话框或者切换焦点控件的按键消息
	if (nReturn != 1)
	{
		if (nMsg == OM_KEYDOWN)
		{
			if (wParam == KEY_ESCAPE)
			{
				// 关闭窗口
				// 给自己发送OM_CLOSE消息
				O_MSG msg;
				msg.pWnd    = this;
				msg.message = OM_CLOSE;
				msg.wParam  = m_ID;
				msg.lParam  = 0;
				m_pApp->PostMsg (&msg);
				nReturn = 1;
			}

			// 下面处理关系到焦点切换的按键
			if ((wParam == KEY_TAB)  ||
			    (wParam == KEY_UP)   ||
			    (wParam == KEY_DOWN) ||
			    (wParam == KEY_LEFT) ||
			    (wParam == KEY_RIGHT))
			{
				if (OnChangeFocus())
				{
					int id = -1;
					int nTabOrder = -1;
					if (m_pActive != NULL)
					{
						nTabOrder = m_pActive->m_nTabOrder;
						id = m_pActive->m_ID;
					}

					// 处理上下左右键
					if ((wParam == KEY_LEFT)  ||
						(wParam == KEY_RIGHT) ||
						(wParam == KEY_UP)    ||
						(wParam == KEY_DOWN))
					{
						OWindow* pWnd = NULL;

						if (m_pActive != NULL)
						{
							switch (wParam)
							{
							case KEY_LEFT:
								pWnd = FindWindowLeft (m_pActive);
								break;
							case KEY_RIGHT:
								pWnd = FindWindowRight (m_pActive);
								break;
							case KEY_UP:
								pWnd = FindWindowUp (m_pActive);
								break;
							case KEY_DOWN:
								pWnd = FindWindowDown (m_pActive);
								break;
							}
						}

						if ((pWnd != NULL) && (pWnd != m_pActive))
						{
							if (pWnd->IsWindowEnabled())
							{
								id = pWnd->m_ID;
								nReturn = 1;
							}
						}
					}

					if (nReturn == 0)
					{
						// 处理焦点切换键
						if ((wParam == KEY_TAB)   ||
							(wParam == KEY_RIGHT) ||
							(wParam == KEY_DOWN))
						{
							// 循环切换
							nTabOrder += 1;
							OWindow* pWnd = FindChildByTab (nTabOrder);
							// 如果没有找到应当被设置为焦点的控件，则将第一个设置为焦点
							if (pWnd != NULL)
								id = pWnd->m_ID;
							else if (m_pChildren != NULL)
								id = m_pChildren->m_ID;

							nReturn = 1;
						}
					}

					if (nReturn == 0)
					{
						if (wParam == KEY_LEFT || wParam == KEY_UP)
						{
							int i;
				  			for (i = 0; i < m_nChildCount; i++)
				    		{
				      			if (nTabOrder == 0) {
									nTabOrder = m_nChildCount;
								}
					  
								nTabOrder --;

								OWindow* pWnd = FindChildByTab (nTabOrder);

								if (pWnd != NULL)
								{
					  				if (((pWnd->m_wStatus & WND_STATUS_INVALID) == 0) &&
										((pWnd->m_wStatus & WND_STATUS_INVISIBLE) == 0))
									{
										id = pWnd->m_ID;
										nReturn = 1;
										break;
									}
								}
							}
						}
					}

					// 切换焦点的工作用消息交给基类去处理
					O_MSG msg;	
					msg.pWnd    = this;
					msg.message = OM_SETCHILDACTIVE;
					msg.wParam  = id;
					msg.lParam  = 0;
					m_pApp->PostMsg (&msg);
				}
				nReturn = 1;
			}
		}
	}

#if defined (CHINESE_SUPPORT)
	// 处理快捷键列表(打开输入法的时候要屏蔽快捷键)
	if ((nReturn != 1) && (! m_pApp->IsIMEOpen ()))
	{
#endif // defined(CHINESE_SUPPORT)

#if !defined (CHINESE_SUPPORT)
       // 处理快捷键列表
       if (nReturn != 1)
	{
#endif //!defined(CHINESE_SUPPORT)
		if (nMsg == OM_KEYDOWN)
		{
			int id = m_pAccell->Find (wParam);
			if (id != 0)
			{
				// 如果ID与某个按钮的ID相同，则发出OM_PUSHDOWN消息使按钮按下
				// 首先发送消息使该按钮成为焦点
				OWindow* pWnd = FindChildByID (id);
				if (pWnd != NULL)
				{
					O_MSG msg;
				//  如果快捷键响应时需要切换焦点，则解开下列语句
				/*
					msg.pWnd	= this;
					msg.message = OM_SETCHILDACTIVE;
					msg.wParam	= id;
					msg.lParam	= 0;
					m_pApp->PostMsg (&msg);	// 设置焦点的消息
				*/
					msg.pWnd	= pWnd;
					msg.message = OM_PUSHDOWN;
					msg.wParam	= 0;
					msg.lParam	= 0;
					m_pApp->PostMsg (&msg);	// 向按钮发送通知其按下的消息 
				}
				else
				{
					O_MSG msg;
					msg.pWnd	= this;
					msg.message = OM_NOTIFY_PARENT;
					msg.wParam	= id;
					msg.lParam	= 0;
					m_pApp->PostMsg (&msg);	// 给自己发送快捷键消息 
				}
			}
		}

	}
	nReturn = 1;
	return nReturn;
}

#if defined (MOUSE_SUPPORT)
int ODialog::PtProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int nReturn = OWindow::PtProc (pWnd, nMsg, wParam, lParam);

	if (nReturn != 1)
	{
		if (nMsg == OM_LBUTTONDOWN)
		{
			if (PtInWindow (wParam, lParam) )
			{
				// 处理关闭按钮
				// 可见，有题头，显示关闭按钮，才处理关闭按钮
				if (((m_wStatus & WND_STATUS_INVISIBLE)   == 0) &&
					((m_wStyle  & WND_STYLE_NO_TITLE)     == 0) &&
					((m_wStyle  & WND_STYLE_CLOSE_BUTTON) >  0))
				{
					int x = wParam;
					int y = lParam;
					int L = m_x + m_w - 15;
					int T = m_y;
					int R = m_x + m_w;
					int B = m_y + 15;
					if ((x >= L) && (y >= T) && (x <= R) && (y <= B))
					{
						// 关闭窗口
						// 给自己发送OM_CLOSE消息
						O_MSG msg;
						msg.pWnd    = this;
						msg.message = OM_CLOSE;
						msg.wParam  = m_ID;
						msg.lParam  = 0;
						m_pApp->PostMsg (&msg);
					}
				}
				nReturn = 1;
			}
		}
	}

	nReturn = 1;
	return nReturn;
}
#endif // defined(MOUSE_SUPPORT)

// 进入模式状态
int ODialog::DoModal()
{
	CHECK_TYPE_RETURN;

	if (m_pApp == NULL) {
		return -1;
	}

	int nMsgInfo = 1;
	O_MSG msg;

	while (nMsgInfo != 0)
	{
		int nMsgInfo = m_pApp->m_pMsgQ->GetMsg (&msg);

		if (nMsgInfo == 1)
		{
			// 遇到Delete本窗口的消息，执行后退出
			if( (msg.pWnd    == m_pParent)      &&
				(msg.message == OM_DELETECHILD) &&
				(msg.wParam  == m_ID) )
			{
				int nRet = m_nDoModalReturn;
				m_pApp->DespatchMsg (&msg);
				return nRet;
			}
			else {
				m_pApp->DespatchMsg (&msg);
			}
		}
		else if (nMsgInfo == -1) {
			m_pApp->Idle();
		}

		if (m_pApp->m_fnEventProcess) {
			// 维持主程序事件循环的运转
			m_pApp->m_fnEventProcess();
		}
		else {
			DebugPrintf("ERROR: ODialog::DoModal m_pApp->m_fnEventProcess is NULL!\n");
		}
	}

	// 向消息队列中填补一条QUIT消息
	msg.message = OM_QUIT;
	m_pApp->PostMsg (&msg);

	return 0;
}

// 从对话框资源文件创建对话框
BOOL ODialog::CreateFromTemplet (OWindow* pParent, char* filename)
{
	DLGTEMPLET* pT = new DLGTEMPLET;

	// 从对话框资源文件初始化对话框模板
	if (OpenDlgTemplet (pT, filename))
	{
		// 调用基类的Create函数向父窗口注册本对话框
		OWindow::Create(pParent,
						pT->wStyle,
						WND_STATUS_FOCUSED,
						pT->x, pT->y, pT->w, pT->h, pT->id);

		// 根据模板修改对话框的各个成员变量
		SetText (pT->caption, WINDOW_CAPTION_BUFFER_LEN -1);

		// 初始化快捷键列表
		m_pAccell->Create (pT->pAccellTable, m_pApp->m_pKeyMap);

		// 根据模板创建各种控件
		CTRLDATA* pNext = pT->controls;
		int i;
		for (i=0; i<pT->controlnr; i++)
		{
			if (pNext == NULL)
			{
				DeleteDlgTemplet (pT);
				return FALSE;
			}

			// 控件坐标是相对于对话框左上点的位置，应当转换成绝对坐标
			int ix = pT->x + pNext->x;
			int iy = pT->y + pNext->y;

			if ((pT->wStyle & WND_STYLE_NO_TITLE) == 0)
				iy += 15;			// 加上题头的高度

			int iw = pNext->w;
			int ih = pNext->h;
			int id = pNext->id;

			switch (pNext->wType)
			{
			case WND_TYPE_STATIC:
				{
				  //DebugPrintf( "Debug: Here Create a Static %s \n",pNext->caption );
					OStatic* pCtrl = new OStatic();
					pCtrl->Create (this,
								pNext->wStyle,
								WND_STATUS_INVALID,
								ix, iy, iw, ih, id);
					pCtrl->SetText (pNext->caption, 255);
					if (pNext->nAddData > 0) {
						pCtrl->SetImage (m_pApp->m_pImageMgt->GetImage(pNext->nAddData));
					}
				}
				break;

			case WND_TYPE_BUTTON:
				{
					OButton* pCtrl = new OButton ();
					pCtrl->Create (this,
								pNext->wStyle,
								WND_STATUS_NORMAL,
								ix, iy, iw, ih, id);
					pCtrl->SetText (pNext->caption, WINDOW_CAPTION_BUFFER_LEN-1);
				}
				break;

			case WND_TYPE_EDIT:
				{
					OEdit* pCtrl = new OEdit ();
					pCtrl->Create (this,
								pNext->wStyle,
								WND_STATUS_NORMAL,
								ix, iy, iw, ih, id);
					pCtrl->SetText (pNext->caption, WINDOW_CAPTION_BUFFER_LEN-1);
					pCtrl->LimitText (pNext->nAddData);
				}
				break;

			case WND_TYPE_LIST:
				{
					OList* pCtrl = new OList ();
					pCtrl->Create (this,
								pNext->wStyle,
								WND_STATUS_NORMAL,
								ix, iy, iw, ih, id);
					pCtrl->SetLinage (pNext->nAddData);
				}
				break;

			case WND_TYPE_COMBO:
				{
					OCombo* pCtrl = new OCombo ();
					pCtrl->Create (this,
								pNext->wStyle,
								WND_STATUS_NORMAL,
								ix, iy, iw, ih, id);
					pCtrl->SetText (pNext->caption, WINDOW_CAPTION_BUFFER_LEN-1);
					pCtrl->SetDroppedLinage (pNext->nAddData);
				}
				break;

			case WND_TYPE_PROGRESS:
				{
					OProgressBar* pCtrl = new OProgressBar ();
					pCtrl->Create (this,
								pNext->wStyle,
								WND_STATUS_NORMAL,
								ix, iy, iw, ih, id);
				}
				break;

			case WND_TYPE_IMAGE_BUTTON:
				{
					OImgButton* pCtrl = new OImgButton();
					pCtrl->Create (this,
								pNext->wStyle,
								WND_STATUS_NORMAL,
								ix, iy, iw, ih, id);
					pCtrl->SetImage (m_pApp->m_pImageMgt->GetImage(pNext->nAddData));
				}
				break;

			case WND_TYPE_CHECK_BOX:
				{
					OCheckBox* pCtrl = new OCheckBox();
					pCtrl->Create( this,
								pNext->wStyle,
								WND_STATUS_NORMAL,
								ix, iy, iw, ih, id );
					pCtrl->SetText (pNext->caption, WINDOW_CAPTION_BUFFER_LEN-1);
					pCtrl->SetCheck (pNext->nAddData);
				}
				break;

			default:
				{
					DebugPrintf("No such kind of control in dialog templet file %s.", filename);
				}
			}
			pNext = pNext->next;
		}

		// 设置默认按钮
		m_pDefaultButton = FindDefaultButton ();

		// 将第一个控件设置为焦点
		SetFocus (m_pChildren);
		//
		if (m_pActive != NULL)
		{
			O_MSG msg;
			msg.pWnd = m_pActive;
			msg.message = OM_SETFOCUS;
			msg.wParam = m_pActive->m_ID;
			msg.lParam = 0;
			m_pApp->SendMsg (&msg);
		}

		// 删除对话框模板
		DeleteDlgTemplet (pT);
		return TRUE;
	}
	DeleteDlgTemplet (pT);
	return FALSE;
}

// 根据ID号，使用相应的模板文件创建对话框
BOOL ODialog::CreateFromID(OWindow* pWnd, int nID)
{
	char sFileName[WINDOW_CAPTION_BUFFER_LEN];
	memset (sFileName, 0x0, WINDOW_CAPTION_BUFFER_LEN);
	sprintf (sFileName, "dtm/%d.dtm", nID);
	return (CreateFromTemplet (pWnd, sFileName));
}

// 从模板文件初始化对话框模板
BOOL ODialog::OpenDlgTemplet (DLGTEMPLET* pTemplet, char* filename)
{
	// 创建模板
	pTemplet->wStyle		= WND_STYLE_NORMAL;
	pTemplet->x				= 0;
	pTemplet->y				= 0;
	pTemplet->w				= 0;
	pTemplet->h				= 0;
	memset (pTemplet->caption, 0x0, WINDOW_CAPTION_BUFFER_LEN);
	pTemplet->id			= 0;
	pTemplet->controlnr		= 0;
	pTemplet->controls		= NULL;
	pTemplet->pAccellTable	= NULL;

	char* pFile = NULL;
	DWORD dwFileLength;

	// 打开对话框模板文件（在目标系统上需要修改）
#if defined (RUN_ENVIRONMENT_LINUX)
	FILE* fp = fopen (filename, "r");
	if (fp == NULL) {
		DebugPrintf ("ERROR: ODialog::OpenDlgTemplet Dialog Templet file %s can not open!\n", filename);
		return FALSE;
	}
	
	// 得到文件长度
    if (0 != fseek (fp, 0, SEEK_END)) {
		fclose (fp);
		DebugPrintf ("ERROR: ODialog::OpenDlgTemplet SEEK_END error!\n");
		return FALSE;
	}
	dwFileLength = ftell (fp);

	// 创建一个与文件等长的数组
	pFile = new char[dwFileLength+1];
	memset (pFile, 0x0, dwFileLength+1);

	// 将文件内容读入数组
    if (0 != fseek(fp, 0, SEEK_SET)) {
		fclose (fp);
		delete [] pFile;
		DebugPrintf ("ERROR: ODialog::OpenDlgTemplet SEEK_SET error!\n");
		return FALSE;
	}

	fread (pFile, sizeof(char), dwFileLength, fp);
	fclose (fp);
#endif // defined(RUN_ENVIRONMENT_LINUX)

#if defined (RUN_ENVIRONMENT_WIN32)
	HANDLE hFile;
	hFile = CreateFile(filename,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,NULL);
	if (hFile == INVALID_HANDLE_VALUE)
		return FALSE;

	// 将文件打开到一个char数组中
	dwFileLength = GetFileSize (hFile, 0);

	pFile = new char[dwFileLength+1];
	memset (pFile, 0x0, dwFileLength+1);

	DWORD dwActualSize = 0;
	BOOL bReadSt;
	bReadSt = ReadFile (hFile, pFile, dwFileLength, &dwActualSize, NULL);
	CloseHandle (hFile);
	
	if ((!bReadSt) || (dwActualSize != dwFileLength))
	{
		delete [] pFile;
		DebugPrintf ("ERROR: ODialog::OpenDlgTemplet ReadFile error!\n");
		return FALSE;
	}
#endif // defined(RUN_ENVIRONMENT_WIN32)

	// 逐行解读模板文件
	BOOL bTempletValid = FALSE;
	int k = 0;
	char sOneLine[512];
	memset (sOneLine, 0x0, sizeof(sOneLine));
	while (GetLine (pFile, sOneLine, sizeof(sOneLine), k))
	{
		k ++;
		if ((sOneLine[0] != '#') && (sOneLine[0] != '\0'))	// 去掉注释行和空行
		{
			int i = 0;
			char sSlice[256];
			memset (sSlice, 0x0, sizeof(sSlice));
			if (GetSlice (sOneLine, sSlice, sizeof(sSlice), i))
			{
				if (strcmp (sSlice, "DIALOG") == 0)
				{
					// 解读对话框设置
					if (GetDialogInfoFromString (pTemplet,sOneLine))
					{
						bTempletValid = TRUE;
					}
					else
					{
						DebugPrintf("ERROR: Dialog Templet Error! [DIALOG].\n");
						delete [] pFile;
						return FALSE;
					}
				}
				else if (strcmp (sSlice, "CONTROL") == 0)
				{
					// 解读控件设置
					if (! GetControlInfoFromString (pTemplet,sOneLine))
					{
						DebugPrintf("ERROR: Dialog Templet Error! [CONTROL].\n");
						delete [] pFile;
						return FALSE;
					}
				}
				else if (strcmp (sSlice, "ACCELL") == 0)
				{
					// 将快捷键列表复制到指定位置
					int iLength = strlen (sOneLine);
					if (iLength > 6)
					{
						iLength -= 6;
					}
					else
					{
						DebugPrintf("ERROR: Dialog Templet Error! [ACCELL].\n");
						delete [] pFile;
						return FALSE;
					}

					pTemplet->pAccellTable = new char[iLength];
					memset (pTemplet->pAccellTable, 0x0, iLength);
					strncpy (pTemplet->pAccellTable, (sOneLine+7), iLength-1);
				}
				else
				{
					DebugPrintf("ERROR: Dialog Templet Error! Unexpected Line.\n");
					delete [] pFile;
					return FALSE;
				}
			}
			else
			{
				DebugPrintf("ERROR: Dialog Templet Error! GetSlice fail!\n");
				delete [] pFile;
				return FALSE;
			}
		}
		// 清空暂存行
		memset (sOneLine, 0x0, 512);
	}

	if (bTempletValid) {
		delete [] pFile;
		return TRUE;
	}

	if (pFile) {
		delete [] pFile;
	}
	DebugPrintf ("ERROR: ODialog::OpenDlgTemplet Templet Content Invalid!\n");
	return FALSE;
}

// 从字符串中获取对话框设置
// 格式：DIALOG;对话框风格(数字);X(数字);Y(数字);W(数字);H(数字);ID(数字);对话框题头文字(字符串，不超过255);
BOOL ODialog::GetDialogInfoFromString (DLGTEMPLET* pTemplet, char* sString)
{
	int k = 1;
	char sTemp[WINDOW_CAPTION_BUFFER_LEN];

	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
		return FALSE;
	pTemplet->wStyle = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
		return FALSE;
	pTemplet->x = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
		return FALSE;
	pTemplet->y = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
		return FALSE;
	pTemplet->w = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
		return FALSE;
	pTemplet->h = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
		return FALSE;
	pTemplet->id = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (!GetSlice (sString, sTemp, sizeof(sTemp), k))
		return FALSE;
	strncpy (pTemplet->caption, sTemp, 255);

	return TRUE;
}

// 从字符串中获取控件设置
// 格式：CONTROL;控件类型(数字);控件风格(数字);X(数字);Y(数字);W(数字);H(数字);ID(数字);对话框题头文字(字符串，不超过255);
BOOL ODialog::GetControlInfoFromString (DLGTEMPLET* pTemplet, char* sString)
{
	CTRLDATA* pCtrl = new CTRLDATA;
	memset (pCtrl->caption, 0x0, WINDOW_CAPTION_BUFFER_LEN);

	int k = 1;
	char sTemp[WINDOW_CAPTION_BUFFER_LEN];

	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
	{
		delete pCtrl;
		return FALSE;
	}
	pCtrl->wType = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
	{
		delete pCtrl;
		return FALSE;
	}
	pCtrl->wStyle = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
	{
		delete pCtrl;
		return FALSE;
	}
	pCtrl->x = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
	{
		delete pCtrl;
		return FALSE;
	}
	pCtrl->y = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
	{
		delete pCtrl;
		return FALSE;
	}
	pCtrl->w = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
	{
		delete pCtrl;
		return FALSE;
	}
	pCtrl->h = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
	{
		delete pCtrl;
		return FALSE;
	}
	pCtrl->id = atoi (sTemp);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
	{
		delete pCtrl;
		return FALSE;
	}
	strncpy (pCtrl->caption, sTemp, 255);

	k ++;
	memset (sTemp, 0x0, sizeof(sTemp));
	if (! GetSlice (sString, sTemp, sizeof(sTemp), k))
	{
		delete pCtrl;
		return FALSE;
	}
	pCtrl->nAddData = atoi (sTemp);

	int nCount = pTemplet->controlnr;
	if (nCount == 0)
	{
		pTemplet->controls = pCtrl;
		pTemplet->controlnr ++;
		return TRUE;
	}
	else
	{
		CTRLDATA* pNext = pTemplet->controls;
		int i;
		for (i = 0; i < nCount; i++)
		{
			if (pNext == NULL)
			{
				delete pCtrl;
				return FALSE;
			}
			if (i == nCount-1)
			{
				pNext->next = pCtrl;
				pTemplet->controlnr ++;
				return TRUE;
			}
			pNext = pNext->next;
		}
	}

	delete pCtrl;
	return FALSE;
}

// 删除对话框模板
// 应当依照链表从最后一个开始delete，直到删光所有控件模板
BOOL ODialog::DeleteDlgTemplet (DLGTEMPLET* pTemplet)
{
	CHECK_TYPE_RETURN;

	if (pTemplet == NULL) {
		return TRUE;
	}

	// 删除控件链表
	if ((pTemplet->controlnr == 0) || (pTemplet->controls == NULL))
		return TRUE;

	int i;
	for (i = 0; i < pTemplet->controlnr; i++)
	{
		CTRLDATA* pNext = pTemplet->controls->next;
		if (pTemplet->controls == NULL) {
			return FALSE;
		}

		delete pTemplet->controls;
		pTemplet->controls = pNext;
	}

	// 删除快捷键列表的文本
	if (pTemplet->pAccellTable != NULL) {
		delete pTemplet->pAccellTable;
	}

	// 删除对话框模板
	delete pTemplet;

	return TRUE;
}

// 绘制对话框
// bFocusMode：FALSE,非焦点窗口；TRUE,反白显示(焦点窗口)
void ODialog::DrawDialog (LCD* pLCD, BOOL bFocusMode)
{
	CHECK_TYPE;

	// 如果不可见，则什么也不绘制
	if ((m_wStatus & WND_STATUS_INVISIBLE) != 0) {
		return;
	}

	int nTitleLength = 0;
	if( (m_wStyle & WND_STYLE_CLOSE_BUTTON) > 0 ) {
		// 关闭按钮占用一段显示空间
		nTitleLength = GetDisplayLimit (m_sCaption, m_w-20);
	}
	else {
		nTitleLength = GetDisplayLimit (m_sCaption, m_w-4);
	}

	// 具有圆角风格并且尺寸足够大的对话框才可以使用圆角风格
	if (((m_wStyle & WND_STYLE_ROUND_EDGE) == 0) && (m_w > 12) && (m_h > 12))
	{
		// 绘制普通样式的对话框
		int crBk = 1;
		int crFr = 0;

		// 在窗口范围绘制一个边框
		pLCD->FillRect(m_x, m_y, m_w, m_h, crBk);
		pLCD->HLine   (m_x, m_y, m_w, crFr);
		pLCD->HLine   (m_x, m_y+m_h-1, m_w, crFr);
		pLCD->VLine   (m_x, m_y, m_h, crFr);
		pLCD->VLine   (m_x+m_w-1, m_y, m_h, crFr);

		if (bFocusMode)
		{
			// 普通的，焦点的
			if ((m_wStyle & WND_STYLE_NO_TITLE) == 0)
			{
				// 绘制反白题头
				pLCD->FillRect (m_x+1, m_y+1, m_w-2, 15, crFr);
				pLCD->TextOut  (m_x+2,m_y+2,(BYTE*)m_sCaption,nTitleLength, LCD_MODE_INVERSE);
				// 绘制关闭按钮
				if ((m_wStyle & WND_STYLE_CLOSE_BUTTON) > 0) {
					pLCD->DrawImage (m_x+m_w-15,m_y,15,15,g_4color_Close_Button1,0,0,LCD_MODE_INVERSE);
				}
			}

			// 如果指定了立体效果，则绘制对话框的阴影
			if ((m_wStyle & WND_STYLE_SOLID) > 0)
			{
				pLCD->HLine (m_x+1, m_y+m_h, m_w, crFr);
				pLCD->VLine (m_x+m_w, m_y+1, m_h, crFr);
			}
		}
		else
		{
			// 普通的，非焦点的
			if ((m_wStyle & WND_STYLE_NO_TITLE) == 0)
			{
				// 绘制正常题头
				pLCD->TextOut (m_x+2,m_y+2,(BYTE*)m_sCaption,nTitleLength, LCD_MODE_NORMAL);
				pLCD->HLine (m_x+1, m_y+15, m_w-2, crFr);
				// 绘制关闭按钮
				if ((m_wStyle & WND_STYLE_CLOSE_BUTTON) > 0)
					pLCD->DrawImage (m_x+m_w-15,m_y,15,15,g_4color_Close_Button1,0,0,LCD_MODE_NORMAL);
			}
		}
	}
	else
	{
		// 绘制圆角的对话框
		int crBk = 1;
		int crFr = 0;

		// 绘制一个边框
		pLCD->FillRect(m_x, m_y+6, m_w, m_h-12, crBk);
		pLCD->FillRect(m_x+6, m_y+1, m_w-12, 5, crBk);
		pLCD->FillRect(m_x+6, m_y+m_h-6, m_w-12, 5, crBk);
		pLCD->HLine   (m_x+6, m_y, m_w-12, crFr);
		pLCD->HLine   (m_x+6, m_y+m_h-1, m_w-12, crFr);
		pLCD->VLine   (m_x, m_y+6, m_h-12, crFr);
		pLCD->VLine   (m_x+m_w-1, m_y+6, m_h-12, crFr);

		//nTitleLength -= 2;

		if (bFocusMode)
		{
			// 圆角的，焦点的

			// 左上角和右上角两个角图的填补
			if ((m_wStyle & WND_STYLE_NO_TITLE) == 0)
			{
				// 有标题栏
				if ((m_wStyle & WND_STYLE_SOLID) > 0)
				{
					// 有立体效果
					pLCD->DrawImage (m_x, m_y, 6, 6, g_4color_Button_Default, 0, 0, LCD_MODE_BLACKNESS);
					pLCD->DrawImage (m_x+m_w-6, m_y, 6, 6, g_4color_Button_Default, 6, 0, LCD_MODE_BLACKNESS);
				}
				else
				{
					// 无立体效果
					pLCD->DrawImage (m_x, m_y, 6, 6, g_4color_Button_Normal, 0, 0, LCD_MODE_BLACKNESS);
					pLCD->DrawImage (m_x+m_w-6, m_y, 6, 6, g_4color_Button_Normal, 6, 0, LCD_MODE_BLACKNESS);
				}
				// 绘制关闭按钮
				if ((m_wStyle & WND_STYLE_CLOSE_BUTTON) > 0) {
					pLCD->DrawImage (m_x+m_w-15,m_y,15,15,g_4color_Close_Button2,0,0,LCD_MODE_NORMAL);
				}
			}
			else
			{
				// 无标题栏
				if ((m_wStyle & WND_STYLE_SOLID) > 0)
				{
					// 有立体效果
					pLCD->DrawImage (m_x, m_y, 6, 6, g_4color_Button_Default, 0, 0, LCD_MODE_NORMAL);
					pLCD->DrawImage (m_x+m_w-6, m_y, 6, 6, g_4color_Button_Default, 6, 0, LCD_MODE_NORMAL);
				}
				else
				{
					// 无立体效果
					pLCD->DrawImage (m_x, m_y, 6, 6, g_4color_Button_Normal, 0, 0, LCD_MODE_NORMAL);
					pLCD->DrawImage (m_x+m_w-6, m_y, 6, 6, g_4color_Button_Normal, 6, 0, LCD_MODE_NORMAL);
				}
			}

			// 绘制左下角和右下角的圆弧
			// 如果指定了立体效果，则选用带有阴影的角图，并绘制对话框的阴影
			if ((m_wStyle & WND_STYLE_SOLID) > 0)
			{
				// 无立体效果
				pLCD->DrawImage (m_x, m_y+m_h-6, 7, 7, g_4color_Button_Default, 0, 6, LCD_MODE_NORMAL);
				pLCD->DrawImage (m_x+m_w-6, m_y+m_h-6, 7, 7, g_4color_Button_Default, 6, 6, LCD_MODE_NORMAL);
				pLCD->HLine (m_x+6, m_y+m_h, m_w-12, crFr);
				pLCD->VLine (m_x+m_w, m_y+6, m_h-12, crFr);
			}
			else
			{
				// 有立体效果
				pLCD->DrawImage (m_x, m_y+m_h-6, 7, 7, g_4color_Button_Normal, 0, 6, LCD_MODE_NORMAL);
				pLCD->DrawImage (m_x+m_w-6, m_y+m_h-6, 7, 7, g_4color_Button_Normal, 6, 6, LCD_MODE_NORMAL);
			}

			// 填补题头
			if ((m_wStyle & WND_STYLE_NO_TITLE) == 0)
			{
				// 绘制反白题头
				pLCD->FillRect (m_x+6, m_y+1, m_w-12, 15, crFr);
				pLCD->FillRect (m_x+1, m_y+6, m_w-2, 10, crFr);
				pLCD->TextOut (m_x+7,m_y+2,(BYTE*)m_sCaption,nTitleLength, LCD_MODE_INVERSE);
				// 绘制关闭按钮
				if ((m_wStyle & WND_STYLE_CLOSE_BUTTON) > 0) {
					pLCD->DrawImage (m_x+m_w-15,m_y,15,15,g_4color_Close_Button2,0,0,LCD_MODE_INVERSE);
				}
			}
		}
		else
		{
			// 圆角的，非焦点的

			// 正常模式，圆角，先绘制背景
			pLCD->DrawImage (m_x, m_y, 6, 6, g_4color_Button_Normal, 0, 0, LCD_MODE_NORMAL);
			pLCD->DrawImage (m_x+m_w-6, m_y, 6, 6, g_4color_Button_Normal, 6, 0, LCD_MODE_NORMAL);
			pLCD->DrawImage (m_x, m_y+m_h-6, 7, 7, g_4color_Button_Normal, 0, 6, LCD_MODE_NORMAL);
			pLCD->DrawImage (m_x+m_w-6, m_y+m_h-6, 7, 7, g_4color_Button_Normal, 6, 6, LCD_MODE_NORMAL);

			if ((m_wStyle & WND_STYLE_NO_TITLE) == 0)
			{
				// 绘制正常题头
				pLCD->FillRect(m_x+6, m_y+1, m_w-12, 15, crBk);
				pLCD->FillRect(m_x+1, m_y+6, m_w-2, 10, crBk);
				pLCD->TextOut (m_x+7,m_y+2,(BYTE*)m_sCaption,nTitleLength, LCD_MODE_NORMAL);
				pLCD->HLine   (m_x+1, m_y+15, m_w-2, crFr);
				// 绘制关闭按钮
				if ((m_wStyle & WND_STYLE_CLOSE_BUTTON) > 0) {
					pLCD->DrawImage (m_x+m_w-15,m_y,15,15,g_4color_Close_Button2,0,0,LCD_MODE_NORMAL);
				}
			}
		}
	}
}

// 处理焦点切换
BOOL ODialog::OnChangeFocus()
{
	CHECK_TYPE_RETURN;

	return TRUE;
}

/* END */
