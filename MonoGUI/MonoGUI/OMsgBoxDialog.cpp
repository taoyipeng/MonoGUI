////////////////////////////////////////////////////////////////////////////////
// @file OMsgBoxDialog.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OMsgBoxDialog::OMsgBoxDialog ()
{
	memset (m_sInformation, 0x0, sizeof(m_sInformation));
	m_wMsgBoxStyle = 0x0;
}

OMsgBoxDialog::~OMsgBoxDialog ()
{
}

// 虚函数，绘制对话框
void OMsgBoxDialog::Paint (LCD* pLCD)
{
	// 如果不可见，则什么也不绘制
	if (! IsWindowVisible())
		return;

	// 首先调用基类的绘制函数重绘各控件
	ODialog::Paint (pLCD);

	if (m_wMsgBoxStyle > 0)
	{
		FOUR_COLOR_IMAGE* pIcon = NULL;

		if ((m_wMsgBoxStyle & OMB_ERROR) > 0)
		{
			pIcon = &g_4color_Icon_Error;
		}
		else if ((m_wMsgBoxStyle & OMB_EXCLAMATION) > 0)
		{
			pIcon = &g_4color_Icon_Exclamation;
		}
		else if ((m_wMsgBoxStyle & OMB_QUESTION) > 0)
		{
			pIcon = &g_4color_Icon_Question;
		}
		else if ((m_wMsgBoxStyle & OMB_INFORMATION) > 0)
		{
			pIcon = &g_4color_Icon_Information;
		}

		if (pIcon != NULL)
		{
			pLCD->DrawImage (m_x+10,m_y+18,23,23,*pIcon,0,0, LCD_MODE_NORMAL);
		}
	}
}

// 虚函数，消息处理
// 消息处理过了，返回1，未处理返回0
int OMsgBoxDialog::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	if (! IsWindowEnabled())
		return 0;

	// 屏蔽ESC键
	if ((nMsg == OM_KEYDOWN) && (wParam == KEY_ESCAPE))
		return 1;

	ODialog::Proc (pWnd, nMsg, wParam, lParam);

	if ((pWnd == this) && (nMsg == OM_NOTIFY_PARENT))	// 按钮被按下
	{
		O_MSG msg;

		// 关闭MessageBox对话框
		msg.pWnd    = m_pParent;
		msg.message = OM_DELETECHILD;
		msg.wParam  = m_ID;
		msg.lParam  = (ULONGLONG)m_pOldParentActiveWindow;
		m_pApp->PostMsg (&msg);
		
		//发送消息,让默认的Active窗口获得焦点
		if (m_pOldParentActiveWindow != NULL)
		{
		    msg.pWnd    = m_pOldParentActiveWindow;
		    msg.message = OM_SETFOCUS;
		    msg.wParam  = m_pOldParentActiveWindow->m_ID;
		    msg.lParam  = 0;
		    m_pApp->PostMsg (&msg);
		}

		// 向父窗口发送消息，通知被按下的按钮
		msg.pWnd    = m_pParent;
		msg.message = OM_MSGBOX_RETURN;
		msg.wParam  = m_ID;
		msg.lParam  = wParam;
		m_pApp->PostMsg (&msg);

		// 记录按下的按钮
		m_nDoModalReturn = wParam;
	}
	return 1;	// 禁止其他消息向父窗口传递
}

// 创建MessageBox
BOOL OMsgBoxDialog::Create (OWindow* pParent, char* sTitle, char* sText, WORD wMsgBoxStyle, int ID)
{
	memset (m_sInformation, 0x0, WINDOW_CAPTION_BUFFER_LEN);
	strncpy (m_sInformation, sText, WINDOW_CAPTION_BUFFER_LEN -1);
	m_wMsgBoxStyle = wMsgBoxStyle;

	WORD wStyle = WND_STYLE_NORMAL;
	if ((wMsgBoxStyle & OMB_ROUND_EDGE) > 0)
	{
		wMsgBoxStyle &= ~OMB_ROUND_EDGE;	// 去掉圆角属性，绘制时可以减少判断
		wStyle |= WND_STYLE_ROUND_EDGE;
	}
	if ((wMsgBoxStyle & OMB_SOLID) > 0)
	{
		wMsgBoxStyle &= ~OMB_SOLID;		// 去掉立体属性，绘制时可以减少判断
		wStyle |= WND_STYLE_SOLID;
	}

	// 计算显示文字可能会占用的空间
	int nWidth = 0;
	int nHeight = 0;
	char sTemp[WINDOW_CAPTION_BUFFER_LEN];

	// 最多不能超过四行文本
	int i;
	for (i = 0; i < 4; i++)
	{
		memset (sTemp, 0x0, WINDOW_CAPTION_BUFFER_LEN);
		if (GetLine (m_sInformation, sTemp, sizeof(sTemp), i))
		{
			int nDesplayLength = GetDisplayLength (sTemp, WINDOW_CAPTION_BUFFER_LEN -1);
			if (nDesplayLength > nWidth) {
				nWidth = nDesplayLength;
			}

			nHeight += 14;
		}
	}

	if (nWidth > (SCREEN_W - 40)) {
		nWidth = SCREEN_W - 40;
	}

	int nTextLeft = 10;
	int tw = nWidth + 24;
	int th = nHeight + 57;
	if (m_wMsgBoxStyle > 0)
	{
		tw += 30;
		nTextLeft += 30;
	}
	if (tw < 80)
		tw = 80;

	int tx = pParent->m_x + (pParent->m_w - tw) / 2;
	int ty = pParent->m_y + (pParent->m_h - th) / 2;

	// 创建MessageBox对话框
	BOOL bSuccess = ODialog::Create (pParent, wStyle, WND_STATUS_FOCUSED, tx, ty, tw, th, ID);

	if (! bSuccess) {
		DebugPrintf("ERROR: OMsgBoxDialog::OMsgBoxDialog ODialog::Create fail!\n");
		return FALSE;
	}

	// DebugPrintf( "OMsgBoxDialog: sTitle = %s \n", sTitle );
	SetText (sTitle, 80);

	// 为每一行文本创建一个OStatic控件
	// 最多不能超过四行文本
	for (i = 0; i < 4; i++)
	{
		memset (sTemp, 0x0, WINDOW_CAPTION_BUFFER_LEN);
		if (GetLine (m_sInformation, sTemp, sizeof(sTemp), i))
		{
			OStatic* pStatic = new OStatic ();
			pStatic->Create(this, WND_STYLE_NO_BORDER, WND_STATUS_INVALID, 
							m_x+nTextLeft, m_y+21+i*14, nWidth+36, 14, 12);
			//DebugPrintf( "Debug: OStatic: sTemp = %s \n", sTemp );
			pStatic->SetText (sTemp, 32);
		}
	}

	// 创建按钮
	// 如果未指定按钮样式，则创建一个“确定”按钮
	// 如果指定了OMB_YESNO样式，则创建“是”“否”两个按钮
	// 如果指定了OMB_OKCANCEL样式，则创建“确定”“取消”两个按钮
	// 创建确定按钮
	if ((wMsgBoxStyle & OMB_YESNO) > 0)
	{
		OButton* pYesButton = new OButton ();
		pYesButton->Create (this, wStyle, WND_STATUS_FOCUSED,
							m_x+m_w/2-50, m_y+m_h-27, 40, 20, OID_YES);

		// 支持中英文两种按钮
#if defined (CHINESE_SUPPORT)
		if ((wMsgBoxStyle & OMB_ENGLISH) == 0)
			pYesButton->SetText ("是", 2);
		else
#endif // defined(CHINESE_SUPPORT)

			pYesButton->SetText ("YES", 3);

		OButton* pNoButton = new OButton ();
		pNoButton->Create (this, wStyle, WND_STATUS_FOCUSED,
						   m_x+m_w/2+10, m_y+m_h-27, 40, 20, OID_NO);

		// 支持中英文两种按钮
#if defined (CHINESE_SUPPORT)
		if ((wMsgBoxStyle & OMB_ENGLISH) == 0)
			pNoButton->SetText ("否", 2);
		else
#endif // defined(CHINESE_SUPPORT)

			pNoButton->SetText ("NO", 2);

		// 如果设定了MB_DEFAULT_NO样式，则将“否”设置为默认按钮
		if ((wMsgBoxStyle & OMB_DEFAULT_NO) == 0)
		{
			SetFocus (pYesButton);
		}
		else
		{
			SetFocus (pNoButton);
		}
	}
	else if ((wMsgBoxStyle & OMB_OKCANCEL) > 0)
	{
		OButton* pOkButton = new OButton ();
		pOkButton->Create (this, wStyle, WND_STATUS_FOCUSED,
						   m_x+m_w/2-70, m_y+m_h-27, 60, 20, OID_OK);

		// 支持中英文两种按钮
#if defined (CHINESE_SUPPORT)
		if ((wMsgBoxStyle & OMB_ENGLISH) == 0)
			pOkButton->SetText ("确定", 4);
		else
#endif // defined(CHINESE_SUPPORT)

			pOkButton->SetText ("OK", 2);

		OButton* pCancelButton = new OButton ();
		pCancelButton->Create (this, wStyle, WND_STATUS_FOCUSED,
							   m_x+m_w/2+10, m_y+m_h-27, 60, 20, OID_CANCEL);

		// 支持中英文两种按钮
#if defined (CHINESE_SUPPORT)
		if ((wMsgBoxStyle & OMB_ENGLISH) == 0)
		      pCancelButton->SetText ("取消", 4);
		else
#endif // defined(CHINESE_SUPPORT)

		      pCancelButton->SetText("CANCEL", 6);

		// 如果设定了MB_DEFAULT_NO样式，则将“取消”设置为默认按钮
		if ((wMsgBoxStyle & OMB_DEFAULT_NO) == 0)
		{
			SetFocus (pOkButton);
		}
		else
		{
			SetFocus (pCancelButton);
		}
	}
	else
	{
		OButton* pOkButton = new OButton ();
		pOkButton->Create (this, wStyle, WND_STATUS_FOCUSED,
						   m_x+m_w/2-30, m_y+m_h-27, 60, 20, OID_OK);

		// 支持中英文两种按钮
#if defined (CHINESE_SUPPORT)
		if ((wMsgBoxStyle & OMB_ENGLISH) == 0)
			pOkButton->SetText ("确定", 4);
		else
#endif // defined(CHINESE_SUPPORT)

			pOkButton->SetText ("OK", 2);
	}

	return TRUE;
}

/* END */