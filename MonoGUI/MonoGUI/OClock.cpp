////////////////////////////////////////////////////////////////////////////////
// @file OClock.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if defined(RUN_ENVIRONMENT_LINUX)
#include <sys/time.h>
#endif // defined(RUN_ENVIRONMENT_LINUX)

#include "MonoGUI.h"

OClock::OClock (OApp* pApp)
{
	m_pApp = pApp;
	m_nStatus = 0;
	m_lLastTime = sys_clock()-1000;
	memset (m_sTime, 0x0, sizeof(m_sTime));
	memset (m_sDate, 0x0, sizeof(m_sDate));

	m_pBuffer = new LCD(SCREEN_W, SCREEN_H);

	m_nClockX = DEFAULT_CLOCK_X;
	m_nClockY = DEFAULT_CLOCK_Y;

#if defined (MOUSE_SUPPORT)
	m_nClockButtonX = -1;
	m_nClockButtonY = -1;
#endif // defined(MOUSE_SUPPORT)
}

OClock::~OClock()
{
	if (m_pBuffer != NULL) {
		delete m_pBuffer;
		m_pBuffer = NULL;
	}
}

void OClock::Enable (BOOL bEnable)
{
	if (bEnable)
	{
		m_nStatus = 1;
		m_lLastTime = sys_clock()-1000;
	}
	else
	{
		m_nStatus = 0;
		O_MSG msg;
		msg.pWnd = m_pApp->m_pMainWnd;
		msg.message = OM_PAINT;
		msg.wParam = 0;
		msg.lParam = 0;
		m_pApp->PostMsg (&msg);
	}
}

BOOL OClock::IsEnable()
{
	if (m_nStatus == 1) {
		return TRUE;
	}

	return FALSE;
}

BOOL OClock::Check (LCD* pLCD, LCD* pBuf)
{
	if (0 == m_nStatus) {
		return FALSE;
	}

	ULONGLONG lNow = sys_clock();
	if ((lNow - m_lLastTime) < 100) {
		// 不到100毫秒钟不刷新显示
		return FALSE;
	}

	m_lLastTime = lNow;

	ODATETIME stTime;

	// 得到系统时间
	time_t time_val;
	time_t tloc;
	struct tm* pTime;
	time_val = time (&tloc);
	if (time_val != (time_t)-1)
	{
		pTime	= gmtime (&time_val);
		stTime.nYear   = pTime->tm_year+1900;
		stTime.nMonth  = pTime->tm_mon+1;
		stTime.nDay    = pTime->tm_mday;
		stTime.nHour   = pTime->tm_hour;
		stTime.nMinute = pTime->tm_min;
		stTime.nSecond = pTime->tm_sec;
	}

	// 修正时区
	stTime.nHour += LOCAL_TIME_ZONE;
	if (stTime.nHour >= 24) {
		stTime.nHour -= 24;
		NextDay (&stTime);
	}

	int nYear   = stTime.nYear;
	int nMonth  = stTime.nMonth;
	int nDay    = stTime.nDay;
	int nHour   = stTime.nHour;
	int nMinute = stTime.nMinute;
	int nSecond = stTime.nSecond;

	// 将缓冲区内容拷贝入类成员缓冲区中
	m_pBuffer->Copy (*pBuf);

	// 绘制背景
	m_pBuffer->DrawImage(m_nClockX, m_nClockY,
		g_4color_ClockFace.w, g_4color_ClockFace.h,
		g_4color_ClockFace, 0, 0, LCD_MODE_NORMAL);

	// 绘制数字的日期和时间
#if defined (CHINESE_SUPPORT)
	sprintf (m_sDate, "%4d年%2d月%2d日",   nYear,nMonth,nDay);
	sprintf (m_sTime, " %2d 时%2d分%2d秒", nHour,nMinute,nSecond);
	m_pBuffer->TextOut (m_nClockX+82, m_nClockY+34,
		(BYTE *)m_sDate, 14, LCD_MODE_NORMAL);
	m_pBuffer->TextOut (m_nClockX+82, m_nClockY+52,
		(BYTE *)m_sTime, 14,LCD_MODE_NORMAL);
#else
	sprintf (m_sDate,   "%4d/%2d/%2d", nYear,nMonth,nDay);
	sprintf (m_sTime, "  %2d:%2d:%2d", nHour,nMinute,nSecond);
	m_pBuffer->TextOut (m_nClockX+96, m_nClockY+32,
		(BYTE *)m_sDate, 14, LCD_MODE_INVERSE);
	m_pBuffer->TextOut (m_nClockX+96, m_nClockY+50,
		(BYTE *)m_sTime, 14, LCD_MODE_INVERSE);
#endif // defined(CHINESE_SUPPORT)

	// 绘制两条表针
	int hand_color = 0;

	// 1绘制秒针
	// 确定起始点
	int x1 = m_nClockX + CENTER_X + 1;
	int y1 = m_nClockY + CENTER_Y + 1;
	if ((nSecond>0) && (nSecond<=15))
	{
		x1 += 1;
	}
	else if ((nSecond>15) && (nSecond<=30))
	{
		x1 += 1;
		y1 += 1;
	}
	else if ((nSecond>30) && (nSecond<=45))
	{
		y1 += 1;
	}

	// 确定终止点
	int x2 = m_nClockX + MINUTE_HAND[nSecond%60][0] + 1;
	int y2 = m_nClockY + MINUTE_HAND[nSecond%60][1] + 1;
	// 绘制秒针
	m_pBuffer->Line (x1,y1,x2,y2, hand_color);

	// 2绘制分针
	// 确定起始点
	x1 = m_nClockX + CENTER_X + 1;
	y1 = m_nClockY + CENTER_Y + 1;
	// 确定终止点
	x2 = m_nClockX + MINUTE_HAND[nMinute%60][0] + 1;
	y2 = m_nClockY + MINUTE_HAND[nMinute%60][1] + 1;
	// 绘制分针（分针宽度3像素）
	m_pBuffer->Line (x1,y1,x2,y2, hand_color);
	x1 += 1;
	x2 += 1;
	m_pBuffer->Line (x1,y1,x2,y2, hand_color);
	y1 += 1;
	y2 += 1;
	m_pBuffer->Line (x1,y1,x2,y2, hand_color);
	x1 -= 1;
	x2 -= 1;
	m_pBuffer->Line (x1,y1,x2,y2, hand_color);

	// 3绘制时针
	// 确定起始点
	x1 = m_nClockX + CENTER_X + 1;
	y1 = m_nClockY + CENTER_Y + 1;
	// 确定终止点
	int nIndex = (nHour % 12) * 5 + nMinute / 12;
	x2 = m_nClockX + CENTER_X + 1 + (MINUTE_HAND[nIndex][0]-CENTER_X)*2/3;
	y2 = m_nClockY + CENTER_Y + 1 + (MINUTE_HAND[nIndex][1]-CENTER_Y)*2/3;
	// 绘制时针（时针宽度3像素）
	m_pBuffer->Line (x1,y1,x2,y2,hand_color);
	x1 += 1;
	x2 += 1;
	m_pBuffer->Line (x1,y1,x2,y2, hand_color);
	y1 += 1;
	y2 += 1;
	m_pBuffer->Line (x1,y1,x2,y2, hand_color);
	x1 -= 1;
	x2 -= 1;
	m_pBuffer->Line (x1,y1,x2,y2, hand_color);

	// 将绘制好的缓冲区送上屏幕
	pLCD->Copy (*m_pBuffer);

	return TRUE;
}

#if defined (MOUSE_SUPPORT)
// 鼠标点击时钟按钮的处理
BOOL OClock::PtProc (int x, int y)
{
	if (m_nStatus == 0)
	{
		if ((m_nClockButtonX != -1) && (m_nClockButtonY != -1))
		{
			if (PtInRect(x, y, m_nClockButtonX, m_nClockButtonY,
				m_nClockButtonX+15, m_nClockButtonY+15))
			{
				Enable(TRUE);
				return TRUE;
			}
		}
		return FALSE;
	}
	else {
		Enable(FALSE);
		return TRUE;
	}
}

// 显示时钟按钮
void OClock::ShowClockButton (LCD* pLCD)
{
	// 绘制时钟按钮
	if (m_nStatus == 0)
	{
		if ((m_nClockButtonX != -1) && (m_nClockButtonY != -1))
		{
			pLCD->DrawImage(m_nClockButtonX, m_nClockButtonY,
				g_4color_Clock_Button.w, g_4color_Clock_Button.h,
				g_4color_Clock_Button, 0, 0, LCD_MODE_INVERSE);
		}
	}
}

// 设置表盘的显示位置
BOOL  OClock::SetClockPos (int x, int y)
{
	// 传入无效值使表盘位置复位
	if (x<0 || x>SCREEN_W || y<0 || y>SCREEN_H)
	{
		m_nClockX = DEFAULT_CLOCK_X;
		m_nClockY = DEFAULT_CLOCK_Y;
		return FALSE;
	}

	m_nClockX = x;
	m_nClockY = y;
	return TRUE;
}

// 设置时钟按钮的位置
BOOL OClock::SetClockButton (int x, int y)
{
	// 传入无效值使时钟按钮消失
	if (x<0 || x>SCREEN_W || y<0 || y>SCREEN_H)
	{
		m_nClockButtonX = -1;
		m_nClockButtonY = -1;
		return FALSE;
	}

	m_nClockButtonX = x;
	m_nClockButtonY = y;
	return TRUE;
}
#endif // defined(MOUSE_SUPPORT)

/* END */