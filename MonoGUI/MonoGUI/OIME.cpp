////////////////////////////////////////////////////////////////////////////////
// @file OIME.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

// 定义各种输入法的名称
const static char* g_arIMEName[IME_END-1] = 
{
	"全键拼音",
	"全键方言",
	"九键拼音",
	"九键方言",
	"英文小写",
	"英文大写",
	"标点符号"
};

OIME::OIME ()
	: OWindow(self_type)
{
	m_nCurIME = IME_OFF;  // 不打开输入法
	m_nCurInputLine	= IME_CURRENT_UPPER;  // 默认上栏
	m_pTargetWnd = NULL;
	m_pIMEDB = NULL;
	m_nSyLength = 0;
	m_nSyValidCount = 0;
	m_nSyCount = 0;
	m_nCurSy = 0;
	m_nHzCount = 0;
	m_nCurPage = 0;
	m_nOutStrLength = 0;
	memset (m_sUpperLine, 0x0, sizeof(m_sUpperLine));
	memset (m_sLowerLine, 0x0, sizeof(m_sLowerLine));

	m_wStatus = 0;
}

OIME::~OIME ()
{
	m_pTargetWnd = NULL;
}

// 创建输入法窗口
BOOL OIME::Create (OApp* pApp)
{
	if (pApp == NULL) {
		DebugPrintf("ERROR: OIME::create parameter == NULL!\n");
		return FALSE;
	}

	m_pApp = pApp;
	m_x = 0;
	m_y = 93;
	m_w = 264;
	m_h = 35;
	m_nCurIME = IME_OFF;

	m_pIMEDB = new OIME_DB ();

#if defined (RUN_ENVIRONMENT_WIN32)
	if (! m_pIMEDB->CheckDataTable()) {
		return m_pIMEDB->CreateDataTable();
	}
#endif // defined(RUN_ENVIRONMENT_WIN32)

	// 注意，如果输入法数据库索引已经创建好了，
	// 可以直接加载（不需要再调用CreateDataTable函数）
	if (m_pIMEDB->LoadDataTable()){
		return TRUE;
	}
	else {
		DebugPrintf("ERROR: OIME::create m_pIMEDB->LoadDataTable fail!\n");
	}
}

// 绘制输入法窗口
void OIME::Paint (LCD* pLCD)
{
	CHECK_TYPE;

	// 如果不可见，则什么也不绘制
	if (! IsWindowVisible()) {
		return;
	}

	if (m_nCurIME == IME_OFF) {
		return;
	}

	int crBk = 1;
	int crFr = 0;

	// 绘制背景
	pLCD->FillRect (m_x, m_y, m_w, m_h, crBk);
//	pLCD->FillRect (m_x, m_y, m_w, m_h, 2);	// 填充灰色底子的效果
//	pLCD->FillRect (m_x+7, m_y+2, m_w-10, m_h-4, crBk);

	// 绘制上行的文字(可以绘制32个英文字母)
	pLCD->TextOut (m_x+9, m_y+2, m_sUpperLine, sizeof(m_sUpperLine)-1, LCD_MODE_NORMAL);
	// 如果是英文输入法，且没有超时，则最后一个字母反白显示；
	// 如果超时了，则最后一个字母的后面添加一条小竖线
	if ((m_nCurIME == IME_9LE) || (m_nCurIME == IME_9UE))
	{
		if (m_nTimeOut == 0)
		{
			// 未超时，最后一个字母反白显示
			if (m_nOutStrLength > 0)
			{
				int nLength = GetDisplayLength ((char*)m_sUpperLine, m_nOutStrLength - 1);
				BYTE cc = m_sUpperLine[m_nOutStrLength - 1];
				pLCD->TextOut (m_x+9+nLength, m_y+2, &cc, 1, LCD_MODE_INVERSE);
			}
		}
		else
		{
			// 超时，如果上行有内容则在最后显示一条小竖线
			if (m_nOutStrLength > 0)
			{
				int nLength = GetDisplayLength ((char*)m_sUpperLine, m_nOutStrLength);
				pLCD->VLine (m_x+9+nLength, m_y+2, 14, 1);
			}
		}
	}
	// 绘制下行的文字
	pLCD->TextOut (m_x+9, m_y+19, m_sLowerLine, sizeof(m_sLowerLine)-1, LCD_MODE_NORMAL);

	// 绘制外框
	pLCD->HLine (m_x, m_y-1, m_w, crBk);
	pLCD->HLine (m_x, m_y+m_h, m_w, crBk);
	pLCD->HLine (m_x, m_y, m_w, crFr);
	pLCD->HLine (m_x, m_y+m_h-1, m_w, crFr);
	pLCD->VLine (m_x, m_y, m_h, crFr);
	pLCD->VLine (m_x+m_w-1, m_y, m_h, crFr);
	pLCD->HLine (m_x+7, m_y+2, m_w-10, crFr);
	pLCD->HLine (m_x+7, m_y+m_h-3, m_w-10, crFr);
	pLCD->VLine (m_x+7, m_y+2, m_h-4, crFr);
	pLCD->VLine (m_x+m_w-3, m_y+2, m_h-4, crFr);
	pLCD->HLine (m_x+7, m_y+17, m_w-10, crFr);
	pLCD->VLine (m_x+209, m_y+2, 16, crFr);

	// 绘制当前输入行指示
	int nYPos = 0;
	if (m_nCurInputLine == IME_CURRENT_UPPER) {
		nYPos = m_y+6;
	}
	else {
		nYPos = m_y+21;
	}

	pLCD->DrawImage (m_x, nYPos, 7, 7,
					 g_4color_Arror_Right, 0, 0, LCD_MODE_AND);

	// 绘制文字指示当前输入法
	BYTE sText[9];
	memset (sText, 0x0, sizeof(sText));
	memcpy (sText, g_arIMEName[m_nCurIME-1], 8);
	pLCD->TextOut (m_x+211, m_y+4, sText, 8, LCD_MODE_NORMAL);

	if (! m_bAllSyInvalid)
	{
		// 绘制上栏的左右选择指示箭头
		if ((m_nSyLength > 0) && (m_nSyValidCount > 1))
		{
			if (m_nCurSy > 0)	// 绘制向左的箭头
			{
				pLCD->DrawImage (m_x+199, m_y+7, 5, 5,
								 g_4color_Little_Arror_Left, 0, 0, LCD_MODE_NORMAL);
			}
			if (m_nCurSy < (m_nSyValidCount-1))	// 绘制向右的箭头
			{
				pLCD->DrawImage (m_x+204, m_y+7, 5, 5,
								 g_4color_Little_Arror_Right, 0, 0, LCD_MODE_NORMAL);
			}
		}

		// 绘制当前拼音选择的下划线
		int nx = m_x + 9 + (ASC_W + ASC_GAP) * (m_nSyLength + 1) * m_nCurSy;
		int nw = (ASC_W + ASC_GAP) * m_nSyLength;
		pLCD->HLine (nx, m_y+15, nw, 0);
	}

	// 绘制下栏的上下翻页指示箭头
	if (m_nHzCount > 9)
	{
		if (m_nCurPage > 0)		// 绘制向上箭头
		{
			pLCD->DrawImage (m_x+m_w-10, m_y+20, 5, 5,
							 g_4color_Little_Arror_Up, 0, 0, LCD_MODE_NORMAL);
		}
		if (m_nCurPage < ((m_nHzCount+8)/9-1))	// 绘制向下箭头
		{
			pLCD->DrawImage (m_x+m_w-10, m_y+25, 5, 5,
							 g_4color_Little_Arror_Down, 0, 0, LCD_MODE_NORMAL);
		}
	}

	// 绘制九宫格小键盘
	BW_IMAGE* pKeyboardThumbnail = NULL;
	switch (m_nCurIME) {
	case IME_9PY:
	case IME_9FY:
	case IME_9LE:
		pKeyboardThumbnail = &g_bw_SudokoKeyboard_Lowercase;
		break;

	case IME_9UE:
		pKeyboardThumbnail = &g_bw_SudokoKeyboard_Uppercase;
		break;
	}

	if (pKeyboardThumbnail != NULL)
	{
		if (m_bShowAboveTargetWnd) {
			// 显示在输入法窗口的上边
			pLCD->DrawImage (m_x, m_y - pKeyboardThumbnail->h,
				pKeyboardThumbnail->w, pKeyboardThumbnail->h,
				*pKeyboardThumbnail, 0, 0, LCD_MODE_NORMAL);
		}
		else {
			// 显示在输入法窗口的下边
			pLCD->DrawImage (m_x, m_y + m_h,
				pKeyboardThumbnail->w, pKeyboardThumbnail->h,
				*pKeyboardThumbnail, 0, 0, LCD_MODE_NORMAL);
		}
	}
}

// 消息处理，返回1
int OIME::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (m_nCurIME == IME_OFF || m_pTargetWnd == NULL) {
		return FALSE;
	}

	if (pWnd == this)
	{
		if ((nMsg == OM_KEYDOWN) && (wParam == KEY_IME_NEXT || wParam == KEY_IME_PREV))
		{
			m_pApp->KillTimer (IME_TIMER_ID);
			m_nTimeOut = 0;

			// 切换输入法
			if (wParam == KEY_IME_NEXT)
			{
				if (m_nCurIME < (IME_END - 1)) {
					m_nCurIME ++;
				}
				else {
					m_nCurIME = IME_BEGIN;
				}
			}
			else
			{
				if (m_nCurIME > IME_BEGIN) {
					m_nCurIME --;
				}
				else {
					m_nCurIME = IME_END - 1;
				}
			}

			// 根据输入法的不同进行不同的初始化
			m_pIMEDB->SyRemoveAll ();
			memset (m_sUpperLine, 0x0, sizeof(m_sUpperLine));
			memset (m_sLowerLine, 0x0, sizeof(m_sLowerLine));
			m_nOutStrLength = 0;
			m_nCurInputLine = IME_CURRENT_UPPER;
			m_nSyLength = 0;
			m_nSyValidCount = 0;
			m_nSyCount = 0;
			m_nCurSy = 0;
			m_nHzCount = 0;
			m_nCurPage = 0;
			m_nOutStrLength = 0;

			if (m_nCurIME == IME_SY)
			{
				// 符号输入法，将当前输入行设置在下行，并初始化下行的显示内容
				m_nCurInputLine = IME_CURRENT_LOWER;
				m_sHzReturn = (BYTE*)strPunctuation;
				m_nHzCount = IME_PUNCTUATION_NUM;
				RenewLowerLine ();		// 根据m_cHzReturn和当前行等信息更新下栏
			}

			if (m_nCurIME == IME_9PY) {	// 九键拼音输入法
				m_pIMEDB->SetCurIME (IME_9PY);
			}
			else if (m_nCurIME == IME_9FY) {	// 九键方言输入法
				m_pIMEDB->SetCurIME (IME_9FY);
			}
		}
		else
		{
			switch (m_nCurIME)
			{
			case IME_PY:
			case IME_FY:
				{
					PinyinProc (pWnd, nMsg, wParam, lParam);
				}
				break;

			case IME_9PY:		// 九键拼音输入法
			case IME_9FY:
				{
					Pinyin9KeyProc (pWnd, nMsg, wParam, lParam);
				}
				break;

			case IME_9LE:		// 九键英文输入法
			case IME_9UE:
				{
					English9KeyProc (pWnd, nMsg, wParam, lParam);
				}
				break;
			case IME_SY:		// 符号输入
				{
					PunctuationProc (pWnd, nMsg, wParam, lParam);
				}
				break;
			}
		}
	}

	return OWindow::Proc (pWnd, nMsg, wParam, lParam);
//	UpdateView (this);	// 如果界面刷新不好，解开此语句
}

// 是否可以处理控制按键(如果输入窗是空的，则不处理控制按键，而让OEdit窗口去处理)
BOOL OIME::CanHandleControlKey ()
{
	CHECK_TYPE_RETURN;

	// 全键盘输入法，只要上栏为空就不处理控制键
	if ((m_nCurIME == IME_PY || m_nCurIME == IME_FY)
		&& (m_nSyLength == 0)) {
		return FALSE;
	}
	if ((m_nCurIME == IME_9PY || m_nCurIME == IME_9FY)
		&& (m_nCurInputLine == IME_CURRENT_UPPER)
		&& (m_nSyValidCount == 0)) {
		return FALSE;
	}
	if ((m_nCurIME == IME_9LE || m_nCurIME == IME_9UE) 
		&& (m_nOutStrLength == 0)) {
		return FALSE;
	}
	if (m_nCurIME == IME_SY) {
		return FALSE;
	}

	return TRUE;
}

// 打开输入法窗口(打开显示，创建联系)
BOOL OIME::OpenIME (OWindow* pWnd)
{
	CHECK_TYPE_RETURN;

	if (pWnd == NULL) {
		return FALSE;
	}

	m_pTargetWnd = pWnd;

	// 如果输入法窗口挡住了目标控件，则显示在目标控件的上方
	m_x = pWnd->m_x;
	if ((m_y + m_h) > SCREEN_H) {
		m_y = pWnd->m_y - m_h - 1;
		m_bShowAboveTargetWnd = TRUE;
	}
	else {
		m_y = pWnd->m_y + pWnd->m_h + 1;
		m_bShowAboveTargetWnd = FALSE;
	}

	m_nCurIME = IME_PY;	// 默认拼音输入法
	m_pIMEDB->SetCurIME (IME_PY);

	m_wStatus &= ~WND_STATUS_INVISIBLE;
	return TRUE;
}

// 关闭输入法窗口(关闭显示，断开联系)
BOOL OIME::CloseIME (OWindow* pWnd)
{
	CHECK_TYPE_RETURN;

	if (pWnd != m_pTargetWnd) {
		return FALSE;
	}

	m_nCurIME = IME_OFF;	// 关闭输入法窗口
	m_pIMEDB->SyRemoveAll ();
	m_nCurInputLine = IME_CURRENT_UPPER;
	m_pTargetWnd = NULL;
	m_nSyLength = 0;
	m_nSyValidCount = 0;
	m_nSyCount = 0;
	m_nCurSy = 0;
	m_nHzCount = 0;
	m_nCurPage = 0;
	m_nOutStrLength = 0;
	memset (m_sUpperLine, 0x0, sizeof(m_sUpperLine));
	memset (m_sLowerLine, 0x0, sizeof(m_sLowerLine));

	m_wStatus |= WND_STATUS_INVISIBLE;
	return TRUE;
}

// 看输入法窗口是否处于打开状态
BOOL OIME::IsIMEOpen ()
{
	CHECK_TYPE_RETURN;

	if (m_nCurIME == IME_OFF) {
		return FALSE;
	}
	else {
		return TRUE;
	}
}

// 处理定时器消息(只用于英文输入法)
void OIME::OnTimer (int nTimerID, int iInterval)
{
	CHECK_TYPE;

	m_pApp->KillTimer (IME_TIMER_ID);
	m_nTimeOut = 1;
	UpdateView(this);
}

// 向目标窗口发送字符(如果是英文，则只有c1，c2为0)
void OIME::SendChar (BYTE c1, BYTE c2)
{
	CHECK_TYPE;

	O_MSG msg;
	msg.pWnd = m_pTargetWnd;
	msg.message = OM_CHAR;
	msg.wParam = c1;
	msg.lParam = c2;
	m_pApp->SendMsg (&msg);
}

// 根据数字件1~0选择字符并发送
BYTE* OIME::LowerLineGetChar (int nKeyValue, int nLength)
{
	CHECK_TYPE_RETURN_NULL;

	int nSelect = 0;

	switch (nKeyValue)
	{
	case KEY_1:	nSelect=0; break;
	case KEY_2:	nSelect=1; break;
	case KEY_3:	nSelect=2; break;
	case KEY_4:	nSelect=3; break;
	case KEY_5:	nSelect=4; break;
	case KEY_6:	nSelect=5; break;
	case KEY_7:	nSelect=6; break;
	case KEY_8:	nSelect=7; break;
	case KEY_9:	nSelect=8; break;
	case KEY_0: nSelect=9; break;
	default:	return NULL;
	}

	int nOffset = m_nCurPage * 10 + nSelect;
	if (nOffset < m_nHzCount)
	{
		nOffset *= 2;
		if (nLength == 1)
		{
			SendChar (m_sHzReturn[nOffset], 0x0);
		}
		else if (nLength == 2)
		{
			SendChar (m_sHzReturn[nOffset], m_sHzReturn[nOffset+1]);
		}
		return &m_sHzReturn[nOffset];
	}
	return NULL;
}

// 全键拼音输入法
void OIME::PinyinProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE;

	if (nMsg == OM_KEYDOWN)
	{
		switch (wParam)
		{
		case KEY_UP:
		case KEY_LEFT:
			{
				// 向上(下栏，向前翻页)
				if (m_nCurInputLine == IME_CURRENT_LOWER)
				{
					LowerLinePageUp ();
				}
			}
			break;

		case KEY_DOWN:
		case KEY_RIGHT:
			{
				// 向下(下栏，向后翻页)
				if (m_nCurInputLine == IME_CURRENT_LOWER)
				{
					LowerLinePageDown ();
				}
			}
			break;

		case KEY_BACK_SPACE:
			{
				// 如果上栏有内容，把操作切回上栏
				if (m_nSyLength > 0) {
					m_nCurInputLine = IME_CURRENT_UPPER;
				}

				// 删除(上栏，删除一个字符)
				if (m_nCurInputLine == IME_CURRENT_UPPER)
				{
					if (m_nSyLength > 0)
					{
						// 删掉最后一个字符
						if (m_nSyLength > 0) {
							m_nSyLength --;
							m_sUpperLine[m_nSyLength] = 0x0;
							RenewPinyin ();
						}
					}
				}
			}
			break;

		case KEY_SPACE:
			{
				// 空格键
				SendChar (0x20, 0x0);
			}
			break;

		case KEY_PERIOD:
			{
				// 输入小数点
				SendChar ('.', 0x0);
			}
			break;

		case KEY_ENTER:
			{
				// 确认键(切换到下栏)
				if (m_nCurInputLine == IME_CURRENT_UPPER)
				{
					// 上栏
					// 如果下栏有东西，则将当前输入设置为下栏
					if (m_nHzCount > 0)
					{
						m_nCurInputLine = IME_CURRENT_LOWER;
					}
				}
			}
			break;

		case KEY_ESCAPE:
			{
				// ESCAPE键(切换回上栏)
				if (m_nCurInputLine == IME_CURRENT_LOWER)
				{
					m_nCurInputLine = IME_CURRENT_UPPER;
				}
			}
			break;

		case KEY_0:
		case KEY_1:
		case KEY_2:
		case KEY_3:
		case KEY_4:
		case KEY_5:
		case KEY_6:
		case KEY_7:
		case KEY_8:
		case KEY_9:
			{
				// 数字键(上栏输入数字，下栏选择汉字)
				if (m_nCurInputLine == IME_CURRENT_UPPER)
				{
					// 上下栏都空，输入数字
					if (m_nSyLength == 0 && m_nHzCount == 0) {
						SendChar (wParam, 0x0);
						break;
					}

					// 如果下栏有内容，直接切换到下栏功能
					if (m_nHzCount > 0) {
						m_nCurInputLine = IME_CURRENT_LOWER;
					}
				}
				
				if (m_nCurInputLine == IME_CURRENT_LOWER)
				{
					// 下栏
					// 选择一个汉字，然后查找联想词库，根据返回集更新下栏
					// 然后将当前输入切换到上栏
					BYTE* cHz = LowerLineGetChar (wParam, 2);
					if (cHz != NULL)
					{
						BYTE cc [3];
						memset (cc, 0x0, sizeof(cc));
						memcpy (cc, cHz, 2);
						m_nHzCount = m_pIMEDB->GetLx (cc, &m_sHzReturn);
						m_nCurPage = 0;
						RenewLowerLine ();
						// 清空上栏
						m_pIMEDB->SyRemoveAll ();
						memset (m_sUpperLine, 0x0, sizeof(m_sUpperLine));
						m_nSyLength = 0;
						m_nSyCount = 0;
						m_nSyValidCount = 0;
						m_nCurSy = 0;

						//// 将当前输入切换到上栏
						//m_nCurInputLine = IME_CURRENT_UPPER;
					}
				}
			}
			break;

		case KEY_A:
		case KEY_B:
		case KEY_C:
		case KEY_D:
		case KEY_E:
		case KEY_F:
		case KEY_G:
		case KEY_H:
		case KEY_I:
		case KEY_J:
		case KEY_K:
		case KEY_L:
		case KEY_M:
		case KEY_N:
		case KEY_O:
		case KEY_P:
		case KEY_Q:
		case KEY_R:
		case KEY_S:
		case KEY_T:
		case KEY_U:
		case KEY_V:
		case KEY_W:
		case KEY_X:
		case KEY_Y:
		case KEY_Z:
			{
				// 字母键只在上栏有效，如果当前输入在下栏，则将当前输入切换回上栏
				if (m_nCurInputLine == IME_CURRENT_LOWER)
				{
					m_nCurInputLine = IME_CURRENT_UPPER;
				}

				// 字母键，只在上栏有效
				if (m_nCurInputLine == IME_CURRENT_UPPER)
				{
					// 上栏(拼音不会超过7个字母)
					if (m_nSyLength < 7) {

						//使用小写字母
						m_sUpperLine[m_nSyLength] = (wParam + 32);
						m_nSyLength ++;
						RenewPinyin ();
					}
				}
			}
			break;
		}
	}
}

// 拼音输入法
void OIME::Pinyin9KeyProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE;

	if (nMsg == OM_KEYDOWN)
	{
		switch (wParam)
		{
		case KEY_UP:
			{
				// 向上(下栏，向前翻页)
				if (m_nCurInputLine == IME_CURRENT_LOWER)
				{
					LowerLinePageUp ();
				}
			}
			break;

		case KEY_DOWN:
			{
				// 向下(下栏，向后翻页)
				if (m_nCurInputLine == IME_CURRENT_LOWER)
				{
					LowerLinePageDown ();
				}
			}
			break;

		case KEY_LEFT:
			{
				// 向左(上栏，选择前面的一个拼音)(下栏，向前翻页)
				if (m_nCurInputLine == IME_CURRENT_UPPER)
				{
					if (m_nCurSy > 0) {
						m_nCurSy --;
					}
					// 修改拼音的后续处理
					RenewSy ();
				}
				else if (m_nCurInputLine == IME_CURRENT_LOWER)
				{
					LowerLinePageUp ();
				}
			}
			break;

		case KEY_RIGHT:
			{
				// 向右(上栏，选择后面的一个拼音)(下栏，向后翻页)
				if (m_nCurInputLine == IME_CURRENT_UPPER)
				{
					if (m_nCurSy < (m_nSyValidCount - 1)) {
						m_nCurSy ++;
					}
					// 修改拼音的后续处理
					RenewSy ();
				}
				else if (m_nCurInputLine == IME_CURRENT_LOWER)
				{
					LowerLinePageDown ();
				}
			}
			break;

		case KEY_BACK_SPACE:
			{
				// 删除(上栏，删除一个字符)
				if (m_nCurInputLine == IME_CURRENT_UPPER)
				{
					if (m_nSyLength > 0)
					{
						// 删掉最后一个字符
						m_pIMEDB->SyRemove ();
						// 重新获得拼音长度
						m_nSyLength = m_pIMEDB->GetSyLength ();
						// 查询拼音组合
						m_nSyCount = m_pIMEDB->GetSy (&m_sSyReturn);
						// 修改拼音的后续处理
						m_nCurSy = 0;
						RenewSy ();
					}
				}
			}
			break;

		case KEY_SPACE:
			{
				// 空格键
				SendChar (0x20, 0x0);
			}
			break;

		case KEY_PERIOD:
			{
				// 输入小数点
				SendChar ('.', 0x0);
			}
			break;

		case KEY_ENTER:
			{
				// 确认键(切换到下栏)
				if (m_nCurInputLine == IME_CURRENT_UPPER)
				{
					// 上栏
					// 如果下栏有东西，则将当前输入设置为下栏
					if (m_nHzCount > 0)
					{
						m_nCurInputLine = IME_CURRENT_LOWER;
					}
				}
			}
			break;

		case KEY_ESCAPE:
			{
				// ESCAPE键(切换回上栏)
				if (m_nCurInputLine == IME_CURRENT_LOWER)
				{
					m_nCurInputLine = IME_CURRENT_UPPER;
				}
			}
			break;

		case KEY_0:
		case KEY_1:
		case KEY_2:
		case KEY_3:
		case KEY_4:
		case KEY_5:
		case KEY_6:
		case KEY_7:
		case KEY_8:
		case KEY_9:
			{
				// 数字键(上栏输入拼音，下栏选择汉字)
				if (m_nCurInputLine == IME_CURRENT_UPPER)
				{
					// 上栏
					// 将按键转换成ASC字符，插入拼音序列
					// 如果返回集是空的，则把刚才插入的删掉
					// 首先将按键转换成字符
					char cKey = MessageToKey (wParam);
					// 向拼音集添加字符
					m_pIMEDB->SyAdd (cKey);
					// 查询拼音组合
					m_nSyCount = m_pIMEDB->GetSy (&m_sSyReturn);
					// 如果拼音组合是空的，则删除刚才输入的字符
					if (m_nSyCount == 0)
					{
						m_pIMEDB->SyRemove ();
						// 查询拼音组合
						m_nSyCount = m_pIMEDB->GetSy (&m_sSyReturn);
					}
					else
					{
						// 重新获得拼音长度
						m_nSyLength = m_pIMEDB->GetSyLength ();
						// 将当前选择设定为第一个
						m_nCurSy = 0;
						// 修改拼音的后续处理
						RenewSy ();
					}
				}
				else if (m_nCurInputLine == IME_CURRENT_LOWER)
				{
					// 下栏
					// 选择一个汉字，然后查找联想词库，根据返回集更新下栏
					// 然后将当前输入切换到上栏
					BYTE* cHz = LowerLineGetChar (wParam, 2);
					if (cHz != NULL)
					{
						BYTE cc [3];
						memset (cc, 0x0, sizeof(cc));
						memcpy (cc, cHz, 2);
						m_nHzCount = m_pIMEDB->GetLx (cc, &m_sHzReturn);
						m_nCurPage = 0;
						RenewLowerLine ();
						// 清空上栏并将当前输入切换到上栏
						m_pIMEDB->SyRemoveAll ();
						memset (m_sUpperLine, 0x0, sizeof(m_sUpperLine));
						m_nSyLength = 0;
						m_nSyCount = 0;
						m_nSyValidCount = 0;
						m_nCurSy = 0;
						m_nCurInputLine = IME_CURRENT_UPPER;
					}
				}
			}
			break;
		}
	}
}

// 英文输入法
void OIME::English9KeyProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE;

	if (nMsg == OM_KEYDOWN)
	{
		switch (wParam)
		{
		case KEY_SPACE:
			{
				// 空格键
				int i;
				for (i = 0; i < m_nOutStrLength; i++)
				{
					char cc = m_sUpperLine[i];
					SendChar (cc, 0x0);
				}
				m_nOutStrLength = 0;
				memset (m_sUpperLine, 0x0, sizeof(m_sUpperLine));
				// 关闭超时定时器
				m_pApp->KillTimer (IME_TIMER_ID);
				m_nTimeOut = 0;
				// 输出一个空格
				SendChar (0x20, 0x0);
			}
			break;

		case KEY_DOWN:
		case KEY_RIGHT:
			{
				// 向右和向下，提前终止超时定时器
				m_pApp->KillTimer (IME_TIMER_ID);
				m_nTimeOut = 1;
			}
			break;

		case KEY_BACK_SPACE:
			{
				// 删除
				if (m_nOutStrLength > 0)
				{
					m_sUpperLine[m_nOutStrLength - 1] = 0x0;
					m_nOutStrLength --;
				}
				// 关闭超时定时器
				m_pApp->KillTimer (IME_TIMER_ID);
				m_nTimeOut = 1;
			}
			break;

		case KEY_ENTER:
			{
				// 确认键
				int i;
				for (i = 0; i < m_nOutStrLength; i++)
				{
					char cc = m_sUpperLine[i];
					SendChar (cc, 0x0);
				}
				m_nOutStrLength = 0;
				memset (m_sUpperLine, 0x0, sizeof(m_sUpperLine));
				// 关闭超时定时器
				m_pApp->KillTimer (IME_TIMER_ID);
				m_nTimeOut = 0;
			}
			break;

		case KEY_0:
		case KEY_1:
		case KEY_2:
		case KEY_3:
		case KEY_4:
		case KEY_5:
		case KEY_6:
		case KEY_7:
		case KEY_8:
		case KEY_9:
			{
				// 数字键
				if (m_nOutStrLength < IME_OUT_STRING_LIMIT)
				{
					// 将数字键转换成ASC字符
					char cKey = MessageToKey (wParam);

					// 首先检测超时标志的情况
					if (m_nTimeOut == 1)
					{
						// 已经超时，则输入新的字符
						// 根据大小写状态进行不同的处理
						char cc = KeyToChar (cKey);	// 默认小写字母
						if (cc != 0x0)
						{
							if (m_nCurIME == IME_9UE) {	// 英文大写
								cc -= 0x20;
							}
							m_sUpperLine[m_nOutStrLength] = cc;
							m_nOutStrLength ++;
						}
					}
					else
					{
						// 没有超时，看新输入的字符是什么字符
						// 如果与上次输入的不是同一个字符，则输入新的字符
						// 如果与上次输入的是同一个字符，则修改最后一个字符
						// 根据大小写状态进行不同的处理
						char cc = 0x0;
						if (m_nOutStrLength > 0) {
							cc = CharToKey (m_sUpperLine[m_nOutStrLength-1]);
						}

						if (cc == cKey)		// 同一个按键，修改最后一个字符
						{
							cc = KeyNextChar (m_sUpperLine[m_nOutStrLength-1]);
							if (m_nCurIME == IME_9UE) {	// 英文大写
								cc -= 0x20;
							}
							m_sUpperLine[m_nOutStrLength-1] = cc;
						}
						else				// 插入新的字母
						{
							cc = KeyToChar (cKey);	// 默认小写字母
							if (cc != 0x0)
							{
								if (m_nCurIME == 4) {	// 英文大写
									cc -= 0x20;
								}
								m_sUpperLine[m_nOutStrLength] = cc;
								m_nOutStrLength ++;
							}
						}
					}
					// 设置超时定时器
					m_pApp->SetTimer (this, IME_TIMER_ID, IME_KEY_PRESS_GAP);
					m_nTimeOut = 0;
				}
			}
			break;
		}
	}
}

// 符号输入法
void OIME::PunctuationProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE;

	if (nMsg == OM_KEYDOWN)
	{
		switch (wParam)
		{
		case KEY_UP:
			{
				// 向上
				LowerLinePageUp ();
			}
			break;

		case KEY_DOWN:
			{
				// 向下
				LowerLinePageDown ();
			}
			break;

		case KEY_0:
		case KEY_1:
		case KEY_2:
		case KEY_3:
		case KEY_4:
		case KEY_5:
		case KEY_6:
		case KEY_7:
		case KEY_8:
		case KEY_9:
			{
				// 数字键
				LowerLineGetChar (wParam, 1);
			}
			break;

		case KEY_PERIOD:
			{
				// 小数点
				SendChar ('.', 0x0);
			}
			break;

		case KEY_SPACE:
			{
				// 空格键
				SendChar (0x20, 0x0);
			}
			break;
		}
	}
}

// 修改拼音后续处理
BOOL OIME::RenewPinyin ()
{
	CHECK_TYPE_RETURN;

	// 得到当前选择的拼音字符串
	BYTE sSymbol[7];
	memset (sSymbol, 0x20, sizeof(sSymbol)-1);
	sSymbol[sizeof(sSymbol)-1] = 0x0;
	memcpy (sSymbol, m_sUpperLine, m_nSyLength);

	// 根据当前拼音组合查询汉字
	m_nHzCount = m_pIMEDB->GetHz (sSymbol, &m_sHzReturn);
	// 将下栏当前页设置为第一页
	m_nCurPage = 0;
	// 更新下栏
	RenewLowerLine ();

	return TRUE;
}

// 修改九键拼音以后的后续处理(更新上下栏)
BOOL OIME::RenewSy ()
{
	CHECK_TYPE_RETURN;

	// 更新上栏
	RenewUpperLine ();
	// 得到当前选择的拼音字符串
	BYTE sSymbol[7];
	memset (sSymbol, 0x20, sizeof(sSymbol)-1);
	sSymbol[sizeof(sSymbol)-1] = 0x0;
	int nCurSyPos = (m_nSyLength + 1) * m_nCurSy;
	memcpy (sSymbol, &m_sUpperLine[nCurSyPos], m_nSyLength);
	// 根据当前拼音组合查询汉字
	m_nHzCount = m_pIMEDB->GetHz (sSymbol, &m_sHzReturn);
	// 将下栏当前页设置为第一页
	m_nCurPage = 0;
	// 更新下栏
	RenewLowerLine ();

	return TRUE;
}

// 根据m_sSyReturn、m_nSyLength、m_nSyCount更新上栏的字符串m_sUpperLine并设置m_nSyValidCount
void OIME::RenewUpperLine ()
{
	CHECK_TYPE;

	m_bAllSyInvalid = FALSE;

	memset (m_sUpperLine, 0x0, sizeof(m_sUpperLine));
	m_nSyValidCount = 0;
	int i;
	for (i = 0; i < m_nSyCount; i++)
	{
		int nOffset = i * 8;
		// 如果是完全匹配的，则加入上行
		if (m_sSyReturn[nOffset + 6] == '1')
		{
			int nTargetOffset = m_nSyValidCount * (m_nSyLength + 1);
			memcpy (&m_sUpperLine[nTargetOffset], &m_sSyReturn[nOffset], m_nSyLength);
			m_sUpperLine[nTargetOffset + m_nSyLength] = 0x20;		// 拼音结束位置补一个空格
			m_nSyValidCount ++;
		}
	}

	// 如果总的行数不为0而有效行数为0，
	// 说明所有拼音组合都是不完全组合，这时应当列出所有不完全组合
	// 注意：应消除“go go ho ho”“ko ko lo lo”的现象
	if ((m_nSyCount > 0) && (m_nSyValidCount == 0))
	{
		m_bAllSyInvalid = TRUE;
		char sOldSymbol [7];	// 用比较法消除go go ho ho现象
		for (i = 0; i < m_nSyCount; i++)
		{
			int nOffset = i * 8;
			int nTargetOffset = m_nSyValidCount * (m_nSyLength + 1);
			if ((memcmp (sOldSymbol, &m_sSyReturn[nOffset], m_nSyLength)) != 0)
			{
				memcpy (&m_sUpperLine[nTargetOffset], &m_sSyReturn[nOffset], m_nSyLength);
				m_sUpperLine[nTargetOffset + m_nSyLength] = 0x20;		// 拼音结束位置补一个空格
				m_nSyValidCount ++;
				memcpy (sOldSymbol, &m_sSyReturn[nOffset], 6);
			}
		}
	}
}

// 根据m_sHzReturn、m_nHzCount、m_nCurPage更新下栏的字符串m_sLowerLine
void OIME::RenewLowerLine ()
{
	CHECK_TYPE;

	memset (m_sLowerLine, 0x0, sizeof(m_sLowerLine));

	if (m_nHzCount == 0) {
		return;
	}

	int nCount = 10;
	// 如果是最后一行，则长度取余数
	if (m_nCurPage == (((m_nHzCount + 9) / 10) - 1))
	{
		nCount = m_nHzCount % 10;
		if (nCount == 0) {
			nCount = 10;
		}
	}

	// 设置cLowerLine字符串的内容
	int nOffset = m_nCurPage * 20;
	int i;
	for (i = 0; i < nCount; i++)
	{
		if (i != 9) {
			m_sLowerLine[i*4] = (i + '1');					// 标号(从1开始)
		}
		else {
			m_sLowerLine[i*4] = ('0');
		}
		m_sLowerLine[i*4+1] = m_sHzReturn[nOffset+i*2];		// 第一个字节
		m_sLowerLine[i*4+2] = m_sHzReturn[nOffset+i*2+1];	// 第二个字节
		m_sLowerLine[i*4+3] = 0x20;							// 空格
	}
}

// 下行分页的向前翻页处理
void OIME::LowerLinePageUp ()
{
	CHECK_TYPE;

	if (m_nCurPage > 0) {
		m_nCurPage --;
	}
	RenewLowerLine ();
}

// 下行分页的向后翻页处理
void OIME::LowerLinePageDown ()
{
	CHECK_TYPE;

	int nPageCount = (m_nHzCount + 8) / 9;
	if (m_nCurPage < (nPageCount - 1)) {
		m_nCurPage ++;
	}
	RenewLowerLine ();
}

// 根据字母查找对应的数字键
char OIME::CharToKey (char cc)
{
	CHECK_TYPE_RETURN;

	// 如果是大写字母，则转换为小写字母
	if ((cc >= 'A') && (cc <= 'Z')) {
		cc += 0x20;
	}

	int i;
	for (i=0; i<4; i++)
	{
		if (Key0[i] == cc) {
			return '0';
		}
	}
	for (i=0; i<4; i++)
	{
		if (Key1[i] == cc) {
			return '1';
		}
	}
	for (i=0; i<4; i++)
	{
		if (Key2[i] == cc) {
			return '2';
		}
	}
	for (i=0; i<4; i++)
	{
		if (Key3[i] == cc) {
			return '3';
		}
	}
	for (i=0; i<4; i++)
	{
		if (Key4[i] == cc) {
			return '4';
		}
	}
	for (i=0; i<4; i++)
	{
		if (Key5[i] == cc) {
			return '5';
		}
	}
	for (i=0; i<4; i++)
	{
		if (Key6[i] == cc) {
			return '6';
		}
	}
	for (i=0; i<4; i++)
	{
		if (Key7[i] == cc) {
			return '7';
		}
	}
	for (i=0; i<4; i++)
	{
		if (Key8[i] == cc) {
			return '8';
		}
	}
	for (i=0; i<4; i++)
	{
		if (Key9[i] == cc) {
			return '9';
		}
	}
	return 0x0;
}

// 将按键消息转换成按键字符
char OIME::MessageToKey (int nMessage)
{
	CHECK_TYPE_RETURN;

	char cKey = 0x0;
	switch (nMessage)
	{
	case KEY_0:	cKey='0'; break;
	case KEY_1:	cKey='1'; break;
	case KEY_2:	cKey='2'; break;
	case KEY_3:	cKey='3'; break;
	case KEY_4:	cKey='4'; break;
	case KEY_5:	cKey='5'; break;
	case KEY_6:	cKey='6'; break;
	case KEY_7:	cKey='7'; break;
	case KEY_8:	cKey='8'; break;
	case KEY_9:	cKey='9'; break;
	}
	return cKey;
}

// 根据数字键查找对应的字母(返回第一个)
char OIME::KeyToChar (char cKey)
{
	CHECK_TYPE_RETURN;

	switch (cKey)
	{
	case '0':	return Key0[0];
	case '1':	return Key1[0];
	case '2':	return Key2[0];
	case '3':	return Key3[0];
	case '4':	return Key4[0];
	case '5':	return Key5[0];
	case '6':	return Key6[0];
	case '7':	return Key7[0];
	case '8':	return Key8[0];
	case '9':	return Key9[0];
	}
	return 0x0;
}

// 查找该字母所在数字键组的下一个字母
char OIME::KeyNextChar (char cc)
{
	CHECK_TYPE_RETURN;

	// 如果是大写字母，则转换为小写字母
	if ((cc >= 'A') && (cc <= 'Z')) {
		cc += 0x20;
	}

	char cKey = CharToKey (cc);

	char* pGroup = NULL;
	switch (cKey)
	{
	case '0':	pGroup = (char *)Key0;	break;
	case '1':	pGroup = (char *)Key1;	break;
	case '2':	pGroup = (char *)Key2;	break;
	case '3':	pGroup = (char *)Key3;	break;
	case '4':	pGroup = (char *)Key4;	break;
	case '5':	pGroup = (char *)Key5;	break;
	case '6':	pGroup = (char *)Key6;	break;
	case '7':	pGroup = (char *)Key7;	break;
	case '8':	pGroup = (char *)Key8;	break;
	case '9':	pGroup = (char *)Key9;	break;
	}

	if (pGroup == NULL) {
		return 0x0;
	}

	int i;
	for (i = 0; i < 4; i++)
	{
		char cTemp = pGroup[i];
		if (cTemp == cc)
		{
			if (i == 3)	// 该键组的最后一个
			{
				return pGroup[0];
			}
			else if(pGroup[i+1] == '\0')
			{
				return pGroup[0];
			}
			else
			{
				return pGroup[i+1];
			}
		}
	}
	return 0x0;
}

/* END */