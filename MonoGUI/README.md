windows下：
安装 VS2022（需要安装桌面开发和MFC支持），打开 MonoGUI.sln 即可直接编译运行。

ubuntu下：
1、将 DEMO_Ubuntu22.04、dtm、zhlib、MonoGUI 四个目录拷贝到 Linux 下，注意不要改变文件名大小写；
2、进入 MonoGUI 目录，执行 make 命令，生成 monogui.a 文件；
3、进入 DEMO_Ubuntu22.04 目录，执行 make 命令，生成可执行程序 demo 并复制到上一级目录；
4、回到 DEMO_Ubuntu22.04 的上级目录，./demo 执行即可。

Linux 下的注意事项：
1、不要转成 utf-8 格式！
2、不要添加 -input-charset=GBK 编译选项！
3、拼音表索引文件（zhlib下的.idx文件）在windows下生成，再拷贝到 Linux 去用。
4、图片转换器、DTM（对话框模板）编辑器等工具只有windows版（也提供了源码）。