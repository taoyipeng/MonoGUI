// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(__AFX_STDAFX_H__)
#define __AFX_STDAFX_H__


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

// TODO: reference additional headers your program requires here
#include <windows.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>
////

#include "../MonoGUI/MonoGUI.h"

#include "Desktop.h"
#include "DlgShowButtons.h"
#include "DlgShowCombo.h"
#include "DlgShowEdit.h"
#include "DlgShowList.h"
#include "DlgShowProgress.h"
#include "MyApp.h"

#endif // !defined(__AFX_STDAFX_H__)
