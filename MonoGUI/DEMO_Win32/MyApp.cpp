////////////////////////////////////////////////////////////////////////////////
// @file DemoApp.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "stdafx.h"
#include "Images.h"

MyApp::MyApp()
{
	InitImageMgt (m_pImageMgt);
}

MyApp::~MyApp()
{
}

void MyApp::PaintWindows (OWindow* pWnd)
{
	OApp::PaintWindows (pWnd);

	// 测试代码，看看buffer里面画了什么东西
	//m_pScreenBuffer->DebugSaveSnapshoot("ScreenBuffer.bmp");
	//m_pLCD->DebugSaveSnapshoot("LCD.bmp");
	//exit(0);
}

// 显示开机画面
void MyApp::ShowStart ()
{
	m_pSysBar->SetStatus (OSystemBar::SYSBAR_STATE_BAT_SUPPLY);
	m_pSysBar->SetBattery (50);

	SetClockPos (280, 20);

#if defined (MOUSE_SUPPORT)
	SetClockButton (SCREEN_W - 40, 20);
#endif // defined(MOUSE_SUPPROT)

	// TODO：开机动画可以添加在这个位置

	return;
}

/* END */
