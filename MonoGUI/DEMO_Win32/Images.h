////////////////////////////////////////////////////////////////////////////////
// @file Images.h: global images.
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
// 定义图像按钮使用的的图片

#if !defined(__IMAGES_H__)
#define __IMAGES_H__
//

class BWImgMgt;
void InitImageMgt (BWImgMgt* pImgMgt);

#define ID_IMG_BTN_001    200101
#define ID_IMG_BTN_002    200102
#define ID_IMG_BTN_003    200103

//
#endif // !defined(__IMAGES_H__)
