////////////////////////////////////////////////////////////////////////////////
// @file DlgShowButtons.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "Images.h"


static int l_arImageButtonID_Options[] =
{
	ID_IMG_BTN_001,
	ID_IMG_BTN_002,
	ID_IMG_BTN_003
};
#define IMAGE_OPTIONS_COUNT   (sizeof(l_arImageButtonID_Options) / sizeof(l_arImageButtonID_Options[0]))
//////////////////////////////////////////////////////////////////////
CDlgShowButtons::CDlgShowButtons()
{
	m_nCurrentImageIndex = 0;
}

CDlgShowButtons::~CDlgShowButtons()
{
}

// 消息处理过了，返回1，未处理返回0
int CDlgShowButtons::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	ODialog::Proc (pWnd, nMsg, wParam, lParam);
	
	if (pWnd = this)
	{
		if (nMsg == OM_NOTIFY_PARENT)
		{
			switch (wParam)
			{
			case 102:
				{
					// 编辑框展示按钮
					CDlgShowEdit* pDlg = new CDlgShowEdit();
					if (! pDlg->CreateFromID (this, 102))
					{
						delete pDlg;
						WORD wStyle = OMB_ERROR | OMB_SOLID;
						OMsgBox (this, "出错信息", "创建对话框失败！", wStyle, 30);
					}
				}
				break;

			case 103:
				{
					// 组合框展示按钮
					CDlgShowCombo* pDlg = new CDlgShowCombo();
					if (! pDlg->CreateFromID(this, 103))
					{
						delete pDlg;
						WORD wStyle = OMB_ERROR | OMB_SOLID;
						OMsgBox (this, "出错信息", "创建对话框失败！", wStyle, 40);
					}
					else
					{
						pDlg->Init();
					}
				}
				break;

			case 104:
				{
					// 进度条展示按钮
					CDlgShowProgress* pDlg = new CDlgShowProgress();
					if (! pDlg->CreateFromID (this, 104))
					{
						delete pDlg;
						WORD wStyle = OMB_ERROR | OMB_SOLID;
						OMsgBox (this, "出错信息", "创建对话框失败！", wStyle, 50);
					}
					else
					{
						pDlg->Init();
					}
				}
				break;

			case 105:
				{
					// 列表框展示按钮
					CDlgShowList* pDlg = new CDlgShowList();
					if (! pDlg->CreateFromID (this, 105))
					{
						delete pDlg;
						WORD wStyle = OMB_ERROR | OMB_SOLID;
						OMsgBox (this, "出错信息", "创建对话框失败！", wStyle, 60);
					}
					else
					{
						pDlg->Init();
					}
				}
				break;

			case 106:
				{
					// 退出按钮
					O_MSG msg;
					msg.pWnd = this;
					msg.message = OM_CLOSE;
					msg.wParam = 0;
					msg.lParam = 0;
					m_pApp->PostMsg (&msg);
				}
				break;

			case 110:
				{
					OMsgBox (this, "按钮信息", "您按下了图形按钮！\n\n我们给按钮换一张图片", OMB_INFORMATION | OMB_SOLID, 60);
					OImgButton* pImgBtn = (OImgButton *)FindChildByID (110);
					m_nCurrentImageIndex ++;
					if (m_nCurrentImageIndex >= IMAGE_OPTIONS_COUNT) {
						m_nCurrentImageIndex = 0;
					}
					int nImgID = l_arImageButtonID_Options[m_nCurrentImageIndex];
					pImgBtn->SetImage(m_pApp->m_pImageMgt->GetImage(nImgID));
				}
			}
		}
	}

	return 1;
}

/* END */