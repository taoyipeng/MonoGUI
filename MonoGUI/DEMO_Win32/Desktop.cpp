////////////////////////////////////////////////////////////////////////////////
// @file Desktop.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"

#include "../MonoGUI/MonoGUI.h"
#include "Desktop.h"

static BYTE imgLogo[] =
{
0xff,0xff,0xff,0xfe,0x00,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xe0,0x00,0x00,0x00,
0xff,0xff,0xff,0xf8,0x00,0x1f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xe0,0x00,0x00,0x00,
0xff,0xff,0xff,0xf0,0x00,0x03,0xff,0xff,0xf8,0x00,0x03,0xff,0xff,0xff,0xff,0xff,0xe0,0x01,0x04,0x40,
0xff,0xff,0xff,0xe0,0x41,0xfd,0xff,0xe0,0x07,0xff,0xfc,0x00,0x80,0x1f,0xff,0xff,0xe3,0xff,0x88,0x40,
0xff,0xff,0xe7,0xe3,0x87,0xfe,0x7c,0x1f,0xff,0xff,0xff,0xf8,0x7f,0xc3,0xff,0xff,0xe0,0x01,0x11,0xf8,
0xff,0xff,0x01,0x9f,0x1f,0xff,0xbf,0xff,0xff,0xff,0xff,0xf1,0xff,0xf8,0xff,0xff,0xe7,0xfd,0x24,0x40,
0xff,0xfc,0x00,0x7f,0x7f,0xff,0xdf,0xff,0xff,0xff,0xff,0xe3,0xff,0xff,0x7f,0xff,0xe0,0x01,0x08,0x40,
0xff,0xf8,0x01,0xff,0xff,0xc3,0xef,0xff,0xff,0xff,0xff,0xc7,0xff,0xff,0xbf,0xff,0xe1,0xf1,0x1b,0xfc,
0xff,0xf0,0x07,0xcf,0xff,0xbd,0xe3,0xff,0xff,0xff,0xff,0xc3,0xff,0xff,0xbf,0xff,0xe1,0x11,0x28,0x40,
0xff,0xe0,0x0f,0x37,0xff,0x7e,0xdd,0xff,0xff,0xff,0xff,0xc3,0xff,0xff,0xbf,0xff,0xe1,0x11,0x09,0x78,
0xff,0xc0,0x1e,0xfb,0xfe,0xff,0x5e,0xff,0xff,0xff,0xff,0xc7,0xff,0xff,0xbf,0xff,0xe1,0xf1,0x09,0x40,
0xff,0x80,0x3d,0xdf,0xfe,0xff,0x7f,0x7f,0xff,0xff,0xff,0xc7,0x8f,0xff,0xbf,0xff,0xe1,0x11,0x09,0x40,
0xff,0x00,0x7d,0xef,0xfe,0xf9,0x7f,0x7f,0xff,0xff,0xff,0xc7,0xa7,0xff,0xbf,0xff,0xe0,0x05,0x0a,0xc0,
0xff,0x00,0xfb,0xf7,0xfe,0xf9,0x79,0x7f,0xff,0xff,0xf8,0x33,0x83,0xff,0xbf,0xff,0xe0,0x02,0x0c,0x3c,
0xfe,0x00,0xfb,0x87,0xbf,0x7e,0xf9,0x7f,0xff,0xff,0x87,0xfb,0x83,0xff,0x3f,0xff,0xe0,0x00,0x00,0x00,
0xfc,0x01,0xfb,0xb7,0x9f,0xbd,0xfe,0xff,0xff,0xf0,0x7f,0xf9,0x19,0xfc,0x7f,0xff,0xe2,0x00,0x08,0x20,
0xfd,0x01,0xfb,0xcf,0x8f,0xc3,0xfd,0xff,0xff,0x0f,0xff,0xfc,0x1d,0xf8,0xff,0xff,0xe1,0x3f,0x89,0xf8,
0xf9,0x03,0xfd,0xf7,0x97,0xff,0xf3,0xff,0xfc,0xff,0xff,0xfe,0x3d,0xe4,0x7f,0xff,0xe0,0x20,0x11,0x08,
0xfb,0x03,0xfe,0xcf,0x17,0xff,0xf7,0xff,0xf3,0xff,0xff,0xfc,0x3e,0xdc,0x7f,0xff,0xe4,0x20,0x15,0xf8,
0xf2,0x03,0xff,0x3f,0x3b,0xff,0xf7,0xff,0xcf,0xff,0xff,0xfc,0x7f,0x3e,0x3f,0xff,0xe2,0x20,0x39,0x00,
0xf6,0x03,0xff,0xff,0x3d,0xff,0xfb,0xff,0x3f,0xff,0xff,0xf8,0xff,0xfe,0x3f,0xff,0xe0,0xa0,0x09,0xfc,
0xe6,0x07,0xff,0x7f,0x3a,0xff,0xfb,0xfe,0xff,0xff,0xff,0xf8,0xff,0xff,0x3f,0xff,0xe1,0x20,0x11,0x54,
0xef,0x07,0xfe,0xff,0x7b,0x3f,0xfb,0xf9,0xff,0xff,0xff,0xf1,0xff,0xff,0x3f,0xff,0xe2,0x20,0x3d,0x54,
0xcf,0x07,0xfc,0xff,0x7b,0x83,0xfb,0xf7,0xff,0xff,0xfc,0x31,0xff,0xff,0x3f,0xff,0xe6,0x20,0x01,0xfc,
0xdf,0x07,0xf9,0xff,0xbb,0xac,0x7d,0xef,0xff,0xff,0xf3,0xc3,0xff,0xff,0x3f,0xff,0xe2,0x20,0x0d,0x54,
0x9f,0x87,0xf9,0xff,0xb9,0x6f,0x85,0xff,0xff,0xff,0xef,0xf0,0x3f,0xff,0x3f,0xff,0xe2,0x3f,0xb3,0x54,
0xbf,0x87,0xf3,0xff,0xbe,0x6f,0x79,0xff,0xff,0xff,0x9f,0xf3,0xc3,0xff,0x3f,0xff,0xe2,0x00,0x05,0x0c,
0xbf,0xc7,0xf3,0xff,0xbf,0x8c,0x7f,0xff,0xff,0xff,0x7f,0xf3,0xfc,0x3f,0x3f,0xff,0xe0,0x00,0x00,0x00,
0x3f,0xc3,0xe3,0xff,0xdf,0xf3,0x7f,0xff,0xff,0xff,0x7f,0xf3,0xff,0xc7,0x3f,0xff,0xe0,0x00,0x00,0x00,
0x7f,0xe3,0xe7,0xff,0xdf,0x0e,0xff,0xff,0xff,0xfe,0xff,0xe3,0xff,0x39,0x3f,0xff,0xff,0xff,0xff,0xff,
0x7f,0xf1,0xe7,0xff,0xe0,0xfe,0xff,0xff,0xff,0xfe,0xff,0xe3,0xfc,0xfe,0x3f,0xff,0xff,0xff,0xff,0xff,
0x7f,0xf1,0xe7,0xff,0xff,0xfe,0xff,0xff,0xff,0xfd,0xff,0xe7,0xfb,0xbf,0xbf,0xff,0xff,0xff,0xff,0xff,
0xff,0xf8,0xe3,0xff,0xff,0xfe,0xff,0xff,0xff,0xfd,0xff,0xe7,0xf7,0x3f,0x9f,0xff,0xff,0xff,0xff,0xff,
0xff,0xfc,0xe3,0xff,0xff,0xfe,0xff,0xff,0xff,0xfd,0xff,0xe7,0xf7,0x7f,0xdf,0xff,0xff,0xff,0xff,0xff,
0xff,0xfe,0x73,0xff,0xff,0xff,0x7f,0xff,0xff,0xcd,0xff,0xe7,0xef,0x7f,0xdf,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0x31,0xff,0xff,0xff,0x7f,0xff,0xff,0xb5,0xff,0xe7,0xdf,0x7f,0xdf,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0x80,0xff,0xff,0xff,0x7f,0xff,0xff,0x75,0xff,0xc3,0xbf,0x7f,0xdf,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xf8,0x7f,0xff,0xff,0x7f,0xff,0xfe,0xed,0xff,0x81,0xbf,0x7f,0xef,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xfc,0x3f,0xff,0xff,0xbf,0xff,0xfe,0xed,0xff,0xa5,0x7f,0xbf,0xef,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xfe,0x1f,0xff,0xff,0xbf,0xff,0xfd,0xdd,0xff,0xb5,0x7f,0xbf,0xec,0x3f,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0x8f,0xff,0xff,0xbf,0xff,0xfb,0xde,0xff,0xcb,0x7f,0x9f,0xee,0xdf,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xc7,0xff,0xff,0xbf,0xff,0xfb,0xde,0xff,0xe6,0xff,0xcf,0xef,0x6f,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xe3,0xff,0xff,0xbf,0xff,0xe7,0xbe,0xff,0xfe,0xf8,0x0f,0xef,0xa0,0xc3,0xff,0xff,0xff,
0xff,0xff,0xff,0xf8,0xff,0xfe,0x7f,0xff,0xdf,0xbe,0xff,0xfd,0xf3,0xff,0xef,0xdb,0x6d,0xff,0xff,0xff,
0xff,0xff,0xff,0xfe,0x3f,0xfd,0xff,0xff,0x3f,0xbf,0x7f,0xfd,0xe7,0xff,0xdf,0xfd,0xb6,0xff,0xff,0xff,
0xff,0xfb,0xff,0xff,0xcf,0x83,0xff,0xfe,0xff,0xbf,0x7f,0xfd,0xce,0x1f,0xdc,0x3e,0xba,0xff,0xff,0xff,
0xff,0xf9,0xff,0xff,0xf0,0x7f,0xff,0xf9,0xff,0xdf,0xbf,0xfd,0xdf,0x6f,0xde,0xdf,0x7d,0xff,0xff,0xff,
0xff,0xfc,0xff,0xff,0xfe,0x00,0xff,0xc5,0xff,0xdf,0x0f,0xfd,0x9f,0xb7,0xdf,0x6f,0xff,0xff,0xff,0xff,
0xff,0xfe,0x7f,0xff,0xff,0xff,0x00,0x3e,0xff,0xe0,0xe7,0xfd,0xbf,0xd7,0xdf,0xaf,0xff,0xff,0xff,0xff,
0xff,0xfe,0x3f,0xff,0xff,0xff,0xff,0xff,0x7f,0xff,0xeb,0xfd,0xbf,0xef,0xc3,0xdf,0xff,0xff,0xff,0xff,
0xff,0xff,0x0f,0xff,0xff,0xff,0xff,0xff,0x7f,0xff,0xed,0xfd,0xbf,0xff,0xad,0x0f,0xff,0xff,0x0f,0xff,
0xff,0xff,0x07,0xff,0xff,0xff,0xff,0xff,0xbf,0xff,0xee,0x7d,0xbf,0xff,0xb6,0xb7,0xf8,0x7f,0xb7,0xff,
0xff,0xff,0x83,0xff,0xff,0xff,0xff,0xff,0xdf,0xff,0x9f,0xbd,0xbf,0xff,0x7a,0xdb,0xfd,0xbf,0xdb,0xff,
0xff,0xff,0xc1,0xff,0xff,0xff,0xff,0xff,0xc3,0xfe,0x7f,0xdd,0xf8,0x7f,0x7d,0xeb,0xfe,0xdf,0xeb,0xff,
0xff,0xff,0xe0,0x7f,0xff,0xff,0xff,0xff,0xec,0x01,0xff,0xee,0xfd,0xbe,0xff,0xf7,0x0f,0x5f,0xf7,0xff,
0xff,0xff,0xf0,0x1f,0xff,0xff,0xff,0xff,0xef,0xff,0xff,0xf6,0xfe,0xde,0xf8,0x7f,0xb7,0xbf,0xff,0xff,
0xff,0xff,0xf8,0x07,0xff,0xff,0xff,0xff,0xef,0xff,0xff,0xf9,0x7f,0x5d,0xfd,0xbf,0xdb,0xff,0xff,0xff,
0xff,0xff,0xfc,0x01,0xff,0xff,0xff,0xff,0xef,0xff,0xff,0xfe,0x7f,0xb3,0x0e,0xdf,0xeb,0xff,0xff,0xff,
0xff,0xff,0xfc,0x00,0x3f,0xff,0xff,0xff,0xef,0xff,0xff,0xff,0x3f,0xef,0xb7,0x5f,0xf7,0x0f,0xff,0xff,
0xff,0xff,0xfc,0x00,0x03,0xff,0xff,0xff,0xdf,0xff,0xff,0xff,0xcf,0x1f,0xdb,0xb0,0xff,0xb7,0xff,0xff,
0xff,0xff,0xf8,0x00,0x00,0x1f,0xff,0xff,0xdf,0xff,0xff,0xff,0xf0,0xff,0xeb,0xfb,0x7f,0xdb,0xff,0xff,
0xff,0xff,0xe0,0x00,0x00,0x00,0x0f,0xff,0xbf,0xff,0xff,0xff,0xff,0xff,0xf7,0xfd,0xbf,0xeb,0xff,0xff,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xfe,0xbf,0xf7,0xff,0xff,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x7f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x7f,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf0,0x7f,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xe0,0x3f,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xc0,0x1f,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xc1,0xff,0xcf,0x1f,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x80,0xff,0xdf,0x9f,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x0e,0x7f,0xdf,0x9f,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x1f,0x70,0x0f,0x3f,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x1f,0x0f,0xf0,0x7f,
0xff,0x1f,0xfe,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0x80,0xfc,0xff,0x9f,0x3f,0xff,0x1e,0xff,0xfe,0xff,
0xff,0x0f,0xfc,0x3f,0xff,0xff,0xff,0xff,0xff,0xfe,0x00,0x3c,0xff,0x9f,0x3f,0xff,0x8d,0xff,0xe7,0x7f,
0xff,0x0f,0xfc,0x3f,0xff,0xff,0xff,0xff,0xff,0xfc,0x7f,0x3c,0xff,0x9f,0x3f,0xff,0xc3,0x8f,0xc1,0xbf,
0xff,0x07,0xfc,0x3f,0xff,0xff,0xff,0xff,0xff,0xf8,0xff,0xfc,0xff,0x9f,0x3f,0xff,0xf7,0x07,0x9e,0xdf,
0xff,0x27,0xf9,0x3f,0xff,0xff,0xff,0xff,0xff,0xf1,0xff,0xfc,0xff,0x9f,0x3f,0xff,0xf6,0xf3,0x90,0x5f,
0xff,0x27,0xf9,0x3f,0xc1,0xfc,0xc1,0xff,0x07,0xf3,0xff,0xfc,0xff,0x9f,0x3f,0xff,0xec,0x13,0xcc,0x6f,
0xff,0x33,0xf9,0x3f,0x00,0xfc,0x80,0xfc,0x03,0xe7,0xff,0xfc,0xff,0x9f,0x3f,0xff,0xec,0x67,0xc6,0x6f,
0xff,0x33,0xf3,0x3e,0x3c,0x7c,0x3c,0x78,0xf1,0xe7,0xff,0xfc,0xff,0x9f,0x3f,0xff,0xec,0xc6,0xe0,0xef,
0xff,0x39,0xf3,0x3e,0x7e,0x3c,0x7e,0x79,0xf8,0xe7,0xff,0xfc,0xff,0x9f,0x3f,0xff,0xee,0x0d,0x71,0xef,
0xff,0x39,0xe7,0x3c,0xff,0x3c,0xfe,0x73,0xfc,0xe7,0xf0,0x3c,0xff,0x9f,0x3f,0xff,0xef,0x1e,0xff,0xef,
0xff,0x3c,0xe7,0x3c,0xff,0x3c,0xfe,0x73,0xfc,0xe7,0xf0,0x3c,0xff,0x9f,0x3f,0xff,0xef,0xff,0xc7,0xeb,
0xff,0x3c,0xe7,0x3c,0xff,0x3c,0xfe,0x73,0xfc,0xe7,0xff,0x3c,0xff,0x9f,0x3f,0xff,0xf7,0xe3,0xbb,0xd9,
0xff,0x3c,0x4f,0x3c,0xff,0x3c,0xfe,0x73,0xfc,0xe3,0xff,0x3c,0xff,0x9f,0x3f,0xff,0xf7,0xdc,0x7b,0xda,
0xff,0x3e,0x4f,0x3c,0xff,0x3c,0xfe,0x73,0xfc,0xf3,0xff,0x3c,0xff,0x9f,0x3f,0xff,0xfb,0xdf,0xfb,0xba,
0xff,0x3e,0x5f,0x3e,0x7e,0x7c,0xfe,0x79,0xf9,0xf1,0xff,0x3e,0x7f,0x3f,0x3f,0xff,0xfd,0xe0,0x07,0x7a,
0xff,0x3f,0x1f,0x3e,0x3c,0x7c,0xfe,0x78,0xf1,0xf8,0x7e,0x3e,0x3e,0x3f,0x3f,0xff,0xee,0x7f,0xfc,0xed,
0xff,0x3f,0x1f,0x3f,0x00,0xfc,0xfe,0x7c,0x03,0xfc,0x00,0x3f,0x00,0x7f,0x3f,0xff,0xcf,0x8f,0xe3,0xe7,
0xff,0x3f,0xff,0x3f,0x81,0xfc,0xfe,0x7e,0x07,0xff,0x80,0xff,0x81,0xff,0x3f,0xff,0xaf,0xf0,0x1f,0xeb,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xaf,0x7f,0xff,0xeb,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xae,0x7f,0xff,0xeb,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xdd,0x7f,0xff,0xf7,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfd,0x7f,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfd,0x7f,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfe,0xff,0xff,0xff
};
static BW_IMAGE g_bwimg_Logo (160, 96, imgLogo);


#define ID_ENTER_BUTTON    (10001)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
Desktop::Desktop()
	: OWindow(WND_TYPE_DESKTOP)
{
}

Desktop::~Desktop()
{
}

// 虚函数，绘制窗口，只绘制附加的滚动条
void Desktop::Paint(LCD* pLCD)
{
	const int bkgnd_cr = 1;
	const int border_cr = 0;

	// 清空背景
	pLCD->FillRect(0, 0, SCREEN_W, SCREEN_H, bkgnd_cr);

	// 绘制双线边框
	const int border_w = 10;
	pLCD->Line(0, 0, SCREEN_W, 0, border_cr);
	pLCD->Line(0, 0, 0, SCREEN_H, border_cr);
	pLCD->Line(SCREEN_W - 1, 0, SCREEN_W - 1, SCREEN_H - 1, border_cr);
	pLCD->Line(0, SCREEN_H - 1, SCREEN_W - 1, SCREEN_H - 1, border_cr);
	pLCD->Line(border_w, border_w, SCREEN_W - border_w - 1, border_w, border_cr);
	pLCD->Line(border_w, border_w, border_w, SCREEN_H - border_w - 1, border_cr);
	pLCD->Line(SCREEN_W - border_w - 1, border_w, SCREEN_W - border_w - 1, SCREEN_H - border_w - 1, border_cr);
	pLCD->Line(border_w, SCREEN_H - border_w - 1, SCREEN_W - border_w - 1, SCREEN_H - border_w - 1, border_cr);

	if (IsTopMostDialog()) {
		// 显示MonoGUI的Logo
		pLCD->TextOut(160, 60, (BYTE*)"（按 回车键 进入演示系统）", 26, LCD_MODE_NORMAL);
		pLCD->DrawImage(
			(SCREEN_W - g_bwimg_Logo.w) / 2,
			(SCREEN_H - g_bwimg_Logo.h) / 2,
			g_bwimg_Logo.w, g_bwimg_Logo.h,
			g_bwimg_Logo, 0, 0, LCD_MODE_NORMAL);
	}

	// 测试，绘制斜线
	//pLCD->Line (10,10, 230,118, 1);
	//pLCD->Line (240,118, 20,10, 1);
	//pLCD->Line (230,10, 10,118, 1);
	//pLCD->Line (20,118, 240,10, 1);

	OWindow::Paint (pLCD);
}

// 虚函数，消息处理
// 消息处理过了，返回1，未处理返回0
int Desktop::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	// 先进行默认处理
	int nReturn = OWindow::Proc (pWnd, nMsg, wParam, lParam);

	if (nReturn != 1)
	{
		switch (nMsg)
		{
		case OM_NOTIFY_PARENT:
			{
				if (ID_ENTER_BUTTON == wParam)
				{
					ShowDemoDialog();
				}
			}
			break;

		case OM_KEYDOWN:
			{
				switch (wParam)
				{
			  		case  KEY_ENTER:
						{
							// 生成“按钮演示”对话框
							ShowDemoDialog();

						//	实验DoModal
						//	pDlg->DoModal();
						//	wStyle = OMB_INFORMATION | OMB_SOLID;
						//	OMsgBox (this, "信息", "DoModal过程结束！", wStyle, 10);
						}
						break;

					default:
						{
							WORD wStyle = OMB_EXCLAMATION | OMB_ROUND_EDGE | OMB_YESNO | OMB_ENGLISH;
							int nRet = OModalMsgBox (this,"提示:(MsgBox演示)","您按下的不是确定键", wStyle, 20);
							char temp[256];
							sprintf (temp, "刚才的对话框DoModal\n返回值是：%d。", nRet);
							wStyle = OMB_EXCLAMATION | OMB_ROUND_EDGE | OMB_ENGLISH;
							OMsgBox (this,"提示:",temp,wStyle,40);
						}
				}
			}
			break;
		}
	}

	return nReturn;
}

// 窗口创建后的初始化处理
void Desktop::OnInit()
{
	// 添加“进入演示系统”按钮
	OButton* pEnterButton = new OButton ();
	pEnterButton->Create (this, WND_STYLE_NORMAL, WND_STATUS_NORMAL, 190, 220, 120, 40, ID_ENTER_BUTTON);
	pEnterButton->SetText ("进入演示系统",20);
	pEnterButton->m_wStyle |= WND_STYLE_ROUND_EDGE;
}

// 显示功能演示对话框
void Desktop::ShowDemoDialog()
{
	// 生成“按钮演示”对话框
	CDlgShowButtons* pDlg = new CDlgShowButtons();
	WORD wStyle = OMB_ERROR | OMB_SOLID;
	if (! pDlg->CreateFromID (this, 100))
	{
		delete pDlg;
		OMsgBox (this, "出错信息", "创建对话框失败！", wStyle, 10);
	}
}

/* END */