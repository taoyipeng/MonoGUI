////////////////////////////////////////////////////////////////////////////////
// @file DlgShowCombo.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "../MonoGUI/MonoGUI.h"
#include "DlgShowCombo.h"

CDlgShowCombo::CDlgShowCombo()
{
}

CDlgShowCombo::~CDlgShowCombo()
{
}

// 初始化
void CDlgShowCombo::Init()
{
	OCombo* pCombo = (OCombo*)FindChildByID (102);
	pCombo->AddString( (char*)"中国" );
	pCombo->AddString( (char*)"美国" );
	pCombo->AddString( (char*)"英国" );
	pCombo->AddString( (char*)"法国" );
	pCombo->AddString( (char*)"韩国" );
	pCombo->AddString( (char*)"俄罗斯" );
	pCombo->AddString( (char*)"意大利" );
	pCombo->AddString( (char*)"土耳其" );
	pCombo->AddString( (char*)"瑞典" );
	pCombo->AddString( (char*)"丹麦" );
	pCombo->AddString( (char*)"加拿大" );
	pCombo->AddString( (char*)"新西兰" );
}

// 消息处理过了，返回1，未处理返回0
int CDlgShowCombo::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	ODialog::Proc (pWnd, nMsg, wParam, lParam);

	if (pWnd = this)
	{
		if (nMsg == OM_NOTIFY_PARENT)
		{
			switch (wParam)
			{
			case 104:
				{
					OCombo* pCombo = (OCombo*)FindChildByID (102);
					int nSelIndex = pCombo->GetCurSel();
					if (-1 != nSelIndex) {
						char text[LIST_TEXT_MAX_LENGTH];
						if (pCombo->GetString(nSelIndex, text)) {
							char info[LIST_TEXT_MAX_LENGTH + 100];
							sprintf (info, (char*)"您选中的内容是：\n%s", text);
							OMsgBox (this, (char*)"信息", info,
								OMB_INFORMATION | OMB_SOLID | OMB_ROUND_EDGE, 60);
						}
					}
					else {
						OMsgBox (this, (char*)"信息", (char*)"请您在下拉列表中做出选择",
							OMB_INFORMATION | OMB_SOLID | OMB_ROUND_EDGE, 60);
					}
				}
				break;

			case 105:
				{
					// 退出按钮
					O_MSG msg;
					msg.pWnd = this;
					msg.message = OM_CLOSE;
					msg.wParam = 0;
					msg.lParam = 0;
					m_pApp->PostMsg (&msg);
				}
				break;
			}
		}
	}

	return 1;
}

/* END */